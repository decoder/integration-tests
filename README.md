Integration tests for DECODER
=============================

Description
-----------

This a set of benchmarks for the DECODER project.

These benchmarks are tests mostly intended for integration testing and non-regression testing of the tools.

Each test has a bash script (`run.sh`) to run the test and a dataset (in directory `data`).

The currently available tests are:

* [`project_mngmt`](./tests/project_mngmt): project management (projects, users, ...)
* [`vector2`](./tests/vector2): a small C project
* [`opencv-4.5.0`](./tests/opencv-4.5.0): a big C++ project
* [`mythaistar`](./tests/mythaistar): a small subset of [`mythaistar`](https://github.com/devonfw/my-thai-star)
* [`vmlinux`](./tests/vmlinux): a Linux kernel
* [`cve`](./tests/cve): a small benchmark for testing PKM API about CVE lists
* [`git`](./tests/git): a test of the PKM built-in support for Git
* [`at91sam9-watchdog`](./tests/at91sam9-watchdog): a testbench for the AT91SAM9 Watchdog timer (WDT) Linux driver
* [`e1000e-ethernet`](./tests/e1000e-ethernet): a testbench for the Intel e1000e Ethernet Linux driver

License
-------

The framework is license under the Apache License 2.0.
The dataset (software or documents) are under their respective license.

Targets
-------

The targets (set of configuration parameters for server addresses) correspond to the way the DECODER toolset was deployed.

There are two supported targets:

* [`master`](./api/conf.d/master.conf): for use with upstream version of the tools
* [`decoder`](./api/conf.d/decoder.conf): for use with DECODER project server

The environment variable TARGET can select the target.

The default target is 'master' which convenient for the testing tools locally on a workstation.

Tools
-----

The test scripts can invoke the tools by sourcing the bash helper script [`api/decoder_rest_api.sh`](./api/decoder_rest_api.sh).
See existing tests for details.

Certificates
------------

SSL/TLS Certificates are located in directory [`api/certs`](./api/certs).

Extensions
----------

New targets can be added in directory [`api/conf.d`](./api/conf.d).

helper scripts for additional tools can be added into directory [`api/tools.d`](./api/tools.d).

