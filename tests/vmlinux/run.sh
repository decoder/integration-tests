#!/usr/bin/env bash
DIR=$(cd $(dirname $BASH_SOURCE); pwd)
source "${DIR}/../../api/decoder_rest_api.sh"
source "${DIR}/test.conf"

set +o errexit

echo -n "${ADMIN_NAME}'s password: "
read -s ADMIN_PASSWORD
USER_PASSWORD=$(generate_random_password)

set -o nounset

function exit_status
{
	local STATUS=$1
	if [ $STATUS -eq 0 ]; then
		echo "Test PASSED" >&2
	else
		echo "Test FAILED" >&2
	fi

	EXIT_STATUS=$STATUS

	return $STATUS
}

return_status=return

while true; do

	echo "PKM administrator's login"
	ADMIN_KEY=$(pkm_login "${ADMIN_NAME}" "${ADMIN_PASSWORD}")
	[ $? -eq 0 ] || exit_status 1 || break
	echo

	echo "creating user ${USER_NAME} with password '${USER_PASSWORD}'"
	pkm_post_user "${ADMIN_KEY}" ${USER_NAME} "${USER_PASSWORD}" || exit_status 1 || break
	echo

	echo "creating project ${PROJECT_NAME} owned by ${USER_NAME}"
	pkm_post_project "${ADMIN_KEY}" "${PROJECT_NAME}" ${USER_NAME} '"Owner","Developer"' || exit_status 1 || break
	echo

	echo "user's login by ${USER_NAME}"
	USER_KEY=$(pkm_login ${USER_NAME} "${USER_PASSWORD}")
	[ $? -eq 0 ] || exit_status 1 || break
	echo

	echo "inserting vmlinux into ${PROJECT_NAME} by ${USER_NAME}"
	pkm_post_files "${USER_KEY}" "bin/executable/$(uri_encode "${PROJECT_NAME}")" 'binary' "${DIR}/data" 'vmlinux' || exit_status 1 || break
	echo

	echo "inserting data/excavator_invocation.json by ${USER_NAME}"
	pkm_post_json_files "${USER_KEY}" "invocations/$(uri_encode "${PROJECT_NAME}")" "${DIR}/data" "${DIR}/data/excavator_invocation.json" || exit_status 1 || break
	echo
	
	echo "running UNISIM Excavator by ${USER_NAME}"
	run_excavator "${USER_KEY}" "${PROJECT_NAME}" 'invocationID=567248udlhtonf' "$(compact_json "${DIR}/data/excavator_config.json")" || exit_status 1 || break
	echo
	
	echo "getting raw source codes from project ${PROJECT_NAME} by ${USER_NAME}"
	pkm_get "${USER_KEY}" "code/rawsourcecode/$(uri_encode "${PROJECT_NAME}")" || exit_status 1 || break
	echo
	
	exit_status 0
  
	break
done

echo "Tip: ${USER_NAME}'s password is '${USER_PASSWORD}'"

if mr_proper_prompt; then
	echo "cleaning"

	echo "deleting user ${USER_NAME}"
	pkm_delete "${ADMIN_KEY}" "user/${USER_NAME}"
	echo

	echo "deleting project"
	pkm_delete "${ADMIN_KEY}" "project/$(uri_encode "${PROJECT_NAME}")"
	echo
fi

exit ${EXIT_STATUS}
