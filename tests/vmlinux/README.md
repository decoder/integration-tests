vmlinux
=======

A linux kernel 4.9 for AMD64/x86_64 is used to test the following functions:

* PKM: project creation
* PKM: user creation
* PKM: administrator and user login
* PKM: inserting an ELF executable binary
* Excavator: running Excavator on [`vmlinux`](./data/vmlinux)
* PKM: querying the generated source code files
* PKM: deleting a user
* PKM: deleting a project

The benchmark applies Excavator on an x86_64 Linux kernel, and try to reverse engineer the compilation unit corresponding to Intel e1000e ethernet controller Linux driver.
A testbench skeleton intended for testing the processed compilation units is also generated.
The reverse engineering is limited to general code construction such as function protypes and types.
*No source code is required*: only an ELF executable binary file with DWARF debugging information is involved in the process.
The process to build the linux kernel is explained in later sections.

### Configure Linux building process to build a Linux kernel with debugging information

1. In Linux source tree, enter the Linux kernel configuration menu,  
`$ make menuconfig`
2. [Enter `Kernel Hacking`](images/menuconfig_kernel_hacking.png),  
![Enter Kernel Hacking](images/menuconfig_kernel_hacking.png)
3. [Enable `Kernel debugging`](images/menuconfig_kernel_debugging.png),  
![Enable Kernel debugging](images/menuconfig_kernel_debugging.png)
4. [Enter `Compile time checks and compiler options`](images/menuconfig_compile_time_checks_and_compiler_options.png),  
![Enter Compile time checks and compiler options](images/menuconfig_compile_time_checks_and_compiler_options.png)
5. [Enable `Compile the kernel with debug info` and `Generate dwarf4 debuginfo`](images/menuconfig_compile_the_kernel_with_debug_info.png).  
![Enable Compile the kernel with DWARF4 debug info](images/menuconfig_compile_the_kernel_with_debug_info.png)

### Build the provided Linux kernel

	$ wget http://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.9.207.tar.xz
	
	$ tar -xf linux-4.9.207.tar.xz
	
	$ cd linux-4.9.207
	
	$ cp ../linux-4.9.207.config .config
	
	$ KCFLAGS='-gstrict-dwarf' make vmlinux
	
	$ cd ..
