#include "stdlib.h"
#include "math.h"
//#include "stdio.h"

struct vector {
	int x;
	int y;
	int z;
};

const struct vector	null_vector = {0, 0, 0};

// Sum of two vectors
/*@ requires \valid(u) && \valid(v) && \valid(res);
	@ ensures \valid(res);
	@ ensures res->x == u->x + v->x;
	@ ensures res->y == u->y + v->y;
	@ ensures res->z == u->z + v->z;
	@ assigns *res;
 */
void add_vector (struct vector * u, struct vector *v, struct vector* res) {
	res->x = (u->x +v->x);
	res->y = (u->y +v->y);
	res->z = (u->z +v->z);
	return;
};

/*@ requires \valid(u) && \valid(v) && \valid(res);
	@ ensures \valid(res);
	@ ensures res->x == u->x - v->x;
	@ ensures res->y == u->y - v->y;
	@ ensures res->z == u->z - v->z;
	@ assigns *res;
 */
void sub_vector (struct vector * u, struct vector *v, struct vector* res) {
	res->x = (u->x -v->x);
	res->y = (u->y -v->y);
	res->z = (u->z -v->z);
	return;
};

// Scalar product of two vectors
/*@ requires \valid(u) && \valid(v);
	@ assigns \result;
 */
int product (struct vector * u, struct vector *v) {
	int res = (u->x * v->x) + (u->y * v->y) + (u->z * v->z);
	return res;
};

void resize (int l, struct vector *u) {
      u ->x = u->x*l;
      u ->y = u->y*l;
      u ->z = u->z*l;
      return;
};

/*@
	@ requires \valid(u);
	@ ensures \result >=0;
	@ assigns \result;
*/
int norm_square (struct vector * u) {
	return product(u,u);
};

int main () {
	struct vector a = {1,2,3};
	struct vector b = {4,5,6};
	struct vector c;
	add_vector (&a, &b, &c);
	int n = norm_square(&c);
	//	fprintf(stdout,"%f\n",norm(c));
	return 0;
}
