vector2
=======

Description
-----------

vector2 is a C project which tests the following functions:

* admin:
	* PKM: administrator login
* init:
	* PKM: project creation
	* PKM: user creation
* login:
	* PKM: user login
* insert:
	* PKM: inserting a C source code file: [`vector2.c`](./data/vector2.c)
	* PKM: inserting a [compile command](./data/vector2_compile_commands.json) for vector2.c
	* PKM: inserting a documentation file [`vector2.docx`](./data/vector2.docx) of vector2
	* PKM: inserting a UML class diagram of vector2
* run:
	* Frama-C: running Frama-C parser, EVA and WP on [`vector2.c`](./data/vector2.c)
	* ASFM: running ASFM to convert documentation file to ASFM documentation format
* query_code:
	* PKM: getting the source code files
	* PKM: querying the source code
	* PKM: querying the ASFM documentation
	* PKM: querying the UML class diagram
* trace_recovery:
	* Trace recovery: running Trace recovery tool
* srl:
	* SRL: running SRL tool
* ner:
	* NER: running NER tool
* semparsing:
	* Semantic parsing: running Semantic parsing tool
* query_traceability_matrix:
	* PKM: querying traceability matrix
* reset:
	* PKM: deleting a user
	* PKM: deleting a project

Test scripts
------------

* `run.sh`: run all functions or some functions grouped by stage (admin, init, login, insert, run, query_code, trace_recovery, srl, ner, semparsing, query_traceability_matrix, reset)

The scripts are interactive:

* A script can request the user for the PKM administrator password
* Prompt '`Do you want to clean before exiting (yes/No) ? `' appears unless MR_PROPER environment variable is set to '`yes`' or '`no`'

Examples of use
---------------

Run all functions:

	$ ./run.sh

Run all functions and clear everything at exit in an automated manner:

	$ MR_PROPER=yes ./run.sh

Run the test in three steps:

	$ STAGES='admin init login insert run' MR_PROPER=no ./run.sh
	$ STAGES='login query_code' MR_PROPER=no ./run.sh
	$ STAGES='admin reset' ./run.sh

Second step can be repeated many times.

Extensions
----------

Additional stages can be added to run other tools, that depends on the run of previous tools.
For example, after stage '`run`' and before stage '`reset`'.

