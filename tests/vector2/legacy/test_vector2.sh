function help()
{
	echo "Usage: test_vector2.sh <options>"
	echo "Options:"
	echo -e "\t--admin=<name>"
	echo -e "\t--db=<database>"
	echo -e "\t--host=<host:port>"
	echo -e "\t--pkm-db=<database>"
	echo "Example:"
	echo -e "\ttest_vector2.sh --admin=admin --auth-db=admin --pkm-db=pkm --db=mydb"
}

API=$(cd $(dirname $0)/..; pwd)
export DB_ROOT=$API
source "$API/bin/parse_cmdline"
cmdline_option_names=(admin db host pkm-db debug)
if parse_cmdline $@; then
	admin=${cmdline_options[admin]}
	db=${cmdline_options[db]}
	host=${cmdline_options[host]}
	pkm_db=${cmdline_options[pkm-db]}
	debug=${cmdline_options[debug]}
	if [ -z $admin ]; then echo 'No admin specified'; help; exit 1; fi
	if [ -z $db ]; then echo 'No database specified'; help; exit 1; fi
	if [ ${#cmdline_args[@]} -ne 0 ]; then echo "No arguments after options are expected"; help; exit 1; fi
	ADMIN_CMDLINE_OPTS=()
	if [ -n "$admin" ]; then ADMIN_CMDLINE_OPTS+=( "--admin=$admin" ); fi
	if [ -n "$host" ]; then ADMIN_CMDLINE_OPTS+=( "--host=$host" ); fi
	if [ -n "$debug" ]; then ADMIN_CMDLINE_OPTS+=( "--debug" ); fi
	USER_ADMIN_CMDLINE_OPTS=()
	if [ -n "$admin" ]; then USER_ADMIN_CMDLINE_OPTS+=( "--admin=$admin" ); fi
	if [ -n "$host" ]; then USER_ADMIN_CMDLINE_OPTS+=( "--host=$host" ); fi
	if [ -n "$pkm_db" ]; then USER_ADMIN_CMDLINE_OPTS+=( "--pkm-db=$pkm_db" ); fi
	if [ -n "$debug" ]; then USER_ADMIN_CMDLINE_OPTS+=( "--debug" ); fi
	USER_CMDLINE_OPTS=()
	if [ -n "$host" ]; then USER_CMDLINE_OPTS+=( "--host=$host" ); fi
	if [ -n "$pkm_db" ]; then USER_CMDLINE_OPTS+=( "--pkm-db=$pkm_db" ); fi
	if [ -n "$debug" ]; then USER_CMDLINE_OPTS+=( "--debug" ); fi
	echo -n "$admin@$pkm_db's password: "
	read -s ADMIN_PASSWORD
	
	status=0
	
	while true; do
		# create an empty project
		if ! echo "$ADMIN_PASSWORD" | "$API/bin/create_project" "${ADMIN_CMDLINE_OPTS[@]}" "--project=$db"; then
			echo "create_project failed"
			status=1
			break
		fi
		
		# create user garfield, garfield's password is 'garfield'
		if ! echo -e "$ADMIN_PASSWORD\ngarfield" | "$API/bin/create_user" "${USER_ADMIN_CMDLINE_OPTS[@]}" garfield; then
			echo "create_user failed"
			status=1
			break
		fi
		
		# make garfield 'developer' in project
		if ! echo -e "$ADMIN_PASSWORD" | "$API/bin/update_user_role" "${USER_ADMIN_CMDLINE_OPTS[@]}" "--db=$db" "--role=Developer" grant garfield; then
			echo "update_user_role failed"
			status=1
			break
		fi
		
		# insert source file vector2.c as garfield
		if ! echo "garfield" | "$API/bin/insert_source_files" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield "${API}/examples/vector2.c"; then
			echo "insert_source_files failed"
			status=1
			break
		fi
		
		# insert twice source file vector2.c as garfield: should fail
		if echo "garfield" | "$API/bin/insert_source_files" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield "${API}/examples/vector2.c"; then
			echo "insert_source_files succeeded"
			status=1
			break
		fi
		
		# insert source file hello.cpp as garfield
		if ! echo "garfield" | "$API/bin/insert_source_files" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield "${API}/examples/hello.cpp"; then
			echo "insert_source_files failed"
			status=1
			break
		fi
		
		# run frama-c
		if ! echo "garfield" | "$API/bin/run_frama_c" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield 'examples/vector2.c'; then
			echo "run_frama_c failed"
			status=1
			break
		fi
		
		# run frama-clang
		if ! echo "garfield" | "$API/bin/run_frama_clang" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield 'examples/hello.cpp'; then
			echo "run_frama_clang failed"
			status=1
			break
		fi
		
		# insert UML class diagram file vector2_uml_class.json as garfield
		if ! echo "garfield" | "$API/bin/insert_uml_class_diagram_files" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield "${API}/examples/vector2_uml_class.json"; then
			echo "insert_uml_class_diagram_files failed"
			status=1
			break
		fi
		
		# insert UML class diagram file vector2_uml_class.json as garfield: should fail
		if echo "garfield" | "$API/bin/insert_uml_class_diagram_files" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield "${API}/examples/vector2_uml_class.json"; then
			echo "insert_uml_class_diagram_files succeeded"
			status=1
			break
		fi
		
		# insert document file vector2.docx as garfield
		if ! echo "garfield" | "$API/bin/insert_doc_files" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield --format=binary "${API}/examples/vector2.docx"; then
			echo "insert_doc_files failed"
			status=1
			break
		fi
		
		# insert twice document file vector2.docx as garfield: should fail
		if echo "garfield" | "$API/bin/insert_doc_files" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield --format=binary "${API}/examples/vector2.docx"; then
			echo "insert_doc_files succeeded"
			status=1
			break
		fi
		
		# run doc_to_asfm
		if ! echo "garfield" | "$API/bin/run_doc_to_asfm" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield 'examples/vector2.docx'; then
			echo "run_doc_to_asfm failed"
			status=1
			break
		fi
		
		# create user odie, odie's password is 'odie'
		if ! echo -e "$ADMIN_PASSWORD\nodie" | "$API/bin/create_user" "${USER_ADMIN_CMDLINE_OPTS[@]}" odie; then
			echo "create_user failed"
			status=1
			break
		fi

		# update password of user odie to 'bone'
		if ! echo -e "odie\nbone" | "$API/bin/update_user_password" "${USER_CMDLINE_OPTS[@]}" odie; then
			echo "update_user_password failed"
			status=1
			break
		fi
		
		# make odie 'reviewer' in project
		if ! echo -e "$ADMIN_PASSWORD" | "$API/bin/update_user_role" "${USER_ADMIN_CMDLINE_OPTS[@]}" "--db=$db" "--role=Reviewer" grant odie; then
			echo "update_user_role failed"
			status=1
			break
		fi
		
		# find in sources Function add_vector as odie
		if ! echo "bone" | "$API/bin/find_in_c_source" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=odie function add_vector; then
			echo "find_in_c_source failed"
			status=1
			break
		fi
		
		# find in sources Variable null_vector as odie
		if ! echo "bone" | "$API/bin/find_in_c_source" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=odie variable null_vector; then
			echo "find_in_c_source failed"
			status=1
			break
		fi
		
		# find in sources Type vector as odie
		if ! echo "bone" | "$API/bin/find_in_c_source" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=odie type vector; then
			echo "find_in_c_source failed"
			status=1
			break
		fi
		
		# find in UML class diagrams Class vector and show attributes as odie
		if ! echo "bone" | "$API/bin/find_in_uml_class_diagrams" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=odie vector attributes; then
			echo "find_in_uml_class_diagrams failed"
			status=1
			break
		fi
		
		# find in UML class diagrams Class vector and show operations as odie
		if ! echo "bone" | "$API/bin/find_in_uml_class_diagrams" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=odie vector operations; then
			echo "find_in_uml_class_diagrams failed"
			status=1
			break
		fi
		
		# find in UML class diagrams Method add_vector from Class vector and show parameters
		if ! echo "bone" | "$API/bin/find_in_uml_class_diagrams" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=odie vector add_vector parameters; then
			echo "find_in_uml_class_diagrams failed"
			status=1
			break
		fi
		
		# find in UML class diagrams Method add_vector from Class vector and show return type as odie
		if ! echo "bone" | "$API/bin/find_in_uml_class_diagrams" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=odie vector add_vector returnType; then
			echo "find_in_uml_class_diagrams failed"
			status=1
			break
		fi
		
		# find in UML class diagrams Attributes of type int from Class vector and show them as odie
		if ! echo "bone" | "$API/bin/find_in_uml_class_diagrams" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=odie vector attributes int; then
			echo "find_in_uml_class_diagrams failed"
			status=1
			break
		fi
		
		# find in documents fields of Class vector as odie
		if ! echo "bone" | "$API/bin/find_in_doc" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=odie --class=vector --field=//; then
			echo "find_in_doc failed"
			status=1
			break
		fi
		
		# find in documents Methods add_vector of Class vector
		if ! echo "bone" | "$API/bin/find_in_doc" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=odie --class=vector --method=/add_vector/; then
			echo "find_in_doc failed"
			status=1
			read
			break
		fi
		
		# update source code as garfield
		if ! echo "garfield" | "$API/bin/update_source_files" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield "${API}/examples/vector2.c"; then
			echo "update_source_files failed"
			status=1
			break
		fi
		
		# update UML class diagram file as garfield
		if ! echo "garfield" | "$API/bin/update_uml_class_diagram_files" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield "${API}/examples/vector2_uml_class.json"; then
			echo "update_uml_class_diagram_files failed"
			status=1
			break
		fi

		# update document file as garfield
		if ! echo "garfield" | "$API/bin/update_doc_files" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield --format=binary "${API}/examples/vector2.docx"; then
			echo "update_doc_files failed"
			status=1
			break
		fi
		
		# delete source file vector2.c as garfield
		if ! echo "garfield" | "$API/bin/delete_source_files" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield "examples/vector2.c"; then
			echo "delete_source_files failed"
			status=1
			break
		fi

		# delete UML Class Diagram named 'The Graphic library' as garfield
		if ! echo "garfield" | "$API/bin/delete_uml_class_diagrams" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield 'The Graphic library'; then
			echo "delete_uml_class_diagrams failed"
			status=1
			break
		fi
		
		# delete document file vector2.docx as garfield
		if ! echo "garfield" | "$API/bin/delete_doc_files" "${USER_CMDLINE_OPTS[@]}" "--db=$db" --user=garfield "examples/vector2.docx"; then
			echo "delete_doc_files failed"
			status=1
			break
		fi
		
		echo "Test is finished"
		break
	done
	
	# delete a project
	if ! echo "$ADMIN_PASSWORD" | "$API/bin/delete_project" "${ADMIN_CMDLINE_OPTS[@]}" "--project=$db"; then
		echo "delete_project failed"
		status=1
	fi
	
	# drop user garfield
	if ! echo "$ADMIN_PASSWORD" | "$API/bin/drop_user" "${USER_ADMIN_CMDLINE_OPTS[@]}" garfield; then
		echo "drop_user failed"
		status=1
	fi
	
	# drop user odie
	if ! echo "$ADMIN_PASSWORD" | "$API/bin/drop_user" "${USER_ADMIN_CMDLINE_OPTS[@]}" odie; then
		echo "drop_user failed"
		status=1
	fi

	if [ $status -eq 0 ]; then
		echo "Test PASSED"
	else
		echo "Test FAILED"
	fi
	exit $status
else
	help
	exit 1
fi

