#!/usr/bin/env bash
DIR=$(cd $(dirname $BASH_SOURCE); pwd)
source "${DIR}/../../api/decoder_rest_api.sh"
source "${DIR}/test.conf"

set +o nounset
if [ -z "${STAGES}" ]; then
	STAGES="default"
fi
set -o nounset

set +o errexit

function exit_status
{
	local STATUS=$1
	if [ $STATUS -eq 0 ]; then
		echo "Test PASSED" >&2
	else
		echo "Test FAILED" >&2
	fi

	EXIT_STATUS=$STATUS

	return $STATUS
}

return_status=return

REGEX_STAGES='^'"$(echo -n "${STAGES}" | sed -e 's/  */$|^/g')"'$'
USER_PASSWORD=
USER_KEY=
ADMIN_PASSWORD=
ADMIN_KEY=

while true; do

	if [[ 'default' =~ ${REGEX_STAGES} ]] || [[ 'admin' =~ ${REGEX_STAGES} ]]; then
	
		echo -n "${ADMIN_NAME}'s password: "
		read -s ADMIN_PASSWORD
		
		echo "PKM administrator's login"
		ADMIN_KEY=$(pkm_login "${ADMIN_NAME}" "${ADMIN_PASSWORD}")
		[ $? -eq 0 ] || exit_status 1 || break
		echo

	fi
	
	if [[ 'default' =~ ${REGEX_STAGES} ]] || [[ 'init' =~ ${REGEX_STAGES} ]]; then
	
		USER_PASSWORD=$(generate_random_password)
		echo -n "${USER_PASSWORD}" > user.pwd

		echo "creating user ${USER_NAME} with password '${USER_PASSWORD}'"
		pkm_post_user "${ADMIN_KEY}" ${USER_NAME} "${USER_PASSWORD}" || exit_status 1 || break
		echo

		echo "creating project ${PROJECT_NAME} owned by ${USER_NAME}"
		pkm_post_project "${ADMIN_KEY}" ${PROJECT_NAME} ${USER_NAME} '"Owner","Developer"' || exit_status 1 || break
		echo

		echo "getting project ${PROJECT_NAME}"
		pkm_get_project "${ADMIN_KEY}" ${PROJECT_NAME} || exit_status 1 || break
		echo
		
	fi
	
	if [[ 'default' =~ ${REGEX_STAGES} ]] || [[ 'login' =~ ${REGEX_STAGES} ]]; then
	
		USER_PASSWORD=$(cat user.pwd)
		[ $? -eq 0 ] || exit_status 1 || break

		echo "user's login by ${USER_NAME}"
		USER_KEY=$(pkm_login ${USER_NAME} "${USER_PASSWORD}")
		[ $? -eq 0 ] || exit_status 1 || break
		echo
		
	fi

	if [[ 'default' =~ ${REGEX_STAGES} ]] || [[ 'insert' =~ ${REGEX_STAGES} ]]; then
		
		echo "inserting C source code into project ${PROJECT_NAME} by ${USER_NAME}"
		pkm_post_files "${USER_KEY}" "code/rawsourcecode/$(uri_encode "${PROJECT_NAME}")" 'text' "${DIR}/data" "${DIR}/data/vector2.c" || exit_status 1 || break
		echo

		echo "inserting compile commands in project ${PROJECT_NAME} by ${USER_NAME}"
		pkm_post_json_file "${USER_KEY}" "compile_command/$(uri_encode "${PROJECT_NAME}")" "${DIR}/data/vector2_compile_commands.json" || exit_status 1 || break
		echo

		echo "inserting a documentation file in project ${PROJECT_NAME} by ${USER_NAME}"
		pkm_post_files "${USER_KEY}" "doc/rawdoc/$(uri_encode "${PROJECT_NAME}")" 'binary' "${DIR}/data" "${DIR}/data/vector2.docx" || exit_status 1 || break
		echo

		echo "inserting UML class diagram in project ${PROJECT_NAME} by ${USER_NAME}"
		pkm_post_json_files "${USER_KEY}" "uml/uml_class/$(uri_encode "${PROJECT_NAME}")" "${DIR}/data" "${DIR}/data/vector2_uml_class.json" || exit_status 1 || break
		echo
		
	fi
			
	if [[ 'default' =~ ${REGEX_STAGES} ]] || [[ 'run' =~ ${REGEX_STAGES} ]]; then
		
		echo "inserting data/frama_c_invocation.json by ${USER_NAME}"
		pkm_post_json_files "${USER_KEY}" "invocations/$(uri_encode "${PROJECT_NAME}")" "${DIR}/data" "${DIR}/data/frama_c_invocation.json" || exit_status 1 || break
		echo
		echo "running frama-c (parser, EVA and WP enabled) by ${USER_NAME}"
		run_frama_c "${USER_KEY}" ${PROJECT_NAME} '{"parser":true,"eva":true,"wp":true}' '{"wp":{"wp_fct": ["add_vector","sub_vector","product","norm_square"]}}' 'invocationID=734128cjajdsasd' vector2.c || exit_status 1 || break
		echo

		echo "inserting data/doc_to_asfm_invocation.json by ${USER_NAME}"
		pkm_post_json_files "${USER_KEY}" "invocations/$(uri_encode "${PROJECT_NAME}")" "${DIR}/data" "${DIR}/data/doc_to_asfm_invocation.json" || exit_status 1 || break
		echo
		
		echo "running doc_to_asfm by ${USER_NAME}"
		run_doc_to_asfm "${USER_KEY}" ${PROJECT_NAME} 'invocationID=435454asdaeqrs' vector2.docx || exit_status 1 || break
		echo
		
		echo "inserting data/asfm_to_doc_invocation.json by ${USER_NAME}"
		pkm_post_json_files "${USER_KEY}" "invocations/$(uri_encode "${PROJECT_NAME}")" "${DIR}/data" "${DIR}/data/asfm_to_doc_invocation.json" || exit_status 1 || break
		echo
		
		echo "running asfm_to_doc by ${USER_NAME}"
		run_asfm_to_doc "${USER_KEY}" ${PROJECT_NAME} 'invocationID=983527aertyers' 'The Graphic library' || exit_status 1 || break
		echo
	fi
		
	if [[ 'default' =~ ${REGEX_STAGES} ]] || [[ 'query' =~ ${REGEX_STAGES} ]]; then
	
		echo "getting raw source codes from project ${PROJECT_NAME} by ${USER_NAME}"
		pkm_get "${USER_KEY}" "code/rawsourcecode/$(uri_encode "${PROJECT_NAME}")" || exit_status 1 || break
		echo
		
		echo "getting all files (abbreviated) from project ${PROJECT_NAME} by ${USER_NAME}"
		pkm_get "${USER_KEY}" "files/$(uri_encode "${PROJECT_NAME}")?abbrev=true&type=Code" || exit_status 1 || break
		echo
		
		echo "getting C source code ASTs"
		pkm_get "${USER_KEY}" "code/c/sourcecode/$(uri_encode "${PROJECT_NAME}")?limit=1" || exit_status 1 || break
		echo

		echo "getting source code ASTs"
		pkm_get "${USER_KEY}" "code/sourcecode/$(uri_encode "${PROJECT_NAME}")?skip=1&limit=3" || exit_status 1 || break
		echo
		
		echo "getting C source code ACSL annotations"
		pkm_get "${USER_KEY}" "code/c/annotations/$(uri_encode "${PROJECT_NAME}")?limit=1" || exit_status 1 || break
		echo
		
		echo "getting source code annotations"
		pkm_get "${USER_KEY}" "code/annotations/$(uri_encode "${PROJECT_NAME}")?skip=2&limit=2" || exit_status 1 || break
		echo
		
		echo "getting C source code comments"
		pkm_get "${USER_KEY}" "code/c/comments/$(uri_encode "${PROJECT_NAME}")/vector2.c?limit=0" || exit_status 1 || break
		echo

		echo "getting source code comments"
		pkm_get "${USER_KEY}" "code/comments/$(uri_encode "${PROJECT_NAME}")?skip=1" || exit_status 1 || break
		echo
		
		echo "getting C source code ASTs for a specific file"
		pkm_get "${USER_KEY}" "code/c/sourcecode/$(uri_encode "${PROJECT_NAME}")/vector2.c" || exit_status 1 || break
		echo

		echo "getting C function 'add_vector'"
		pkm_get "${USER_KEY}" "code/c/functions/$(uri_encode "${PROJECT_NAME}")/add_vector" || exit_status 1 || break
		echo

		echo "getting C variable 'null_vector'"
		pkm_get "${USER_KEY}" "code/c/variables/$(uri_encode "${PROJECT_NAME}")/null_vector" || exit_status 1 || break
		echo

		echo "getting C type 'vector'"
		pkm_get "${USER_KEY}" "code/c/types/$(uri_encode "${PROJECT_NAME}")/vector" || exit_status 1 || break
		echo

		echo "getting ASFM documentation from 'vector2.docx'"
		pkm_get "${USER_KEY}" "doc/asfm/docs/$(uri_encode "${PROJECT_NAME}")?filename=$(uri_encode 'vector2.docx')" || exit_status 1 || break
		echo

		echo "getting documentation of class vector"
		pkm_get "${USER_KEY}" "doc/asfm/artefacts/$(uri_encode "${PROJECT_NAME}")?class=vector" || exit_status 1 || break
		echo

		echo "getting UML class diagram"
		pkm_get "${USER_KEY}" "uml/uml_class/$(uri_encode "${PROJECT_NAME}")/$(uri_encode 'The Graphic library')" || exit_status 1 || break
		echo

		echo "getting UML class diagrams"
		pkm_get "${USER_KEY}" "uml/uml_class/$(uri_encode "${PROJECT_NAME}")" || exit_status 1 || break
		echo

		echo "getting class vector from UML class diagram"
		pkm_get "${USER_KEY}" "uml/uml_class/class/$(uri_encode "${PROJECT_NAME}")/vector" || exit_status 1 || break
		echo
		
	fi

	if [[ 'default' =~ ${REGEX_STAGES} ]] || [[ 'trace_recovery' =~ ${REGEX_STAGES} ]]; then
		echo "running trace revovery tool"
		trace_recovery "${USER_KEY}" $(build_trace_recovery_query \
			"${PROJECT_NAME}" \
			"code/c/comments/$(uri_encode "${PROJECT_NAME}")" \
			"[].comments[?loc.pos_start.pos_path == 'vector2.c'] | [*][?global_kind == 'GFun'][] | [0].comments" \
			"code/c/comments/$(uri_encode "${PROJECT_NAME}")" \
			"[].comments[?loc.pos_start.pos_path == 'vector2.c'] | [*][?global_kind == 'GFun'][] | [1].comments" \
			) || exit_status 1 || break
	fi
	
	if [[ 'default' =~ ${REGEX_STAGES} ]] || [[ 'srl' =~ ${REGEX_STAGES} ]]; then
		echo "running SRL"
		run_srl "${USER_KEY}" $(build_srl_query \
			"${PROJECT_NAME}" \
			"code/c/comments/$(uri_encode "${PROJECT_NAME}")" \
			"[].comments[?loc.pos_start.pos_path == 'vector2.c'] | [*][?global_kind == 'GFun'][] | [0].comments" ) || exit_status 1 || break
	fi

	if [[ 'default' =~ ${REGEX_STAGES} ]] || [[ 'ner' =~ ${REGEX_STAGES} ]]; then
		echo "running NER"
		run_ner "${USER_KEY}" $(build_ner_query \
			"${PROJECT_NAME}" \
			"code/c/comments/$(uri_encode "${PROJECT_NAME}")" \
			"[].comments[?loc.pos_start.pos_path == 'vector2.c'] | [*][?global_kind == 'GFun'][] | [0].comments" ) || exit_status 1 || break
	fi
	
	if [[ 'default' =~ ${REGEX_STAGES} ]] || [[ 'query_traceability_matrix' =~ ${REGEX_STAGES} ]]; then
		echo "getting all annotations"
		pkm_get "${USER_KEY}" "annotations/$(uri_encode "${PROJECT_NAME}")" || exit_status 1 || break
		
		echo "getting annotations by path"
		pkm_get "${USER_KEY}" "annotations/$(uri_encode "${PROJECT_NAME}")?path=$(uri_encode "code/c/comments/$(uri_encode "${PROJECT_NAME}")")" || exit_status 1 || break
	fi

	if [[ 'default' =~ ${REGEX_STAGES} ]] || [[ 'semparsing' =~ ${REGEX_STAGES} ]]; then
		echo "running Semantic parsing"
		run_semparsing "${USER_KEY}" $(build_semparsing_query \
			"${PROJECT_NAME}" \
			"vector2.c" \
			"c" \
			"ast") || exit_status 1 || break
			
		echo "getting C source code comments from 'vector2.c'"
		pkm_get "${USER_KEY}" "code/c/comments/$(uri_encode "${PROJECT_NAME}")/vector2.c" || exit_status 1 || break
		echo
	fi
	
	if [[ 'default' =~ ${REGEX_STAGES} ]] || [[ 'query_traceability_matrix' =~ ${REGEX_STAGES} ]]; then
		echo "getting traceability matrix"
		pkm_get "${USER_KEY}" "traceability_matrix/$(uri_encode "${PROJECT_NAME}")" || exit_status 1 || break
		echo
	fi
	
	if [[ 'reset' =~ ${REGEX_STAGES} ]]; then
		MR_PROPER=yes
	fi
	
	exit_status 0
	
	break
done

if [ -n "${USER_PASSWORD}" ]; then
	echo "Tip: ${USER_NAME}'s password is '${USER_PASSWORD}'"
fi

if mr_proper_prompt; then
	echo "cleaning"
	
	echo "deleting user ${USER_NAME}"
	pkm_delete "${ADMIN_KEY}" "user/${USER_NAME}"
	echo
	
	echo "deleting project ${PROJECT_NAME}"
	pkm_delete "${ADMIN_KEY}" "project/$(uri_encode "${PROJECT_NAME}")"
	echo
	
	rm -f user.pwd
fi

exit ${EXIT_STATUS}
