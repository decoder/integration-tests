Git
===

This is a test for the PKM built-in support for [Git](https://git-scm.com/).
The test performs the following actions:

* PKM: project creation
* PKM: user creation
* PKM: administrator and user login
* PKM: cloning repositories on remote servers over HTTPS and SSH
* PKM: getting a file from a Git working tree
* PKM: branching starting with a particular commit
* PKM: getting a Git working tree
* PKM: checking that external editor provided to Git is not functional
* PKM: creating a linked worktree
* PKM: posting a text file in a Git working tree
* PKM: deleting a Git working tree while preserving files (unbound) in the PKM
