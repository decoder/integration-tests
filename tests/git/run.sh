#!/usr/bin/env bash
DIR=$(cd $(dirname $BASH_SOURCE); pwd)
source "${DIR}/../../api/decoder_rest_api.sh"
source "${DIR}/test.conf"

set +o errexit

echo -n "${ADMIN_NAME}'s password: "
read -s ADMIN_PASSWORD
echo
USER_PASSWORD=$(generate_random_password)

function read_git_user_credentials
{
	local -n __GIT_USER_CREDENTIALS_REF__=$1
	__GIT_USER_CREDENTIALS_REF__='['
	local OPEN_TAG='{'
	for REPOSITORY in "${REPOSITORIES[@]}"; do
		__GIT_USER_CREDENTIALS_REF__+="${OPEN_TAG}"
		OPEN_TAG=',{'
		__GIT_USER_CREDENTIALS_REF__+="\"git_remote_url\":"
		__GIT_USER_CREDENTIALS_REF__+=$(echo -n "${REPOSITORY}" | to_json_string)
		if [[ "${REPOSITORY}" =~ ^http ]]; then
			echo -n "Git user's name for ${REPOSITORY}: "
			local GIT_USER_NAME
			read GIT_USER_NAME
			__GIT_USER_CREDENTIALS_REF__+=",\"git_user_name\":"
			__GIT_USER_CREDENTIALS_REF__+=$(echo -n "${GIT_USER_NAME}" | to_json_string)
		else
			echo -n "SSH private key file for ${REPOSITORY}: "
			local GIT_SSH_PRIVATE_KEY_FILE
			read GIT_SSH_PRIVATE_KEY_FILE
			GIT_SSH_PRIVATE_KEY_FILE="${GIT_SSH_PRIVATE_KEY_FILE/#\~/${HOME}}"
			__GIT_USER_CREDENTIALS_REF__+=",\"git_ssh_private_key\":"
			__GIT_USER_CREDENTIALS_REF__+=$(to_json_string "${GIT_SSH_PRIVATE_KEY_FILE}")
		fi
		echo -n "Password for ${REPOSITORY}: "
		local GIT_PASSWORD
		read -s GIT_PASSWORD
		echo
		__GIT_USER_CREDENTIALS_REF__+=",\"git_password\":"
		__GIT_USER_CREDENTIALS_REF__+=$(echo -n "${GIT_PASSWORD}" | to_json_string)
		__GIT_USER_CREDENTIALS_REF__+='}'
	done
	__GIT_USER_CREDENTIALS_REF__+=']'
}

set -o nounset

function exit_status
{
	local STATUS=$1
	if [ $STATUS -eq 0 ]; then
		echo "Test PASSED" >&2
	else
		echo "Test FAILED" >&2
	fi

	EXIT_STATUS=$STATUS

	return $STATUS
}

return_status=return

EXIT_STATUS=0

while true; do

	echo "PKM administrator's login"
	ADMIN_KEY=$(pkm_login "${ADMIN_NAME}" "${ADMIN_PASSWORD}")
	[ $? -eq 0 ] || exit_status 1 || break
	echo

	echo "creating user ${USER_NAME} with password '${USER_PASSWORD}'"
	pkm_post_user "${ADMIN_KEY}" ${USER_NAME} "${USER_PASSWORD}" || exit_status 1 || break
	echo

	echo "creating project ${PROJECT_NAME} owned by ${USER_NAME}"
	pkm_post_project "${ADMIN_KEY}" "${PROJECT_NAME}" ${USER_NAME} '"Owner","Developer"' || exit_status 1 || break
	echo

	echo "user's login by ${USER_NAME}"
	USER_KEY=$(pkm_login ${USER_NAME} "${USER_PASSWORD}")
	[ $? -eq 0 ] || exit_status 1 || break
	echo

	
	echo "update ${USER_NAME}'s Git credentials by ${USER_NAME}"
	read_git_user_credentials GIT_USER_CREDENTIALS
	pkm_put "${USER_KEY}" "user" "{\"name\":\"${USER_NAME}\",\"git_user_credentials\":${GIT_USER_CREDENTIALS}}" || exit_status 1 || break
	echo
	
	COUNT=1
	for REPOSITORY in "${REPOSITORIES[@]}"; do
		echo "git clone ${REPOSITORY} at main${COUNT} into ${PROJECT_NAME} by ${USER_NAME}"
		pkm_git "${USER_KEY}" "${PROJECT_NAME}" "{\"git_commands\":[[\"clone\",\"--separate-git-dir=git${COUNT}\",\"${REPOSITORY}\",\"main${COUNT}\"]]}" || exit_status 1 || break
		COUNT=$((${COUNT}+1))
		echo
	done
	
	if [ ${EXIT_STATUS} -ne 0 ]; then
		break
	fi
	
	echo "getting annotation_scripts/create_annotations.sh from the Git working tree 'main1'"
	pkm_get "${USER_KEY}" "git/files/$(uri_encode "${PROJECT_NAME}")/main1/$(uri_encode annotation_scripts/create_annotations.sh)" || exit_status 1 || break
	echo

# 	exit_status 0
# 	break
	
	COUNT=1
	for REPOSITORY in "${REPOSITORIES[@]}"; do
		echo "cd main${COUNT}; git checkout -b newbranch${COUNT} 754d709dfce3655e8e0892edbe91b48d4bea5ec0 into ${PROJECT_NAME} by ${USER_NAME}"
		pkm_git "${USER_KEY}" "${PROJECT_NAME}" "{\"git_commands\":[[\"cd\",\"main${COUNT}\"],[\"checkout\",\"754d709dfce3655e8e0892edbe91b48d4bea5ec0\"]]}" || exit_status 1 || break
		echo
		COUNT=$((${COUNT}+1))
	done
	
	if [ ${EXIT_STATUS} -ne 0 ]; then
		break
	fi
	
	echo "getting .git/config of the Git directory associated to the Git working tree 'main1'"
	pkm_get "${USER_KEY}" "git/config/$(uri_encode "${PROJECT_NAME}")/main1" || exit_status 1 || break
	echo
	
	echo "updating .git/config of the Git directory associated to the Git working tree 'main1'"
	pkm_put "${USER_KEY}" "git/config/$(uri_encode "${PROJECT_NAME}")/main1" '{"git_config": "[core]\n\trepositoryformatversion = 0\n\tfilemode = true\n\tbare = false\n\tlogallrefupdates = true\n[remote \"origin\"]\n\turl = https://gitlab.ow2.org/decoder/driver_annotations.git\n\tfetch = +refs/heads/*:refs/remotes/origin/*\n[remote \"foo\"]\n\turl = https://foobar.com/test.git\n\tfetch = +refs/heads/*:refs/remotes/origin/*\n[branch \"master\"]\n\tremote = origin\n\tmerge = refs/heads/master\n"}' || exit_status 1 || break
	
	echo "resynchronizing PKM and Git working trees (because of .git/config)"
	pkm_git "${USER_KEY}" "${PROJECT_NAME}" "{\"git_commands\":[]}" || exit_status 1 || break
	echo
	
	echo "get Git working tree 'main1'"
	pkm_get "${USER_KEY}" "git/working_trees/$(uri_encode "${PROJECT_NAME}")/main1" || exit_status 1 || break
	echo

	echo "try to edit main1/.git/config"
	pkm_git "${USER_KEY}" "${PROJECT_NAME}" "{\"git_commands\":[[\"cd\",\"main1\"],[\"config\",\"--edit\"]]}" || exit_status 1 || break
	echo
	
	echo "create a worktree in 'main1'"
	pkm_git "${USER_KEY}" "${PROJECT_NAME}" "{\"git_commands\":[[\"cd\",\"main1\"],[\"worktree\",\"add\",\"../worktree1\"]]}" || exit_status 1 || break
	echo
	
	echo "post hello.txt in the Git working tree 'main2'"
	pkm_post_files "${USER_KEY}" "git/files/$(uri_encode "${PROJECT_NAME}")/main2" 'text' "${DIR}/data" 'hello.txt' || exit_status 1 || break
	echo
	
	echo "delete Git working tree 'main1'"
	pkm_delete "${USER_KEY}" "git/working_trees/$(uri_encode "${PROJECT_NAME}")/main1?dontDeletePkmFiles=true" || exit_status 1 || break
	echo
	
	exit_status 0
  
	break
done

echo "Tip: ${USER_NAME}'s password is '${USER_PASSWORD}'"

if mr_proper_prompt; then
	echo "cleaning"

	echo "deleting user ${USER_NAME}"
	pkm_delete "${ADMIN_KEY}" "user/${USER_NAME}"
	echo

	echo "deleting project"
	pkm_delete "${ADMIN_KEY}" "project/$(uri_encode "${PROJECT_NAME}")"
	echo
fi

exit ${EXIT_STATUS}
