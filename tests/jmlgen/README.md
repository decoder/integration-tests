# JmlGen integration tests

The global test (run.sh) proposes to clone my-thai-star sample project out of Gitlab and store it in the PKM.
Then, it generates JML inside, checks that some JML was inserted, and proposes to delete the project.
By default, it uses the online version of the JmlGen service and PKM hosted at OW2, but can also work with a local installation (needs an alias to localhost, declared in /etc/hosts, called "pkm-api_jmlgen_1").

Note that using the remote PKM (OW2 server) requires to set TARGET environment variable as follows:

```
export TARGET=decoder
```

Then, simply run the test (run.sh), and follow instructions:

```
run.sh
```

Another script (jmlgen_create_project.sh) can be used independently, to clone a Git project into the PKM: it receives 2 arguments, the git clone URL and the PKM destination project name (default values clone my-thai-star sample project into jmlgen-mythaistar).

For example, to clone OW2 sat4j project into the PKM:

```
jmlgen_create_project.sh https://gitlab.ow2.org/sat4j/sat4j.git ow2-sat4j
```

