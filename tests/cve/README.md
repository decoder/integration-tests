cve
===

A small benchmark for testing PKM API about CVE lists:
* PKM: project creation
* PKM: user creation
* PKM: administrator and user login
* PKM: inserting CVE list entries into the PKM
* PKM: deleting a user
* PKM: deleting a project
