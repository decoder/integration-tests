#!/usr/bin/env bash
DIR=$(cd $(dirname $BASH_SOURCE); pwd)
source "${DIR}/../../api/decoder_rest_api.sh"
source "${DIR}/../../api/conf.d/master.conf"
source "${DIR}/test.conf"

set +o errexit

echo -n "${ADMIN_NAME} password: "
read -s ADMIN_PASSWORD
USER_PASSWORD=$(generate_random_password)

function exit_status
{
	local STATUS=$1
	if [ $STATUS -eq 0 ]; then
		echo "Test PASSED" >&2
	else
		echo "Test FAILED" >&2
	fi

	EXIT_STATUS=$STATUS

	return $STATUS
}

return_status=return

while true; do

	echo "PKM administrators login"
	ADMIN_KEY=$(curl --silent --request POST --header "Content-Type: application/json" --data "{\"user_name\": \"${ADMIN_NAME}\", \"user_password\": \"${ADMIN_PASSWORD}\"}" ${PKM_SERVER_URL}/user/login) 
	if [[ $ADMIN_KEY =~ "key" ]]
	then
	  echo "Successfully PKM administrators login ${ADMIN_KEY}"
	else
	  echo "ERROR with admin login ${ADMIN_KEY}"
	  exit_status 1
	  break
	fi
	
	sleep 1

	key_data=$(echo ${ADMIN_KEY:1:${#ADMIN_KEY}-2} | tr --delete \")

	echo "creating user ${USER_NAME} with password ${USER_PASSWORD}"
	create_user_output=$(curl --silent --request POST --header "Content-Type: application/json" --header ${key_data} --data "{\"name\":\"${USER_NAME}\",\"password\":\"${USER_PASSWORD}\"}" ${PKM_SERVER_URL}/user)
	if [[ $create_user_output =~ "Created" ]]
	then
	  echo "Successfully creating user ${create_user_output}"
	else
	  echo "ERROR creating user ${create_user_output}"
	  exit_status 1
	  break
	fi
	
	sleep 1

	echo "creating project ${PROJECT_NAME} owned by ${USER_NAME}"
	create_project_output=$(curl --silent --request POST --header "Content-Type: application/json" --header ${key_data} --data "{\"name\":\"${PROJECT_NAME}\",\"members\":[{\"name\":\"${USER_NAME}\",\"owner\":true,\"roles\":[\"Developer\"]}]}" ${PKM_SERVER_URL}/project) 
	if [[ $create_project_output =~ "Created" ]]
	then
	  echo "Successfully project created  ${create_project_output}"
	else
	  echo "ERROR creating project ${create_project_output}"
	  exit_status 1
	  break
	fi
	
	sleep 1

	echo "users login by ${USER_NAME}"
	USER_KEY=$(curl --silent --request POST --header "Content-Type: application/json" --data "{\"user_name\": \"${USER_NAME}\", \"user_password\": \"${USER_PASSWORD}\"}" ${PKM_SERVER_URL}/user/login)
	if [[ $USER_KEY =~ "key" ]]
	then
	  echo "Successfully User login ${USER_KEY}"
	else
	  echo "ERROR with user login ${USER_KEY}"
	  exit_status 1
	  break
	fi
	
	sleep 1
	
	project_url="${PKM_SERVER_URL}/testar/test_results/${PROJECT_NAME}"
	echo "inserting TESTAR Test Results ${PROJECT_NAME} by ${USER_NAME} inside project ${project_url}"
	TestResultsId=$(curl --silent --request POST --header "accept:application/json" --header ${key_data} --header "Content-Type:application/json" --data @${DIR}/data/ArtefactTestResults_MyThaiStar.json ${project_url}) 
	if [[ $TestResultsId =~ "TESTARTestResults artefactId" ]]
	then
	  echo "TESTAR Test Results inserted! with id ${TestResultsId}"
	else
	  echo "ERROR with TESTAR Test Results ${TestResultsId}"
	  exit_status 1
	  break
	fi
	
	project_url="${PKM_SERVER_URL}/testar/state_model/${PROJECT_NAME}"
	echo "inserting TESTAR State Model ${PROJECT_NAME} by ${USER_NAME} inside project ${project_url}"
	StateModelId=$(curl --silent --request POST --header "accept:application/json" --header ${key_data} --header "Content-Type:application/json" --data @${DIR}/data/ArtefactStateModel_MyThaiStar.json ${project_url})
	if [[ $StateModelId =~ "TESTARStateModels artefactId" ]]
	then
	  echo "TESTAR State Model inserted! with id ${StateModelId}"
	else
	  echo "ERROR with TESTAR State Model ${StateModelId}"
	  exit_status 1
	  break
	fi
	
	project_url="${PKM_SERVER_URL}/log/${PROJECT_NAME}"
	echo "inserting TESTAR LOGS ${PROJECT_NAME} by ${USER_NAME} inside project ${project_url}"
	Logs_Id=$(curl --silent --request POST --header "accept:application/json" --header ${key_data} --header "Content-Type:application/json" --data @${DIR}/data/logs_testar.json ${project_url})
	if [[ $Logs_Id =~ "[\"" ]]
	then
	  echo "TESTAR LOGS inserted! with id ${Logs_Id}"
	else
	  echo "ERROR with TESTAR LOGS ${Logs_Id}"
	  exit_status 1
	  break
	fi

	
	exit_status 0
  
	break
done

echo "Tip: ${USER_NAME}'s password is '${USER_PASSWORD}'"

if mr_proper_prompt; then
  echo "cleaning"
  
  echo "deleting user ${USER_NAME}"
  delete_user_output=$(curl --silent --request DELETE --header "Content-Type: application/json" --header ${key_data} ${PKM_SERVER_URL}/user/${USER_NAME})
  echo
  
  echo "deleting project ${PROJECT_NAME}"
  delete_project_output=$(curl --silent --request DELETE --header "Content-Type: application/json" --header ${key_data} ${PKM_SERVER_URL}/project/${PROJECT_NAME})
  echo
fi

exit ${EXIT_STATUS}
