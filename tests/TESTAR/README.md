TESTAR integration test
=======================

Run the tests with the command:

`./run.sh`

You will need to introduce the admin password.

Description
-----------

This test is using the `PKM_SERVER_URL` value from `api/conf.d/master.conf` to create a new testar user and testar database with the values from `test/TESTAR/test.conf`.

After the testar user and database creation, the JSON Artefacts from `test/TESTAR/data` are being used to test the PKM TESTAR interaction:
- ArtefactStateModel
- ArtefactTestResults
- logs_testar
