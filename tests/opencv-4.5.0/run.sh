#!/usr/bin/env bash
DIR=$(cd $(dirname $BASH_SOURCE); pwd)
source "${DIR}/../../api/decoder_rest_api.sh"
source "${DIR}/test.conf"

set +o errexit

echo -n "${ADMIN_NAME}'s password: "
read -s ADMIN_PASSWORD

if [ -f "${DIR}/user.pwd" ]; then
	USER_PASSWORD=$(cat "${DIR}/user.pwd")
else
	USER_PASSWORD=$(generate_random_password)
	echo -n "${USER_PASSWORD}" > "${DIR}/user.pwd"
fi
OPENCV_VERSION=4.5.0
OPENCV_DIR="${DIR}/data/opencv-${OPENCV_VERSION}"

set -o nounset

function exit_status
{
	local STATUS=$1
	if [ $STATUS -eq 0 ]; then
		echo "Test PASSED" >&2
	else
		echo "Test FAILED" >&2
	fi

	EXIT_STATUS=$STATUS

	return $STATUS
}

return_status=return

while true; do

	echo "PKM administrator's login"
	ADMIN_KEY=$(pkm_login "${ADMIN_NAME}" "${ADMIN_PASSWORD}")
	[ $? -eq 0 ] || exit_status 1 || break
	echo

	echo "creating user ${USER_NAME} with password '${USER_PASSWORD}'"
	pkm_post_user "${ADMIN_KEY}" ${USER_NAME} "${USER_PASSWORD}" || exit_status 1 || break
	echo

	echo "creating project ${PROJECT_NAME} owned by ${USER_NAME}"
	pkm_post_project "${ADMIN_KEY}" "${PROJECT_NAME}" ${USER_NAME} '"Owner","Developer"' || exit_status 1 || break
	echo

	echo "user's login by ${USER_NAME}"
	USER_KEY=$(pkm_login ${USER_NAME} "${USER_PASSWORD}")
	[ $? -eq 0 ] || exit_status 1 || break
	echo

	echo "inserting a documentation file in project ${PROJECT_NAME} by ${USER_NAME}"
	pkm_post_files "${USER_KEY}" "doc/rawdoc/$(uri_encode "${PROJECT_NAME}")" 'binary' "${DIR}/data" "${DIR}/data/mat_opencv.docx" || exit_status 1 || break
	echo
	
	echo "inserting data/doc_to_asfm_invocation.json by ${USER_NAME}"
	pkm_post_json_files "${USER_KEY}" "invocations/$(uri_encode "${PROJECT_NAME}")" "${DIR}/data" "${DIR}/data/doc_to_asfm_invocation.json" || exit_status 1 || break
	echo
	
	echo "running doc_to_asfm by ${USER_NAME}"
	run_doc_to_asfm "${USER_KEY}" ${PROJECT_NAME} 'invocationID=435454asdaeqrs' mat_opencv.docx || exit_status 1 || break
	echo
	
	echo "getting documentation of class _InputArray"
	pkm_get "${USER_KEY}" "doc/asfm/artefacts/$(uri_encode "${PROJECT_NAME}")?class=%2F.%2A_InputArray.%2A%2F" || exit_status 1 || break
	echo
		
	echo "inserting opencv source code into ${PROJECT_NAME} by ${USER_NAME}"
	pkm_post_files "${USER_KEY}" "files/$(uri_encode "${PROJECT_NAME}")" 'text' "${OPENCV_DIR}" "*.cpp" "*.cc" "*.cxx" "*.hpp" "*.h" || exit_status 1 || break
	pkm_post_files "${USER_KEY}" "code/rawsourcecode/$(uri_encode "${PROJECT_NAME}")" 'text' "${OPENCV_DIR}" 'build/modules/core/version_string.inc' || exit_status 1 || break
	echo

	echo "inserting compile commands"
	pkm_post_json_file "${USER_KEY}" "compile_command/$(uri_encode "${PROJECT_NAME}")" "${OPENCV_DIR}/build/compile_commands.json" || exit_status 1 || break
	echo
  
# JOB BELOW LASTS FOR HOURS
# The output from clang is just colossal (hundred of gigabytes).
# Clang hangs on lot of preprocessed files while mangling declaration 'unused' in modules/gapi/include/opencv2/gapi/gkernel.hpp:652:13, e.g. with modules/gapi/test/common/gapi_compoundkernel_tests.ii or modules/gapi/test/common/gapi_core_tests.ii
# It makes oboe (stream JSON parser) fail too because of the short input.
#
# Program received signal SIGSEGV, Segmentation fault.
# (anonymous namespace)::CXXNameMangler::mangleExpression (this=0x7fffffff9730, E=0x0, Arity=4294967295) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/include/clang/AST/Stmt.h:1125
# 1125        return static_cast<StmtClass>(StmtBits.sClass);
# (gdb) bt
# #0  (anonymous namespace)::CXXNameMangler::mangleExpression (this=0x7fffffff9730, E=0x0, Arity=4294967295) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/include/clang/AST/Stmt.h:1125
# #1  0x000000000433386d in (anonymous namespace)::CXXNameMangler::mangleType (T=<optimized out>, this=<optimized out>) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/include/clang/AST/Type.h:3061
# #2  (anonymous namespace)::CXXNameMangler::mangleType (this=this@entry=0x7fffffff9730, T=...) at tools/clang/include/clang/AST/TypeNodes.inc:27
# #3  0x000000000432ee7e in (anonymous namespace)::CXXNameMangler::makeVariableTypeTags (VD=0xa4e7838, this=0x7fffffff98c0) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/include/clang/AST/Decl.h:630
# #4  (anonymous namespace)::CXXNameMangler::mangleName (this=0x7fffffff98c0, ND=0xa4e7838) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/lib/AST/ItaniumMangle.cpp:820
# #5  0x0000000004339b1b in (anonymous namespace)::ItaniumMangleContextImpl::mangleCXXName (this=<optimized out>, D=0xa4e7838, Out=...) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/lib/AST/ItaniumMangle.cpp:4959
# #6  0x000000000434e833 in clang::ASTNameGenerator::Implementation::writeFuncOrVarName (OS=..., D=0xa4e7838, this=0x94bb1d0) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/lib/AST/Mangle.cpp:428
# #7  clang::ASTNameGenerator::Implementation::writeFuncOrVarName (OS=..., D=0xa4e7838, this=0x94bb1d0) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/lib/AST/Mangle.cpp:421
# #8  clang::ASTNameGenerator::Implementation::writeName (OS=..., D=0xa4e7838, this=0x94bb1d0) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/lib/AST/Mangle.cpp:310
# #9  clang::ASTNameGenerator::Implementation::getName[abi:cxx11](clang::Decl const*) (D=0xa4e7838, this=0x94bb1d0) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/lib/AST/Mangle.cpp:330
# #10 clang::ASTNameGenerator::getName[abi:cxx11](clang::Decl const*) (this=this@entry=0x7fffffffae60, D=D@entry=0xa4e7838) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/lib/AST/Mangle.cpp:486
# #11 0x000000000433fab7 in clang::JSONNodeDumper::VisitNamedDecl (ND=<optimized out>, this=0x7fffffffa998) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/lib/AST/JSONNodeDumper.cpp:700
# #12 clang::JSONNodeDumper::VisitNamedDecl (this=0x7fffffffa998, ND=0xa4e7838) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/lib/AST/JSONNodeDumper.cpp:697
# #13 0x000000000434689e in clang::JSONNodeDumper::VisitVarDecl (this=0x7fffffffa998, VD=0xa4e7838) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/lib/AST/JSONNodeDumper.cpp:750
# #14 0x000000000434ccbf in clang::JSONNodeDumper::Visit (this=0x7fffffffa998, D=0xa4e7838) at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/lib/AST/JSONNodeDumper.cpp:124
# #15 0x00000000041720e9 in clang::ASTNodeTraverser<clang::JSONDumper, clang::JSONNodeDumper>::Visit(clang::Decl const*)::{lambda()#1}::operator()() const (this=<optimized out>)
#     at /local/home/gmouchard/llvm-project-llvmorg-10.0.1/clang/include/clang/AST/JSONNodeDumper.h:415
#
#   echo "running frama-clang for whole opencv C++ codebase (but 3rdparty/openexr/Half/half.cpp)"
#   SOURCE_FILE_PATHS=( $(jq '.[].file' < "${DIR}/opencv-4.5.0/build/compile_commands.json" | sed -n 's/^"\([^"]*\)"$/\1/p' | sed -e '/3rdparty\/openexr\/Half\/half\.cpp/d') )
#   run_frama_clang ${USER_KEY} "${PROJECT_NAME}" "${SOURCE_FILE_PATHS[@]}" || exit_status 1 || break
#   echo

# test below make clang hangs and oboe (stream JSON parser) fails because of a short input
#   echo "running frama-clang"
#   run_frama_clang ${USER_KEY} "${PROJECT_NAME}" modules/gapi/test/common/gapi_compoundkernel_tests.cpp || exit_status 1 || break
#   echo

	echo "inserting data/frama_clang_invocation.json by ${USER_NAME}"
	pkm_post_json_files "${USER_KEY}" "invocations/$(uri_encode "${PROJECT_NAME}")" "${DIR}/data" "${DIR}/data/frama_clang_invocation.json" || exit_status 1 || break
	echo
	
	echo "running frama-clang"
	run_frama_clang ${USER_KEY} "${PROJECT_NAME}" '{"includes_system_files":true}' 'invocationID=734128cjajdsasd' modules/core/src/matrix_sparse.cpp || exit_status 1 || break
	echo
  
	echo "getting class 'cv::SparseMat'"
	pkm_get "${USER_KEY}" "code/cpp/artefacts/sourcecode/$(uri_encode "${PROJECT_NAME}")?path=$(uri_encode 'cv::SparseMat')&kind=class" || exit_status 1 || break
	echo
	
	echo "getting method 'cv::SparseMat::SparseMat'"
	pkm_get "${USER_KEY}" "code/cpp/artefacts/sourcecode/$(uri_encode "${PROJECT_NAME}")?path=$(uri_encode 'cv::SparseMat::SparseMat')&kind=method" || exit_status 1 || break
	echo
	
	echo "getting method 'cv::SparseMat::convertTo'"
	pkm_get "${USER_KEY}" "code/cpp/artefacts/sourcecode/$(uri_encode "${PROJECT_NAME}")?path=$(uri_encode 'cv::SparseMat::convertTo')&kind=method" || exit_status 1 || break
	echo
	
	echo "getting field 'cv::SparseMat::hdr'"
	pkm_get "${USER_KEY}" "code/cpp/artefacts/sourcecode/$(uri_encode "${PROJECT_NAME}")?path=$(uri_encode 'cv::SparseMat::hdr')&kind=field" || exit_status 1 || break
	echo

	echo "getting struct 'cv::SparseMat::Hdr'"
	pkm_get "${USER_KEY}" "code/cpp/artefacts/sourcecode/$(uri_encode "${PROJECT_NAME}")?path=$(uri_encode 'cv::SparseMat::Hdr')&kind=struct" || exit_status 1 || break
	echo
	
	echo "getting method 'cv::SparseMat::find'"
	pkm_get "${USER_KEY}" "code/cpp/artefacts/sourcecode/$(uri_encode "${PROJECT_NAME}")?path=$(uri_encode 'cv::SparseMat::find')&kind=method" || exit_status 1 || break
	echo
	
	echo "getting fields from class 'cv::SparseMat'"
	pkm_get "${USER_KEY}" "code/cpp/class/fields/$(uri_encode "${PROJECT_NAME}")/$(uri_encode 'cv::SparseMat')" || exit_status 1 || break
	echo

	echo "getting method 'erase' from class 'cv::SparseMat'"
	pkm_get "${USER_KEY}" "code/cpp/artefacts/sourcecode/$(uri_encode "${PROJECT_NAME}")?path=$(uri_encode 'cv::SparseMat::erase')&kind=method" || exit_status 1 || break
	echo
	
	echo "getting comments from 'modules/core/include/opencv2/core/mat.hpp'"
	pkm_get "${USER_KEY}" "code/cpp/comments/$(uri_encode "${PROJECT_NAME}")/$(uri_encode 'modules/core/include/opencv2/core/mat.hpp')" || exit_status 1 || break
	echo
	
	echo "inserting data/code_to_asfm_invocation.json by ${USER_NAME}"
	pkm_post_json_files "${USER_KEY}" "invocations/$(uri_encode "${PROJECT_NAME}")" "${DIR}/data" "${DIR}/data/code_to_asfm_invocation.json" || exit_status 1 || break
	echo
	
	echo "running code_to_asfm"
	run_code_to_asfm "${USER_KEY}" "${PROJECT_NAME}" 'invocationID=567248udlhtonf' || exit_status 1 || break
	echo
	
	echo "getting documentation of class ipp::IwException of unit ${PROJECT_NAME}"
	pkm_get "${USER_KEY}" "doc/asfm/artefacts/$(uri_encode "${PROJECT_NAME}")?unit=$(uri_encode "/${PROJECT_NAME}/")&class=$(uri_encode '/ipp::IwException/')" || exit_status 1 || break
	echo
	
	echo "getting documentation of methods named 'getString()' of unit ${PROJECT_NAME}"
	pkm_get "${USER_KEY}" "doc/asfm/artefacts/$(uri_encode "${PROJECT_NAME}")?unit=$(uri_encode "/${PROJECT_NAME}/")&method=$(uri_encode '/getString()/')" || exit_status 1 || break
	echo
	
	echo "getting documentation of artefact #12 of unit ${PROJECT_NAME}"
	pkm_get "${USER_KEY}" "doc/asfm/artefacts/$(uri_encode "${PROJECT_NAME}")?unit=$(uri_encode "/${PROJECT_NAME}/")&id=12" || exit_status 1 || break
	echo
	
	echo "inserting data/asfm_to_doc_invocation.json by ${USER_NAME}"
	pkm_post_json_files "${USER_KEY}" "invocations/$(uri_encode "${PROJECT_NAME}")" "${DIR}/data" "${DIR}/data/asfm_to_doc_invocation.json" || exit_status 1 || break
	echo
	
	echo "running asfm_to_doc by ${USER_NAME}"
	run_asfm_to_doc "${USER_KEY}" ${PROJECT_NAME} 'invocationID=983527aertyers' "${PROJECT_NAME}" || exit_status 1 || break
	
  echo "getting logs"
  pkm_get "${USER_KEY}" "log/$(uri_encode "${PROJECT_NAME}")?abbrev=true" || exit_status 1 || break
  echo
  
	exit_status 0
  
	break
done

echo "Tip: ${USER_NAME}'s password is '${USER_PASSWORD}'"

if mr_proper_prompt; then
	echo "cleaning"

	echo "deleting user ${USER_NAME}"
	pkm_delete "${ADMIN_KEY}" "user/${USER_NAME}"
	echo

	echo "deleting project"
	pkm_delete "${ADMIN_KEY}" "project/$(uri_encode "${PROJECT_NAME}")"
	echo
fi

exit ${EXIT_STATUS}
