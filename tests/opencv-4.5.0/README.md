OpenCV 4.5.0
============

[`OpenCV`](https://opencv.org/) is a C++ project which is used to test the following functions:

* PKM: project creation
* PKM: user creation
* PKM: administrator and user login
* PKM: insert [developer documentation](data/mat_opencv.docx)
* ASFM: running ASFM to convert developer documentation to ASFM documentation format
* PKM: querying class _InputArray in the ASFM
* PKM: inserting many C++ source code files from a [prepared `OpenCV 4.5.0`](data/opencv-4.5.0) (Linux/x86_64).
* PKM: inserting the [`compile commands`](./data/opencv-4.5.0/build/compile_commands.json) for OpenCV 4.5.0 (Linux /x86_64)
* Frama-Clang: running Frama-Clang parser on [`modules/core/src/matrix_sparse.cpp`](./data/opencv-4.5.0/modules/core/src/matrix_sparse.cpp)
* PKM: querying the source code
* PKM: deleting a user
* PKM: deleting a project
