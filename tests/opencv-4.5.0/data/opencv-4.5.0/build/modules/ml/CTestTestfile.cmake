# CMake generated Testfile for 
# Source directory: @ROOT@/modules/ml
# Build directory: @ROOT@/build/modules/ml
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(opencv_test_ml "@ROOT@/build/bin/opencv_test_ml" "--gtest_output=xml:opencv_test_ml.xml")
set_tests_properties(opencv_test_ml PROPERTIES  LABELS "Main;opencv_ml;Accuracy" WORKING_DIRECTORY "@ROOT@/build/test-reports/accuracy" _BACKTRACE_TRIPLES "@ROOT@/cmake/OpenCVUtils.cmake;1640;add_test;@ROOT@/cmake/OpenCVModule.cmake;1310;ocv_add_test_from_target;@ROOT@/cmake/OpenCVModule.cmake;1074;ocv_add_accuracy_tests;@ROOT@/modules/ml/CMakeLists.txt;2;ocv_define_module;@ROOT@/modules/ml/CMakeLists.txt;0;")
