# CMake generated Testfile for 
# Source directory: @ROOT@/modules/core
# Build directory: @ROOT@/build/modules/core
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(opencv_test_core "@ROOT@/build/bin/opencv_test_core" "--gtest_output=xml:opencv_test_core.xml")
set_tests_properties(opencv_test_core PROPERTIES  LABELS "Main;opencv_core;Accuracy" WORKING_DIRECTORY "@ROOT@/build/test-reports/accuracy" _BACKTRACE_TRIPLES "@ROOT@/cmake/OpenCVUtils.cmake;1640;add_test;@ROOT@/cmake/OpenCVModule.cmake;1310;ocv_add_test_from_target;@ROOT@/modules/core/CMakeLists.txt;117;ocv_add_accuracy_tests;@ROOT@/modules/core/CMakeLists.txt;0;")
add_test(opencv_perf_core "@ROOT@/build/bin/opencv_perf_core" "--gtest_output=xml:opencv_perf_core.xml")
set_tests_properties(opencv_perf_core PROPERTIES  LABELS "Main;opencv_core;Performance" WORKING_DIRECTORY "@ROOT@/build/test-reports/performance" _BACKTRACE_TRIPLES "@ROOT@/cmake/OpenCVUtils.cmake;1640;add_test;@ROOT@/cmake/OpenCVModule.cmake;1212;ocv_add_test_from_target;@ROOT@/modules/core/CMakeLists.txt;118;ocv_add_perf_tests;@ROOT@/modules/core/CMakeLists.txt;0;")
add_test(opencv_sanity_core "@ROOT@/build/bin/opencv_perf_core" "--gtest_output=xml:opencv_perf_core.xml" "--perf_min_samples=1" "--perf_force_samples=1" "--perf_verify_sanity")
set_tests_properties(opencv_sanity_core PROPERTIES  LABELS "Main;opencv_core;Sanity" WORKING_DIRECTORY "@ROOT@/build/test-reports/sanity" _BACKTRACE_TRIPLES "@ROOT@/cmake/OpenCVUtils.cmake;1640;add_test;@ROOT@/cmake/OpenCVModule.cmake;1213;ocv_add_test_from_target;@ROOT@/modules/core/CMakeLists.txt;118;ocv_add_perf_tests;@ROOT@/modules/core/CMakeLists.txt;0;")
