# CMake generated Testfile for 
# Source directory: @ROOT@/modules/video
# Build directory: @ROOT@/build/modules/video
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(opencv_test_video "@ROOT@/build/bin/opencv_test_video" "--gtest_output=xml:opencv_test_video.xml")
set_tests_properties(opencv_test_video PROPERTIES  LABELS "Main;opencv_video;Accuracy" WORKING_DIRECTORY "@ROOT@/build/test-reports/accuracy" _BACKTRACE_TRIPLES "@ROOT@/cmake/OpenCVUtils.cmake;1640;add_test;@ROOT@/cmake/OpenCVModule.cmake;1310;ocv_add_test_from_target;@ROOT@/cmake/OpenCVModule.cmake;1074;ocv_add_accuracy_tests;@ROOT@/modules/video/CMakeLists.txt;2;ocv_define_module;@ROOT@/modules/video/CMakeLists.txt;0;")
add_test(opencv_perf_video "@ROOT@/build/bin/opencv_perf_video" "--gtest_output=xml:opencv_perf_video.xml")
set_tests_properties(opencv_perf_video PROPERTIES  LABELS "Main;opencv_video;Performance" WORKING_DIRECTORY "@ROOT@/build/test-reports/performance" _BACKTRACE_TRIPLES "@ROOT@/cmake/OpenCVUtils.cmake;1640;add_test;@ROOT@/cmake/OpenCVModule.cmake;1212;ocv_add_test_from_target;@ROOT@/cmake/OpenCVModule.cmake;1075;ocv_add_perf_tests;@ROOT@/modules/video/CMakeLists.txt;2;ocv_define_module;@ROOT@/modules/video/CMakeLists.txt;0;")
add_test(opencv_sanity_video "@ROOT@/build/bin/opencv_perf_video" "--gtest_output=xml:opencv_perf_video.xml" "--perf_min_samples=1" "--perf_force_samples=1" "--perf_verify_sanity")
set_tests_properties(opencv_sanity_video PROPERTIES  LABELS "Main;opencv_video;Sanity" WORKING_DIRECTORY "@ROOT@/build/test-reports/sanity" _BACKTRACE_TRIPLES "@ROOT@/cmake/OpenCVUtils.cmake;1640;add_test;@ROOT@/cmake/OpenCVModule.cmake;1213;ocv_add_test_from_target;@ROOT@/cmake/OpenCVModule.cmake;1075;ocv_add_perf_tests;@ROOT@/modules/video/CMakeLists.txt;2;ocv_define_module;@ROOT@/modules/video/CMakeLists.txt;0;")
