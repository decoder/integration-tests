Preparation work for the verification of AT91SAM9 Watchdog timer Linux driver
=============================================================================

Objective of work
-----------------

The objective of this task is to create a testbench from the driver source code that exercises the main functions of the driver.
The testbench should be verified in Continuous Integration environment with various verification tools (dynamic analysis, static analysis, test-case generation, conformance to a model).
If a source code update accidentally introduces a bug, then the testbench should detect it with adequate tool.

The testbench has to contain

- a minimal abstraction of the linux code to reach the objective
- a model for all called functions in linux
- a broad coverage of the functions of the driver
- a verification of the main properties of the driver

No driver source code modification is allowed.
The testbench elaboration should avoid to modify the parts of the linux code that it requires.

The testbench should provide the code in blue in the schema below

![linux driver](../linux_driver.png)

The initialization part initializes the "variables" that are required to make the driver running.
The linux callback functions are functions of the linux kernel that are called from the driver.
Some of these functions call assembly functions to interact with the hardware device: "readl\_relaxed", "writel\_relaxed", "readl", "writel".
Since the hardware device may change its internal state, the testbench needs to model these changes.

Work done to elaborate the testbench
------------------------------------

A first `tesbench.c` has been elaborated from the common part of standard linux driver testbenches.
It exercises the code of the driver through functions like `wdt_init` and `ping`.

This first file is not sufficient to define an exploitable testbench for the verification.
To complete it, it needs an abstract model of the linux and of the device behavior.

To find the API of the linux required data structures and configuration constants, we have used the compilation error messages. They help a lot to include a minimal set of declarations. These declarations are in the sections "public global variables", "constants", "private declarations".

To find the API of the linux required functions, we have used the linker error messages. These declarations are in the sections "public functions declarations", "variables provided by linker script".

Related documentation [linux watchdog documentation](https://www.kernel.org/doc/html/latest/watchdog/).

Verification strategy
---------------------

The verification should address in sequence the following steps:

1. Use dynamic analysis to verify the absence of Run-Time Error
   for a particular driver function

2. Use dynamic analysis to verify the absence of Run-Time Error
   for all driver functions and extract incremental coverage

3. Use dynamic analysis to verify functional properties
   for a particular driver function

4. Use dynamic analysis to verify complex (temporal) properties
   for all driver functions and extract incremental coverage

5. Use syntactic static analysis to verify Quality Assurance of
   the driver

6. Use semantic static analysis to verify the absence of Run-Time Error
   for all driver functions: (Frama-C/Eva).

7. Use test-case analysis to verify the absence of Run-Time Error
   for all driver functions with high coverage (Testar).

   Frama-C/Eva may generate false alarms (semantic overapproximation).
   Testar may uncover some driver behavior (semantic underapproximation).

   Comparing both coverage may be an indicator to classify the alarms
   in Frama-C and to know when Testar has sufficiently tested the code.

Tasks done
----------

* Elaboration of a complete testbench

  * the testbench compiles, links and does an endless execution.
    The `ping` function is regularily called.

  * the testbench is in good shape to start a Frama-C/Eva verification.

  * the testbench has 0.9kloc for 0.45kloc for the driver. 6 days to
    elaborate it from the driver source code with an external knowledge of
    linux.

  * usage of gcc, ld, cross-compilation tools (for arm target)

* Application of Frama-C/Eva

    ```sh
    frama-c -eva at91sam9_wdt.i testbench.i
    ```

    11 initial errors have been pointed:

    * `__int128` not defined for Frama-C. Replaced by `long int`

    * empty initializers only allowed for GCC/MSVC in `at91sam9_wdt.i`

        ```c
        static const struct of_device_id at91_wdt_dt_ids[] = {
          { .compatible = "atmel,at91sam9260-wdt" },
          {} 
        };
        ```

        replaced by

        ```c
        static const struct of_device_id at91_wdt_dt_ids[] = {
          { .compatible = "atmel,at91sam9260-wdt" },
          { .compatible = "" } 
        };
        ```

    * Return statement with a value in function returning void

        ```c
        return ({int x = 1});
        ```

        issues an error in the Frama-C syntax checker.

        Note that 
        ```c
        return ({int x = 1; x; });
        ```

        returns `1` and is a correct syntax for Frama-C.

    * Length of array is not a constant

        ```c
        ((void)sizeof(char[1 - 2*!!(idx >= __end_of_fixed_addresses)]))
        ```

        const function argument `idx` is not understood as constant by Frama-C.
        This static verification is removed: it will be checked by Frama-C on
        next instruction.

    * field `cgrp' declared with a type containing a flexible array member

        ```c
        struct cgroup {

         struct cgroup_subsys_state self;
         ...

         int ancestor_ids[];
        };

        ```

        has been replaced by

        ```c
        struct cgroup {

         struct cgroup_subsys_state self;
         ...

         int* ancestor_ids;
        };

        ```

        since `ancestor_ids` is not used.

    * empty structs are unsupported

        ```c
        struct uprobes_state {
          int x;
        };
        ```

        to be replaced by

        ```c
        struct uprobes_state {
        };
        ```

* Results of Frama-C/Eva

    ```
    [eva:summary] ====== ANALYSIS SUMMARY ======
      ----------------------------------------------------------------------------
      51 functions analyzed (out of 134): 38% coverage.
      In these functions, 514 statements reached (out of 597): 86% coverage.
      ----------------------------------------------------------------------------
      Some errors and warnings have been raised during the analysis:
        by the Eva analyzer:      0 errors  160 warnings
        by the Frama-C kernel:    0 errors  984 warnings
      ----------------------------------------------------------------------------
      6 alarms generated by the analysis:
           2 invalid memory accesses
           1 integer overflow
           2 accesses to uninitialized left-values
           1 other
      1 of them is a sure alarm (invalid status).
      ----------------------------------------------------------------------------
      No logical properties have been reached by the analysis.
      ----------------------------------------------------------------------------
    ```

    The 2 `accesses to uninitialized left-values` are false alarms.

    The 1 `integer overflow` comes from a constant computation
    whose result is not immediately cast to an `unsigned` type.

    ```
    [eva:alarm] at91sam9_wdt.c:117: Warning: 
      signed overflow. assert 0xa5 << 24 ≤ 2147483647;
    ```

    It is a false alarm if the compiler does not immediately provide a type
    for the computation of `0xa5 << 24` since this compuation can be considered
    as invalid `int` computation.

    ```c
    (__u32)(((0xa5 << 24) | (1 << 0)))
    ```

    To remove this "undefined behavior", the line `at91sam9_wdt.h:21`

    ```c
    #define		AT91_WDT_KEY		(0xa5 << 24)		/* KEY Password */
    ```

    should be replaced by

    ```c
    #define		AT91_WDT_KEY		(0xa5U << 24)		/* KEY Password */
    ```

    which removes the Frama-C alarm.

Challenging points
------------------

* make all the interesting code active for a `Frama-C/Eva` verification

    The linker script is not a valid step for Frama-C. So we have replaced
    the uninitialized function pointer `at91wdt_driver_init` by the 
    call to `__platform_driver_probe` with adequate arguments. We have
    used the debugger `gdb` to find the arguments.

* add ACSL annotations for `Frama-C/WP` to avoid the creation of a testbench.

    Approach to be checked later with the interactive debugger in the loop to
    automatically synthesize annotations with scripting.
    
    Note that the annotations needs to be
    updated for any modification in the driver, whereas the testbench
    approach should be more stable to driver's modifications.

* translate the `main` function in the file `testbench.c` into a GUI for Testar.

* apply the (future) interactive debugger by starting from the functions
    `driver_probe`, `ping`.

* better take the interruptions into account in the analyses.

