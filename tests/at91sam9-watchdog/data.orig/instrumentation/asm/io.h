#ifndef __TESTBENCH_INSTRUMENTATION_ASM_IO_H__
#define __TESTBENCH_INSTRUMENTATION_ASM_IO_H__

#include <linux/types.h>

extern u32 testbench_raw_readl(const volatile void __iomem *addr);
extern void testbench_raw_writel(u32 val, volatile void __iomem *addr);

#define readl_relaxed(c) ({ u32 __r = le32_to_cpu((__force __le32) testbench_raw_readl(c)); __r; })
#define writel_relaxed(v,c) testbench_raw_writel((__force u32) cpu_to_le32(v),c)
#define readl(c) readl_relaxed(c)
#define writel(v,c) writel_relaxed(v,c)

#endif // __TESTBENCH_INSTRUMENTATION_ASM_IO_H__
