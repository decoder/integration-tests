/*******************************************************************************************
 *         Testbench for Linux AT91SAM9 watchdog timer driver                              *
 *                                                                                         *
 * Copyright (c) 2019 CEA (Commissariat à l'énergie atomique et aux énergies alternatives) *
 *                                                                                         *
 * This program is free software; you can redistribute it and/or modify it                 *
 * under the terms and conditions of the GNU General Public License,                       *
 * version 2, as published by the Free Software Foundation.                                *
 *                                                                                         *
 * This program is distributed in the hope it will be useful, but WITHOUT                  *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or                   *
 * FITNESS FOR A PARTICULAR PURPOSE.  See File COPYING for more details.                   *
 *                                                                                         *
 * Authors:                                                                                *
 *   Gilles Mouchard <gilles.mouchard@cea.fr>                                              *
 *   Franck Vedrine <franck.vedrine@cea.fr>                                                *
 *                                                                                         *
 *******************************************************************************************/ 

#include <linux/init.h>
#include <linux/linkage.h>
#include <linux/timer.h>
#include <linux/interrupt.h>
#include <linux/watchdog.h>
#include <linux/moduleparam.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/of.h>

#ifdef MODULE
#error "Testbench is not designed for AT91SAM9 watchdog timer driver as a module"
#endif

#define assert(x)

/*****************************************************************************/
/*                     function provided by libc                             */
/*****************************************************************************/

extern ssize_t write(int fd, const void *buf, size_t count);

/*****************************************************************************/
/*                  variables provided by linker script                      */
/*****************************************************************************/

extern initcall_t __initcall6_start[];

/*****************************************************************************/
/*                         public global variables                           */
/*****************************************************************************/

unsigned long volatile __initdata jiffies;
const struct kernel_param_ops param_ops_int;
const struct kernel_param_ops param_ops_bool;

/*****************************************************************************/
/*                        public functions declarations                      */
/*****************************************************************************/

u32 testbench_raw_readl(const volatile void __iomem *addr);
void testbench_raw_writel(u32 val, volatile void __iomem *addr);
asmlinkage __printf(1, 2) __cold int printk(const char *fmt, ...);
void emergency_restart();
int mod_timer(struct timer_list *timer, unsigned long expires);
__printf(2, 3) void dev_err(const struct device *dev, const char *fmt, ...);
__printf(2, 3) void dev_warn(const struct device *dev, const char *fmt, ...);
int __must_check request_threaded_irq(unsigned int irq, irq_handler_t handler, irq_handler_t thread_fn, unsigned long flags, const char *name, void *dev);
void init_timer_key(struct timer_list *timer, unsigned int flags, const char *name, struct lock_class_key *key);
int watchdog_init_timeout(struct watchdog_device *wdd, unsigned int timeout_parm, struct device *dev);
void _set_bit(int nr, volatile unsigned long * p);
int watchdog_register_device(struct watchdog_device *wdd);
int del_timer(struct timer_list * timer);
unsigned int irq_of_parse_and_map(struct device_node *node, int index);
int of_property_read_u32_index(const struct device_node *np, const char *propname, u32 index, u32 *out_value);
int of_property_read_string(const struct device_node *np, const char *propname, const char **out_string);
struct property *of_find_property(const struct device_node *np, const char *name, int *lenp);
void *devm_kmalloc(struct device *dev, size_t size, gfp_t gfp);
void *devm_ioremap_resource(struct device *dev, struct resource *res);
struct clk *devm_clk_get(struct device *dev, const char *id);
struct resource *platform_get_resource(struct platform_device *pdev, unsigned int type, unsigned int num);
int clk_prepare(struct clk *clk);
int clk_enable(struct clk *clk);
void clk_unprepare(struct clk *clk);
void clk_disable(struct clk *clk);
int __platform_driver_probe(struct platform_driver *drv, int (*probe)(struct platform_device *), struct module *module);
void watchdog_unregister_device(struct watchdog_device *wdd);
void platform_driver_unregister(struct platform_driver *driver);

/*****************************************************************************/
/*                                 constants                                 */
/*****************************************************************************/

#define _MACRO_SCLK_HZ 32768
#define _MACRO_SCLK_DIV 128
static const unsigned long SCLK_HZ = _MACRO_SCLK_HZ; // Slow clock frequency in Hz
static const unsigned long SCLK_DIV = _MACRO_SCLK_DIV;  // Slow clock divider
static const unsigned long WDT_HZ = _MACRO_SCLK_HZ / _MACRO_SCLK_DIV; // Watchdog timer frequency in Hz
static const unsigned int IRQ = 4; // Watchdog timer IRQ number
static const int MIN_HEARTBEAT_SEC = 0;  // Watchdog timer min heartbeat (in device tree)
static const int MAX_HEARTBEAT_SEC = 16; // Watchdog timer max heartbeat (in device tree)
static const unsigned long INIT_TIME = 15 * HZ; // time when watchdog timer driver initialization starts

/*****************************************************************************/
/*                           private declarations                            */
/*****************************************************************************/

struct clk
{
	bool prepared;
	bool enabled;
};

struct at91_wdt
{
	bool mr_written;
	bool reload;
	unsigned long deadline;
	unsigned long time_to_deadline;
	unsigned int irq;
	irq_handler_t irq_handler;
	void *dev;
	
	// CR
	bool wdrstt;
	
	// MR
	bool wdidlehlt;
	bool wddbghlt;
	u32 wdd; // 12-bit
	bool wddis;
	bool wdrproc;
	bool wdrsten;
	bool wdfien;
	u32 wdv; // 12-bit

	// SR
	u32 wderr;
	u32 wdunf;

	// Down counter
	u32 down_counter;
	
	// Output
	bool wdt_fault;
	bool wdt_int;
};

/*****************************************************************************/
/*                         private global variables                          */
/*****************************************************************************/

static bool restart;
static struct platform_device __pdev;
static struct watchdog_device *__wdd;
static struct platform_driver *__drv;
static struct timer_list *__timer;
static unsigned int __timer_flags;
static const char *__timer_name;
static struct lock_class_key *__timer_key;
static bool timer_active;

static struct clk __clk;

static struct device_node __of_node;
static bool of_atmel_disable;
static struct property __of_prop_atmel_disable;
static bool of_atmel_idle_halt;
static struct property __of_prop_atmel_idle_halt;
static bool of_atmel_dbg_halt;
static struct property __of_prop_atmel_dbg_halt;

static u32 at91_wdt_iomem[1024];
static struct resource ioresource_mem0;

static unsigned long jiffies_deadline;
static unsigned long jiffies_time_to_deadline;

static struct at91_wdt wdt;

static unsigned long run_time;

/*****************************************************************************/
/*                       private function declarations                       */
/*****************************************************************************/

static int rnd();
static void wdt_init(struct at91_wdt *wdt);
static void wdt_do_cycle(struct at91_wdt *wdt);
static void wdt_dbg_dump(struct at91_wdt *wdt);
static u32 wdt_read_cr(struct at91_wdt *wdt);
static void wdt_write_cr(u32 val, struct at91_wdt *wdt);
static u32 wdt_read_mr(struct at91_wdt *wdt);
static void wdt_set_mr(u32 val, struct at91_wdt *wdt);
static void wdt_write_mr(u32 val, struct at91_wdt *wdt);
static void wdt_set_sr(u32 val, struct at91_wdt *wdt);
static u32 wdt_read_sr(struct at91_wdt *wdt);
static void wdt_write_sr(u32 val, struct at91_wdt *wdt);
static unsigned long HCF(unsigned long a, unsigned long b);
static unsigned long LCM(unsigned long a, unsigned long b);
static void testbench_run();
static void testbench_init();

/*****************************************************************************/
/*                         public function definitions                       */
/*****************************************************************************/

u32 testbench_raw_readl(const volatile void __iomem *addr)
{
	if((u32 *) addr == &at91_wdt_iomem[0])
	{
		return wdt_read_cr(&wdt);
	}
	else if((u32 *) addr == &at91_wdt_iomem[1])
	{
		return wdt_read_mr(&wdt);
	}
	else if((u32 *) addr == &at91_wdt_iomem[2])
	{
		return wdt_read_sr(&wdt);
	}
	else
	{
		/* unmapped */
		/* @assert false; */
	}
	return rnd();
}

void testbench_raw_writel(u32 val, volatile void __iomem *addr)
{
	if((u32 *) addr == &at91_wdt_iomem[0])
	{
		wdt_write_cr(val, &wdt);
	}
	else if((u32 *) addr == &at91_wdt_iomem[1])
	{
		wdt_write_mr(val, &wdt);
	}
	else if((u32 *) addr == &at91_wdt_iomem[2])
	{
		wdt_write_sr(val, &wdt);
	}
	else
	{
		/* unmapped */
		/* @assert false; */
	}
}

asmlinkage __printf(1, 2) __cold
int printk(const char *fmt, ...)
{
	int count;
	static char buffer[4096];
	va_list ap;

	va_start(ap, fmt);
	count = vsprintf(buffer, fmt, ap);
	va_end(ap);
	
	write(1, buffer, count);
	
	return count;
}

void emergency_restart()
{
	restart = true;
}

int mod_timer(struct timer_list *timer, unsigned long expires)
{
	if(timer == __timer)
	{
		timer->expires = expires;
		return timer_active;
	}
	assert(false);
	return 0;
}

__printf(2, 3)
void dev_err(const struct device *dev, const char *fmt, ...)
{
}

__printf(2, 3)
void dev_warn(const struct device *dev, const char *fmt, ...)
{
}

int __must_check
request_threaded_irq(unsigned int irq, irq_handler_t handler,
       irq_handler_t thread_fn,
       unsigned long flags, const char *name, void *dev)
{
	wdt.irq = irq;
	wdt.irq_handler = handler;
	wdt.dev = dev;
	return 0;
}

void init_timer_key(struct timer_list *timer, unsigned int flags,
      const char *name, struct lock_class_key *key)
{
	__timer = timer;
	__timer_flags = flags;
	__timer_name = name;
	__timer_key = key;
}

int watchdog_init_timeout(struct watchdog_device *wdd,
      unsigned int timeout_parm, struct device *dev)
{
	assert(wdd == __wdd);
	wdd->timeout = 15; // 15 seconds
	return 0;
}

void _set_bit(int nr, volatile unsigned long * p)
{
	*p = (*p) | (1ul << nr);
}

int watchdog_register_device(struct watchdog_device *wdd)
{
	__wdd = wdd;
	return 0;
}

int del_timer(struct timer_list * timer)
{
	if(timer == __timer)
	{
		int ret = timer_active;
		timer_active = false;
		__timer = 0;
		return ret;
	}
	assert(false);
	return 0;
}

unsigned int irq_of_parse_and_map(struct device_node *node, int index)
{
	if(node == &__of_node)
	{
		return IRQ;
	}
	assert(false);
	return 0;
}

int of_property_read_u32_index(const struct device_node *np,
           const char *propname,
           u32 index, u32 *out_value)
{
	if(strcmp("atmel,min-heartbeat-sec", propname) == 0)
	{
		return MIN_HEARTBEAT_SEC;
	}
	else if(strcmp("atmel,max-heartbeat-sec", propname) == 0)
	{
		return MAX_HEARTBEAT_SEC;
	}
	return 0;
}

int of_property_read_string(const struct device_node *np,
       const char *propname,
       const char **out_string)
{
	if(strcmp("atmel,watchdog-type", propname) == 0)
	{
		*out_string = "hardware";
		return 0;
	}
	else if(strcmp("atmel,reset-type", propname) == 0)
	{
		*out_string = "all";
		return 0;
	}
	return -EINVAL;
}

struct property *of_find_property(const struct device_node *np,
      const char *name,
      int *lenp)
{
	if(np == &__of_node)
	{
		if(strcmp(name, __of_prop_atmel_disable.name) == 0)
		{
			if(of_atmel_disable)
			{
				if(lenp) *lenp = __of_prop_atmel_disable.length;
				return &__of_prop_atmel_disable;
			}
		}
		else if(strcmp(name, __of_prop_atmel_idle_halt.name) == 0)
		{
			if(of_atmel_idle_halt)
			{
				if(lenp) *lenp = __of_prop_atmel_idle_halt.length;
				return &__of_prop_atmel_idle_halt;
			}
		}
		else if(strcmp(name, __of_prop_atmel_dbg_halt.name) == 0)
		{
			if(of_atmel_dbg_halt)
			{
				if(lenp) *lenp = __of_prop_atmel_dbg_halt.length;
				return &__of_prop_atmel_dbg_halt;
			}
		}
	}
	return 0;
}

void *devm_kmalloc(struct device *dev, size_t size, gfp_t gfp)
{
	static u8 buffer[4096];
	const unsigned int buffer_size = (sizeof(buffer) * __CHAR_BIT__) / 8;
	if(size <= buffer_size)
	{
		unsigned int i;
		for(i = 0; i < size; ++i)
		{
			buffer[i] = (gfp & ___GFP_ZERO) ? 0 : rnd();
		}
		return (void *) buffer;
	}
	return 0;
}

void *devm_ioremap_resource(struct device *dev, struct resource *res)
{
	if(res == &ioresource_mem0)
	{
		return (void *) &at91_wdt_iomem[0]; // indirection to pointer shall be invalid
	}
	return 0;
}

struct clk *devm_clk_get(struct device *dev, const char *id)
{
	return &__clk;
}


struct resource *platform_get_resource(struct platform_device *pdev,
           unsigned int type, unsigned int num)
{
	if((type == IORESOURCE_MEM) && (num == 0))
	{
		return &ioresource_mem0;
	}
	return 0;
}

int clk_prepare(struct clk *clk)
{
	if(clk == &__clk)
	{
		clk->prepared = true;
		return 0;
	}
	assert(false);
	return -ENOSYS;
}

int clk_enable(struct clk *clk)
{
	if(clk == &__clk)
	{
		if(clk->prepared)
		{
			clk->enabled = true;
			return 0;
		}
		assert(false);
		return 0;
	}
	assert(false);
	return -ENOSYS;
}

void clk_unprepare(struct clk *clk)
{
	if(clk == &__clk)
	{
		clk->prepared = false;
		return;
	}
	assert(false);
}

void clk_disable(struct clk *clk)
{
	if(clk == &__clk)
	{
		if(clk->prepared)
		{
			clk->enabled = false;
			return;
		}
	}
	assert(false);
}

int __platform_driver_probe(struct platform_driver *drv,
  int (*probe)(struct platform_device *), struct module *module)
{
	assert(drv->driver.probe_type != PROBE_PREFER_ASYNCHRONOUS);
	
	__drv = drv;
	
	drv->driver.probe_type = PROBE_FORCE_SYNCHRONOUS;
	drv->prevent_deferred_probe = true;
	drv->driver.suppress_bind_attrs = true;
	
	__pdev.dev.of_node = &__of_node;
	
	int status = (*probe)(&__pdev);
	
	drv->probe = NULL;

	return status;
}

void watchdog_unregister_device(struct watchdog_device *wdd)
{
	if(__wdd == wdd)
	{
		__wdd = 0;
		return;
	}
	
	assert(false);
}

void platform_driver_unregister(struct platform_driver *driver)
{
	if(driver == __drv)
	{
		__drv = 0;
		return;
	}
	
	assert(false);
}

/*****************************************************************************/
/*                       private function definitions                        */
/*****************************************************************************/

static int rnd()
{
	return 1;
}

static void wdt_init(struct at91_wdt *wdt)
{
	wdt->mr_written = 0;
	wdt->reload = 0;
	wdt->deadline = 0;
	wdt->time_to_deadline = 0;
	wdt->irq = 0;
	wdt->irq_handler = 0;
	wdt->dev = 0;
	wdt->wdrstt = 0;
	wdt_set_mr(0x3fff2fff, wdt);
	wdt_set_sr(0x00000000, wdt);
	wdt->down_counter = wdt->wdv;
	wdt->wdt_fault = 0;
	wdt->wdt_int = 0;
	
}

static void wdt_do_cycle(struct at91_wdt *wdt)
{
	u32 current_value = wdt->down_counter;
	
	bool current_value_is_zero = current_value == 0;
	bool current_value_is_less_than_watchdog_delta_value = (current_value <= wdt->wdd);
	if(wdt->wdrstt && !current_value_is_less_than_watchdog_delta_value)
	{
		wdt->wdrstt = 0;
		wdt->wderr = 1;
	}
	wdt->wdt_fault = wdt->wderr || current_value_is_zero;
	if(current_value == 0)
	{
		wdt->wdunf = 1;
	}
	wdt->wdt_int = (wdt->wdunf || wdt->wderr);
	
	if(wdt->wdt_int && wdt->irq_handler)
	{
#ifdef DEBUG_TESTBENCH
		printk("At jiffies=%lu, ", jiffies);
		wdt_dbg_dump(wdt);
		printk(", calling WDT IRQ handler\n");
#endif
		irqreturn_t irq_ret = (*wdt->irq_handler)(wdt->irq, wdt->dev);
		if(irq_ret != IRQ_HANDLED)
		{
			assert(false);
		}
	}
	
	if(!wdt->wddis)
	{
		wdt->down_counter = wdt->reload ? wdt->wdv : (current_value_is_zero ? 0 : (current_value - 1));
		wdt->reload = false;
	}
}

static void wdt_dbg_dump(struct at91_wdt *wdt)
{
	printk(
		"WDT("
		"mr_written=%u"
		",reload=%u"
		",deadline=%lu"
		",time_to_deadline=%lu"
		",irq=%u"
		",irq_handler=%p"
		",dev=%p"
		",wdrstt=%u"
		",wdidlehlt=%u"
		",wddbghlt=%u"
		",wdd=%u"
		",wddis=%u"
		",wdrproc=%u"
		",wdrsten=%u"
		",wdfien=%u"
		",wdv=%u"
		",wderr=%u"
		",wdunf=%u"
		",down_counter=%u"
		",wdt_fault=%u"
		",wdt_int=%u)"
		, wdt->mr_written
		, wdt->reload
		, wdt->deadline
		, wdt->time_to_deadline
		, wdt->irq
		, wdt->irq_handler
		, wdt->dev
		, wdt->wdrstt
		, wdt->wdidlehlt
		, wdt->wddbghlt
		, wdt->wdd
		, wdt->wddis
		, wdt->wdrproc
		, wdt->wdrsten
		, wdt->wdfien
		, wdt->wdv
		, wdt->wderr
		, wdt->wdunf
		, wdt->down_counter
		, wdt->wdt_fault
		, wdt->wdt_int
	);
}

static u32 wdt_read_cr(struct at91_wdt *wdt)
{
	/* @assert false; */
	return 0; /* write-only */
}

static void wdt_write_cr(u32 val, struct at91_wdt *wdt)
{
	/* CR is write-protected: writing 0xa5 to upper 8-bit field, unlock write access */
	if(((val >> 24) & 0xff) == 0xa5)
	{
		if(val & 1)
		{
			wdt->wdrstt = 1;
			wdt->reload = 1;
		}
	}
}

static u32 wdt_read_mr(struct at91_wdt *wdt)
{
	return ((u32)(wdt->wdidlehlt & 1    ) << 29)
	     | ((u32)(wdt->wddbghlt  & 1    ) << 28)
	     | ((u32)(wdt->wdd       & 0xfff) << 16)
	     | ((u32)(wdt->wddis     & 1    ) << 15)
	     | ((u32)(wdt->wdrproc   & 1    ) << 14)
	     | ((u32)(wdt->wdrsten   & 1    ) << 13)
	     | ((u32)(wdt->wdfien    & 1    ) << 12)
	     | ((u32)(wdt->wdv       & 0xfff));
}

static void wdt_set_mr(u32 val, struct at91_wdt *wdt)
{
	wdt->wdidlehlt = (val >> 29) & 1;
	wdt->wddbghlt  = (val >> 28) & 1;
	wdt->wdd       = (val >> 16) & 0xfff;
	wdt->wddis     = (val >> 15) & 1;
	wdt->wdrproc   = (val >> 14) & 1;
	wdt->wdrsten   = (val >> 13) & 1;
	wdt->wdfien    = (val >> 12) & 1;
	wdt->wdv       = val & 0xfff;
}

static void wdt_write_mr(u32 val, struct at91_wdt *wdt)
{
	if(!wdt->mr_written)
	{
		wdt_set_mr(val, wdt);
		wdt->mr_written = 1;
	}
	wdt->reload = 1;
}

static void wdt_set_sr(u32 val, struct at91_wdt *wdt)
{
	wdt->wderr = ((val >> 1) & 1);
	wdt->wdunf = val & 1;
}

static u32 wdt_read_sr(struct at91_wdt *wdt)
{
	u32 val = ((u32)(wdt->wderr & 1) << 1)
	        | ((u32)(wdt->wdunf & 1));
	wdt->wderr = 0;
	wdt->wdunf = 0;
	return val;
}

static void wdt_write_sr(u32 val, struct at91_wdt *wdt)
{
	/* read-only */
	/* @assert false; */
}

static unsigned long HCF(unsigned long a, unsigned long b)
{
	unsigned long r;

	r = a % b;

	while(r)
	{
		a = b;
		b = r;
		r = a % b;
	}    
	return b;
}

static unsigned long LCM(unsigned long a, unsigned long b)
{
    return (a * b) / HCF(a,b);
}

static void testbench_run()
{
	unsigned long delta = (jiffies_time_to_deadline < wdt.time_to_deadline) ? jiffies_time_to_deadline : wdt.time_to_deadline;

	jiffies_time_to_deadline -= delta;
	wdt.time_to_deadline -= delta;
	
	if(delta <= (ULONG_MAX - run_time))
	{
		run_time += delta;
	}
	else
	{
		delta -= (ULONG_MAX - run_time);
		delta -= 1;
		run_time = delta;
	}
	
	if(wdt.time_to_deadline == 0)
	{
		wdt.time_to_deadline = wdt.deadline;
		wdt_do_cycle(&wdt);
	}
	
	if(jiffies_time_to_deadline == 0)
	{
		jiffies_time_to_deadline = jiffies_deadline;
		jiffies = (jiffies < ULONG_MAX) ? (jiffies + 1) : 0;
		
		if(__timer && (jiffies == __timer->expires))
		{
#ifdef DEBUG_TESTBENCH
			printk("At jiffies=%lu, ", jiffies);
			wdt_dbg_dump(&wdt);
			printk(", calling timer IRQ handler\n");
#endif
			(*__timer->function)(__timer->data);
		}
	}
	
	if(wdt.wdt_fault)
	{
#ifdef DEBUG_TESTBENCH
		printk("At jiffies=%lu, ", jiffies);
		wdt_dbg_dump(&wdt);
		printk(", WDT fault\n");
#endif
		restart = true;
	}
}

static void testbench_init()
{
	restart = false;
	jiffies = 0;
	__wdd = 0;
	__drv = 0;
	__timer = 0;
	__timer_flags = 0;
	__timer_name = 0;
	__timer_key = 0;
	timer_active = true;
	__clk.prepared = false;
	__clk.enabled = false;
	of_atmel_disable = false;
	of_atmel_idle_halt = true;
	of_atmel_dbg_halt = true;
	__of_prop_atmel_disable.name = "atmel,disable";
	__of_prop_atmel_disable.length = 0;
	__of_prop_atmel_idle_halt.name = "atmel,idle-halt";
	__of_prop_atmel_idle_halt.length = 0;
	__of_prop_atmel_dbg_halt.name = "atmel,dbg-halt";
	__of_prop_atmel_dbg_halt.length = 0;
	wdt_init(&wdt);
	unsigned long freq_resolution = LCM(HZ, WDT_HZ);
	wdt.deadline = freq_resolution / WDT_HZ;
	jiffies_deadline = freq_resolution / HZ;
	wdt.time_to_deadline = wdt.deadline;
	jiffies_time_to_deadline = jiffies_deadline;
	run_time = 0;
}

/*****************************************************************************/
/*                                   entry point                             */
/*****************************************************************************/

int main(int argc, char *argv[])
{
	while(1)
	{
		testbench_init();
		while(jiffies < INIT_TIME)
		{
			testbench_run();
		}
#ifdef DEBUG_TESTBENCH
		printk("At jiffies=%lu, ", jiffies);
		wdt_dbg_dump(&wdt);
		printk(", starting driver initialization\n");
#endif
		initcall_t at91wdt_driver_init = __initcall6_start[0]; // pointer to at91wdt_driver_init
		if((*at91wdt_driver_init)() == 0) // calls at91wdt_driver_init
		{
			assert(!of_atmel_disable || (__wdd && __wdd->ops));
			assert(__wdd->ops->start);
			if(!__wdd->ops->start) return -1;
			(*__wdd->ops->start)(__wdd);
			while(1)
			{
				testbench_run();
				if(restart)
				{
					// reset
#ifdef DEBUG_TESTBENCH
					printk("At jiffies=%lu, ", jiffies);
					wdt_dbg_dump(&wdt);
					printk(", rebooting\n");
#endif
					break;
				}
			}
		}
	}
	return 0;
}
