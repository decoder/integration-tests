AT91SAM9 Watchdog timer Linux driver testbench
==============================================

Description
-----------

This is a testbench for the AT91SAM9 Watchdog timer (WDT) Linux driver.
The WDT (see AT91SAM9261 is currently present in ATMEL SAMA5 microcontrollers based on an ARM Cortex-A5 processor core and also some older boards with an AT91SAM9 microcontroller based on an ARM 926ej-s processor.
See for example, [SMART AT9260 Datasheet, Chapter 15, page 109-115](http://ww1.microchip.com/downloads/en/devicedoc/atmel-6221-32-bit-arm926ej-s-embedded-microprocessor-sam9260_datasheet.pdf#109).
It has a 12-bit down counter that is decremented each WDT cycle (at 256 Hz frequency).
When down counter reaches zero, WDT asserts reset hardware reset signal, so that system restarts.
Maximum value of down counter (4095) allows a WDT period of up to 16 seconds (max heatbeat) before down counter underflow.
When system operates normally, meaning that it is responsive (there's no deadlock and it is able to fluently handle interrupts), the WDT down counter is reloaded before it reaches zero, so that WDT do not trigger system restart.
The driver is intended to rearm down counter at a conservative period (1/4 or 1/2 of max heartbeat), so that system operates normally.

The testbench source code is in the [`data.orig`](./data.orig) directory.
To prepare the source code for Frama-C, following the instructions of ['TestbenchPreparation.md'](./TestbenchPreparation.md), the patch file [`fc.patch`](./fc.patch) was applied, which produced the source code in the [`data`](./data) directory.

The test exercises the followings in DECODER EU Project tool-chain:

* PKM: project creation
* PKM: user creation
* PKM: administrator and user login
* PKM: inserting many C source code and header files.
* PKM: inserting the [`compile commands`](./data/compile_commands.json) (Linux x86_64)
* Frama-C: running Frama-C parser, EVA and WP on [`testbench.c`](./data/testbench.c) and [`at91sam9_wdt.c`](./data/at91sam9_wdt.c)
* PKM: deleting a user
* PKM: deleting a project

Content
-------

- makefile       : a bare makefile for compiling on an x86 host or 
                   cross-compiling for ARM host
- at91sam9_wdt.c : the unmodified Linux device driver C source code
- at91sam9_wdt.h : the unmodified Linux device driver C header
- testbench.c    : the testbench C source code that replace the Linux kernel.
                   The testbench calls the driver init routine and implements the stubs implementing the Linux API that the driver uses.
                   It also provides instrumentation functions for low level I/O.
- testbench.lds  : the linker script intended to make available driver init routine for the testbench
- compiler       : directory containing GCC compiler related headers (`stdarg.h`)
- generated      : directory containing generated C headers obtained from a vmlinux build
    - x86        : ...for a x86 host
    - arm        : ...for an ARM target
- linux-4.9      : directory containing unmodified Linux headers which the driver depends on
- instrumentation: directory containing I/O instrumentation macro and function declarations (`instrumentation/asm/io.h` hides `arch/x86/include/asm/io.h` and `arch/arm/include/asm/io.h`)

Using the testbench
-------------------

The Makefile is intended for an x86 host, and can target optionally an ARM target with the help of a cross-compiler and a ARM emulator.
The Makefile builds testbench but also generate preprocessed source code.
It can use a Linux 4.9 source tree pointed by LINUXHOME_4_9 environment variable for the Linux headers or use its own copy of these files.
The Makefile relies on GCC command to specify the include search directory order to override low level I/O when compiling the device driver.

### x86 host

Compile the testbench:

	$ make
	
You can optionally enable testbench debug messages:

	$ make EXTRA_CPPFLAGS=-DDEBUG_TESTBENCH
	
Run the testbench

	$ ./testbench
	
### ARM target

You need a ARM Cortex A5 cross-compiler (preferably GCC) with a working libc, see [crosstool-NG](https://crosstool-ng.github.io)

Cross-compile the testbench:

	$ make CROSS_COMPILE=arm-cortex_a5-linux-gnueabihf-
	
You can optionally enable testbench debug messages:

	$ make CROSS_COMPILE=arm-cortex_a5-linux-gnueabihf- EXTRA_CPPFLAGS=-DDEBUG_TESTBENCH

Run the testbench:

	$ qemu-arm ./testbench

You can debug the testbench:

	$ qemu-arm -g 1234
	
	$ arm-cortex_a5-linux-gnueabihf-gdb ./testbench
	(gdb) target remote :1234

Principle of operation
----------------------

### General operation

The testbench relies on a Co-emulation of both the driver and the device.
The driver have several hooks that are an initialization routine, a watchdog interrupt handler, and a timer handler.
The testbench provides the driver with an API to install interrupt handler, timer handler, read device tree properties.
The testbench also substitutes low level I/O macro with its own macros to catch device driver memory mapped access to the device, so that it can not only emulate the watchdog device side effects in memory from the device driver point of view, but also trigger a call to the driver interrupt handler when WDT requests an interrupt.
The testbench has a scheduler (in function `testbench_run`) that calls the driver hooks (timer hook and WDT IRQ handler hook in sub-function `wdt_do_cycle`) at precise points in time.
However the Linux kernel time (`jiffies`) period (`HZ`) and the watchdog device clock period (`WDT_HZ`) are not necessarily multiple, so the testbench uses a common clock for which these two clocks are multiple.
The common clock frequency is the least common multiple of these two clock frequencies.

### Test Sequence

Because the watchdog device driver is not necessarily initialized first (Linux kernel and other code have been executed), the testbench starts scheduling timer interrupts and the watchdog device until a given point in time (see variable `INIT_TIME`), and then only calls the watchdog driver initialization routine.
The watchdog driver initialization routine installs a timer handler and optionally a watchdog interrupt handler.
Once the driver initialization has finished successfully, the testbench starts scheduling timer interrupts and the watchdog device, until a restart occurs.
A restart should never occur if system operates normally, so the testbench should run indefinitely.
