#!/usr/bin/env bash
DIR=$(cd $(dirname $BASH_SOURCE); pwd)
source "${DIR}/../../api/decoder_rest_api.sh"
source "${DIR}/test.conf"

set +o errexit

echo -n "${ADMIN_NAME}'s password: "
read -s ADMIN_PASSWORD
USER1_PASSWORD=$(generate_random_password)
USER2_PASSWORD=$(generate_random_password)

function exit_status
{
	local STATUS=$1
	if [ $STATUS -eq 0 ]; then
		echo "Test PASSED" >&2
	else
		echo "Test FAILED" >&2
	fi

	EXIT_STATUS=$STATUS

	return $STATUS
}

return_status=return

while true; do

	echo "PKM administrator's login"
	ADMIN_KEY=$(pkm_login "${ADMIN_NAME}" "${ADMIN_PASSWORD}")
	[ $? -eq 0 ] || exit_status 1 || break
	echo

	echo "creating user ${USER1_NAME} with password '${USER1_PASSWORD}'"
	pkm_post_user "${ADMIN_KEY}" ${USER1_NAME} "${USER1_PASSWORD}" || exit_status 1 || break
	echo

	echo "creating user ${USER2_NAME} with password '${USER2_PASSWORD}'"
	pkm_post_user "${ADMIN_KEY}" ${USER2_NAME} "${USER2_PASSWORD}" || exit_status 1 || break
	echo

	echo "creating project ${PROJECT1_NAME} owned by ${USER1_NAME}"
	pkm_post_project "${ADMIN_KEY}" ${PROJECT1_NAME} ${USER1_NAME} '"Owner","Developer"' || exit_status 1 || break
	echo

	echo "creating project ${PROJECT2_NAME} owned by ${USER2_NAME}"
	pkm_post_project "${ADMIN_KEY}" ${PROJECT2_NAME} ${USER2_NAME} '"Owner","Maintainer"' || exit_status 1 || break
	echo
  
	echo "update ${USER1_NAME}'s password"
	pkm_put "${ADMIN_KEY}" "user" "{\"name\":\"${USER1_NAME}\",\"password\":\"${USER1_PASSWORD}\"}" || exit_status 1 || break
	echo

	echo "updating project ${PROJECT1_NAME} metadata by admin"
	pkm_put "${ADMIN_KEY}" "project" "{\"name\":\"${PROJECT1_NAME}\",\"version\":1}" || exit_status 1 || break
	echo

	echo "user's login by ${USER1_NAME}"
	USER1_KEY1=$(pkm_login ${USER1_NAME} "${USER1_PASSWORD}")
	[ $? -eq 0 ] || exit_status 1 || break
	echo
	
	echo "updating user's data by ${USER1_NAME}"
	pkm_put "${USER1_KEY1}" "user" "{\"password\":\"${USER1_PASSWORD}\",\"phone\":\"+666\",\"name\":\"${USER1_NAME}\",\"last_name\":\"cat\",\"first_name\":\"${USER1_NAME}\",\"email\":\"${USER1_NAME}@catcorp.com\"}" || exit_status 1 || break
	echo

	echo "getting user ${USER1_NAME} by ${USER1_NAME}"
	pkm_get "${USER1_KEY1}" "user/${USER1_NAME}" || exit_status 1 || break
	echo

	echo "getting user ${USER2_NAME} by ${USER1_NAME}"
	pkm_get "${USER1_KEY1}" "user/${USER2_NAME}" || exit_status 1 || break
	echo

	echo "duplicate user's session by ${USER1_NAME}"
	USER1_KEY2=$(dup_user_session "${USER1_KEY1}")
	[ $? -eq 0 ] || exit_status 1 || break
	echo
	
	echo "closing first user's session by ${USER1_NAME}"
	pkm_logout "${USER1_KEY1}" || exit_status 1 || break
	echo
	
	echo "adding membership to ${USER2_NAME} in project ${PROJECT1_NAME} by ${USER1_NAME}"
	pkm_put "${USER1_KEY2}" "user" "{\"name\":\"${USER2_NAME}\",\"roles\":[{\"db\":\"${PROJECT1_NAME}\",\"role\":\"Reviewer\"},{\"db\":\"${PROJECT2_NAME}\",\"role\":\"Maintainer\"},{\"db\":\"${PROJECT2_NAME}\",\"role\":\"Owner\"}]}" || exit_status 1 || break
	echo
	
	echo "adding ownership to ${USER2_NAME} in project ${PROJECT1_NAME} by ${USER1_NAME}"
	pkm_put "${USER1_KEY2}" "project" "{\"name\":\"${PROJECT1_NAME}\",\"version\":1,\"members\":[{\"name\":\"${USER1_NAME}\",\"owner\":true,\"roles\":[\"Developer\",\"Owner\"]},{\"name\":\"${USER2_NAME}\",\"owner\":false,\"roles\":[\"Owner\",\"Reviewer\"]}]}" || exit_status 1 || break

	echo "getting metadata of project ${PROJECT1_NAME} by ${USER1_NAME}"
	pkm_get "${USER1_KEY2}" "project/${PROJECT1_NAME}" || exit_status 1 || break
	echo

	echo "getting ${USER1_NAME}'s projects by ${USER1_NAME}"
	pkm_get "${USER1_KEY2}" "user/${USER1_NAME}/project" || exit_status 1 || break
	echo

	echo "getting projects by ${USER1_NAME}"
	pkm_get "${USER1_KEY2}" "project" || exit_status 1 || break
	echo

	echo "user's login by ${USER2_NAME}"
	USER2_KEY=$(pkm_login ${USER2_NAME} "${USER2_PASSWORD}")
	[ $? -eq 0 ] || exit_status 1 || break
	echo

	exit_status 0
  
	break
done

echo "Tip: ${USER1_NAME}'s password is '${USER1_PASSWORD}'"
echo "Tip: ${USER2_NAME}'s password is '${USER2_PASSWORD}'"

if mr_proper_prompt; then
  echo "cleaning"
  
  echo "deleting user ${USER1_NAME}"
  pkm_delete "${ADMIN_KEY}" "user/${USER1_NAME}"
  echo
  
  echo "deleting project ${PROJECT1_NAME}"
  pkm_delete "${ADMIN_KEY}" "project/${PROJECT1_NAME}"
  echo
  
  echo "deleting project ${PROJECT2_NAME}"
  pkm_delete "${ADMIN_KEY}" "project/${PROJECT2_NAME}"
  echo
  
  echo "deleting user ${USER2_NAME}"
  pkm_delete "${ADMIN_KEY}" "user/${USER2_NAME}"
  echo
fi

exit ${EXIT_STATUS}
