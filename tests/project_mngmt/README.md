project_mngmt
=============

project_mngmt is a test about project management (project, users, ...).

It tests the following functions:

* PKM: project creation
* PKM: user creation
* PKM: administrator and user login
* PKM: updating user's password and informations
* PKM: granting roles into a project
* PKM: getting project informations
* PKM: deleting a user
* PKM: deleting a project
