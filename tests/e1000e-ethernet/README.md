Intel e1000e Ethernet Linux driver testbench
============================================

Description
-----------

This is a testbench for the Intel e1000e Ethernet Linux driver.

The test exercises the followings in DECODER EU Project tool-chain:

* PKM: project creation
* PKM: user creation
* PKM: administrator and user login
* PKM: inserting many C source code and header files.
* PKM: inserting the [`compile commands`](./data/compile_commands.json) (Linux x86_64)
* Frama-C: running Frama-C parser, EVA and WP on [`80003es2lan.c`](./data/80003es2lan.c), [`82571.c`](./data/82571.c), [`ethtool.c`](./data/ethtool.c), [`ich8lan.c`](./data/ich8lan.c), [`mac.c`](./data/mac.c), [`manage.c`](./data/manage.c), [`netdev.c`](./data/netdev.c), [`nvm.c`](./data/nvm.c), [`param.c`](./data/param.c), [`phy.c`](./data/phy.c), [`ptp.c`](./data/ptp.c), and [`testbench.c`](./data/testbench.c)
* PKM: deleting a user
* PKM: deleting a project
