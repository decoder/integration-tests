#!/usr/bin/env bash
DIR=$(cd $(dirname $BASH_SOURCE); pwd)
source "${DIR}/../../api/decoder_rest_api.sh"
source "${DIR}/test.conf"

set +o errexit

echo -n "${ADMIN_NAME}'s password: "
read -s ADMIN_PASSWORD
USER_PASSWORD=$(generate_random_password)
DATA_DIR="${DIR}/data"

set -o nounset

function exit_status
{
	local STATUS=$1
	if [ $STATUS -eq 0 ]; then
		echo "Test PASSED" >&2
	else
		echo "Test FAILED" >&2
	fi

	EXIT_STATUS=$STATUS

	return $STATUS
}

return_status=return

while true; do

	echo "PKM administrator's login"
	ADMIN_KEY=$(pkm_login "${ADMIN_NAME}" "${ADMIN_PASSWORD}")
	[ $? -eq 0 ] || exit_status 1 || break
	echo

	echo "creating user ${USER_NAME} with password '${USER_PASSWORD}'"
	pkm_post_user "${ADMIN_KEY}" ${USER_NAME} "${USER_PASSWORD}" || exit_status 1 || break
	echo

	echo "creating project ${PROJECT_NAME} owned by ${USER_NAME}"
	pkm_post_project "${ADMIN_KEY}" "${PROJECT_NAME}" ${USER_NAME} '"Owner","Developer"' || exit_status 1 || break
	echo

	echo "user's login by ${USER_NAME}"
	USER_KEY=$(pkm_login ${USER_NAME} "${USER_PASSWORD}")
	[ $? -eq 0 ] || exit_status 1 || break
	echo

	echo "inserting e1000e-ethernet source code into ${PROJECT_NAME} by ${USER_NAME}"
	pkm_post_files "${USER_KEY}" "files/$(uri_encode "${PROJECT_NAME}")" 'text' "${DATA_DIR}" "*.c" "*.h" || exit_status 1 || break
	echo

	echo "inserting compile commands"
	pkm_post_json_file "${USER_KEY}" "compile_command/$(uri_encode "${PROJECT_NAME}")" "${DATA_DIR}/compile_commands.json" || exit_status 1 || break
	echo
  
  echo "running frama-c (parser, EVA and WP enabled) by ${USER_NAME}"
  run_frama_c "${USER_KEY}" ${PROJECT_NAME} '{"parser":true,"eva":true,"wp":true}' '{"args":["-machdep","x86_64","-cpp-extra-args=\"-D__int128=long long\""]}' -- 80003es2lan.c 82571.c ethtool.c ich8lan.c mac.c manage.c netdev.c nvm.c param.c phy.c ptp.c testbench.c #|| exit_status 1 || break
  echo
  
  echo "getting logs"
  pkm_get "${USER_KEY}" "log/$(uri_encode "${PROJECT_NAME}")" || exit_status 1 || break
  echo
  
	exit_status 0
  
	break
done

echo "Tip: ${USER_NAME}'s password is '${USER_PASSWORD}'"

if mr_proper_prompt; then
	echo "cleaning"

	echo "deleting user ${USER_NAME}"
	pkm_delete "${ADMIN_KEY}" "user/${USER_NAME}"
	echo

	echo "deleting project"
	pkm_delete "${ADMIN_KEY}" "project/$(uri_encode "${PROJECT_NAME}")"
	echo
fi

exit ${EXIT_STATUS}
