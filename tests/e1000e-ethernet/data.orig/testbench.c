#include <linux/init.h>
#include <linux/linkage.h>
#include <linux/timer.h>
#include <linux/interrupt.h>
#include <linux/device.h>
#include <linux/pci.h>
#include <linux/ipv6.h>
#include <linux/pm_qos.h>
#include <linux/ptp_clock_kernel.h>
#include <linux/ptp_classify.h>
#include <linux/timecounter.h>

#ifdef MODULE
#error "Testbench is not designed for E1000E network driver as a module"
#endif

#define assert(x)

/*****************************************************************************/
/*                     function provided by libc                             */
/*****************************************************************************/

extern ssize_t write(int fd, const void *buf, size_t count);
extern void *malloc(size_t size);
extern void *calloc(size_t nmemb, size_t size);
extern void free(void *ptr);

/*****************************************************************************/
/*                  variables provided by linker script                      */
/*****************************************************************************/

extern initcall_t __initcall6_start[];

/*****************************************************************************/
/*                         public global variables                           */
/*****************************************************************************/

struct cpuinfo_x86 boot_cpu_data;
struct task_struct *current_task ____cacheline_aligned;
struct dma_map_ops *dma_ops;
unsigned long volatile __initdata jiffies;
unsigned long page_offset_base;
const struct kernel_param_ops param_array_ops;
const struct kernel_param_ops param_ops_int;
const struct kernel_param_ops param_ops_uint;
unsigned long phys_base;
struct pv_irq_ops pv_irq_ops;
struct workqueue_struct *system_wq;
unsigned long vmemmap_base;

/*****************************************************************************/
/*                        public functions declarations                      */
/*****************************************************************************/

unsigned int testbench_raw_readl(const volatile void __iomem *addr);
void testbench_raw_writel(unsigned int val, volatile void __iomem *addr);
unsigned short testbench_raw_readw(const volatile void __iomem *addr);
void testbench_raw_writew(unsigned short val, volatile void __iomem *addr);

struct net_device *alloc_etherdev_mqs(int sizeof_priv, unsigned int txqs, unsigned int rxqs);
struct page *alloc_pages_current(gfp_t gfp_mask, unsigned order);
struct sk_buff *__alloc_skb(unsigned int size, gfp_t priority, int flags, int node);
bool arch_dma_alloc_attrs(struct device **dev, gfp_t *gfp);
bool cancel_delayed_work_sync(struct delayed_work *dwork);
bool cancel_work_sync(struct work_struct *work);
int _cond_resched(void);
void __const_udelay(unsigned long xloops);
void consume_skb(struct sk_buff *skb);
struct system_counterval_t convert_art_to_tsc(cycle_t art);
unsigned long __must_check _copy_from_user(void *to, const void __user *from, unsigned n);
unsigned long __must_check _copy_to_user(void __user *to, const void *from, unsigned n);
u32 __pure crc32_le(u32 crc, unsigned char const *p, size_t len);
__sum16 csum_ipv6_magic(const struct in6_addr *saddr, const struct in6_addr *daddr, __u32 len, __u8 proto, __wsum csum);
void delayed_work_timer_fn(unsigned long __data);
int del_timer_sync(struct timer_list *timer);
__printf(2, 3) void dev_err(const struct device *dev, const char *fmt, ...);
void device_set_wakeup_capable(struct device *dev, bool capable);
int device_set_wakeup_enable(struct device *dev, bool enable);
int device_wakeup_enable(struct device *dev);
__printf(2, 3) void _dev_info(const struct device *dev, const char *fmt, ...);
void __dev_kfree_skb_any(struct sk_buff *skb, enum skb_free_reason reason);
void __dev_kfree_skb_irq(struct sk_buff *skb, enum skb_free_reason reason);
void dev_notice(const struct device *dev, const char *fmt, ...);
unsigned long dev_trans_start(struct net_device *dev);
__printf(2, 3) void dev_warn(const struct device *dev, const char *fmt, ...);
void disable_irq(unsigned int irq);
int dma_supported(struct device *hwdev, u64 mask);
void dql_completed(struct dql *dql, unsigned int count);
void dql_reset(struct dql *dql);
__printf(3, 4) void __dynamic_netdev_dbg(struct _ddebug *descriptor, const struct net_device *dev, const char *fmt, ...);
void enable_irq(unsigned int irq);
u32 ethtool_op_get_link(struct net_device *dev);
int ethtool_op_get_ts_info(struct net_device *dev, struct ethtool_ts_info *eti);
__be16 eth_type_trans(struct sk_buff *skb, struct net_device *dev);
int eth_validate_addr(struct net_device *dev);
unsigned long find_first_bit(const unsigned long *addr, unsigned long size);
unsigned long find_next_bit(const unsigned long *addr, unsigned long size, unsigned long offset);
void free_irq(unsigned int irq, void *dev_id);
void free_netdev(struct net_device *dev);
int get_device_system_crosststamp(int (*get_time_fn)(ktime_t *device_time, struct system_counterval_t *system_counterval, void *ctx), void *ctx, struct system_time_snapshot *history, struct system_device_crosststamp *xtstamp);
void init_timer_key(struct timer_list *timer, unsigned int flags, const char *name, struct lock_class_key *key);
void __iomem *ioremap(resource_size_t offset, unsigned long size);
void iounmap(volatile void __iomem *addr);
void kfree(const void *objp);
void *__kmalloc(size_t size, gfp_t flags) __assume_kmalloc_alignment __malloc;
char *kstrdup(const char *s, gfp_t gfp) __malloc;
ktime_t ktime_get_with_offset(enum tk_offsets offs);
int mod_timer(struct timer_list *timer, unsigned long expires);
ssize_t __modver_version_show(struct module_attribute *mattr, struct module_kobject *mk, char *buf);
void msleep(unsigned int msecs);
unsigned long msleep_interruptible(unsigned int msecs);
void mutex_lock(struct mutex *lock);
void mutex_unlock(struct mutex *lock);
struct sk_buff *__napi_alloc_skb(struct napi_struct *napi, unsigned int length, gfp_t gfp_mask);
void napi_complete_done(struct napi_struct *n, int work_done);
void napi_disable(struct napi_struct *n);
gro_result_t napi_gro_receive(struct napi_struct *napi, struct sk_buff *skb);
void __napi_schedule(struct napi_struct *n);
struct sk_buff *__netdev_alloc_skb(struct net_device *dev, unsigned int length, gfp_t gfp_mask);
__printf(2, 3) void netdev_err(const struct net_device *dev, const char *format, ...);
__printf(2, 3) void netdev_info(const struct net_device *dev, const char *format, ...);
void netdev_rss_key_fill(void *buffer, size_t len);
__printf(2, 3) void netdev_warn(const struct net_device *dev, const char *format, ...);
void netif_carrier_off(struct net_device *dev);
void netif_carrier_on(struct net_device *dev);
void netif_device_attach(struct net_device *dev);
void netif_device_detach(struct net_device *dev);
void netif_napi_add(struct net_device *dev, struct napi_struct *napi, int (*poll)(struct napi_struct *, int), int weight);
void netif_schedule_queue(struct netdev_queue *txq);
void netif_tx_wake_queue(struct netdev_queue *dev_queue);
int net_ratelimit(void);
struct timespec ns_to_timespec(const s64 nsec);
netdev_features_t passthru_features_check(struct sk_buff *skb, struct net_device *dev, netdev_features_t features);
int pci_bus_read_config_word(struct pci_bus *bus, unsigned int devfn, int where, u16 *val);
int pci_bus_write_config_word(struct pci_bus *bus, unsigned int devfn, int where, u16 val);
int pci_cleanup_aer_uncorrect_error_status(struct pci_dev *dev);
void pci_clear_master(struct pci_dev *dev);
bool pci_dev_run_wake(struct pci_dev *dev);
void pci_disable_device(struct pci_dev *dev);
void pci_disable_link_state(struct pci_dev *pdev, int state);
void pci_disable_link_state_locked(struct pci_dev *pdev, int state);
void pci_disable_msi(struct pci_dev *dev);
void pci_disable_msix(struct pci_dev *dev);
int pci_disable_pcie_error_reporting(struct pci_dev *dev);
int pcie_capability_clear_and_set_word(struct pci_dev *dev, int pos, u16 clear, u16 set);
int pcie_capability_read_word(struct pci_dev *dev, int pos, u16 *val);
int pcie_capability_write_word(struct pci_dev *dev, int pos, u16 val);
int __must_check pci_enable_device_mem(struct pci_dev *dev);
int pci_enable_msi_range(struct pci_dev *dev, int minvec, int maxvec);
int pci_enable_msix_range(struct pci_dev *dev, struct msix_entry *entries, int minvec, int maxvec);
int pci_enable_pcie_error_reporting(struct pci_dev *dev);
int __pci_enable_wake(struct pci_dev *dev, pci_power_t state, bool runtime, bool enable);
int pci_prepare_to_sleep(struct pci_dev *dev);
int __must_check __pci_register_driver(struct pci_driver *drv, struct module *owner, const char *mod_name);
void pci_release_selected_regions(struct pci_dev *pdev, int bars);
int pci_request_selected_regions_exclusive(struct pci_dev *pdev, int bars, const char *res_name);
void pci_restore_state(struct pci_dev *dev);
int pci_save_state(struct pci_dev *dev);
int pci_select_bars(struct pci_dev *dev, unsigned long flags);
void pci_set_master(struct pci_dev *dev);
void pci_unregister_driver(struct pci_driver *dev);
void pm_qos_add_request(struct pm_qos_request *req, int pm_qos_class, s32 value);
void pm_qos_remove_request(struct pm_qos_request *req);
void pm_qos_update_request(struct pm_qos_request *req, s32 new_value);
int __pm_runtime_idle(struct device *dev, int rpmflags);
int __pm_runtime_resume(struct device *dev, int rpmflags);
int pm_schedule_suspend(struct device *dev, unsigned int delay);
void print_hex_dump(const char *level, const char *prefix_str, int prefix_type, int rowsize, int groupsize, const void *buf, size_t len, bool ascii);
asmlinkage __printf(1, 2) __cold int printk(const char *fmt, ...);
int pskb_expand_head(struct sk_buff *skb, int nhead, int ntail, gfp_t gfp_mask);
unsigned char *__pskb_pull_tail(struct sk_buff *skb, int delta);
int ___pskb_trim(struct sk_buff *skb, unsigned int len);
int ptp_clock_index(struct ptp_clock *ptp);
struct ptp_clock *ptp_clock_register(struct ptp_clock_info *info, struct device *parent);
int ptp_clock_unregister(struct ptp_clock *ptp);
void __put_page(struct page *page);
bool queue_delayed_work_on(int cpu, struct workqueue_struct *wq, struct delayed_work *work, unsigned long delay);
bool queue_work_on(int cpu, struct workqueue_struct *wq, struct work_struct *work);
void __lockfunc _raw_spin_lock(raw_spinlock_t *lock) __acquires(lock);
unsigned long __lockfunc _raw_spin_lock_irqsave(raw_spinlock_t *lock) __acquires(lock);
void __lockfunc _raw_spin_unlock_irqrestore(raw_spinlock_t *lock, unsigned long flags) __releases(lock);
int register_netdev(struct net_device *dev);
int __must_check request_threaded_irq(unsigned int irq, irq_handler_t handler, irq_handler_t thread_fn, unsigned long flags, const char *name, void *dev);
unsigned long round_jiffies(unsigned long j);
int skb_pad(struct sk_buff *skb, int pad);
unsigned char *skb_put(struct sk_buff *skb, unsigned int len);
void skb_trim(struct sk_buff *skb, unsigned int len);
void skb_tstamp_tx(struct sk_buff *orig_skb, struct skb_shared_hwtstamps *hwtstamps);
size_t strlcpy(char *dest, const char *src, size_t size);
void synchronize_irq(unsigned int irq);
u64 timecounter_cyc2time(struct timecounter *tc, cycle_t cycle_tstamp);
void timecounter_init(struct timecounter *tc, const struct cyclecounter *cc, u64 start_tstamp);
u64 timecounter_read(struct timecounter *tc);
void __udelay(unsigned long usecs);
void unregister_netdev(struct net_device *dev);
void usleep_range(unsigned long min, unsigned long max);
void vfree(const void *addr);
void *vmalloc(unsigned long size);
extern void *vzalloc(unsigned long size);
void warn_slowpath_null(const char *file, const int line);

/*****************************************************************************/
/*                                 constants                                 */
/*****************************************************************************/

/*****************************************************************************/
/*                           private declarations                            */
/*****************************************************************************/

/*****************************************************************************/
/*                         private global variables                          */
/*****************************************************************************/

static struct task_struct __init_task;
static struct dma_map_ops __dma_ops; 
static struct pci_driver *__pci_drv;
static struct module *__owner;
static const char *__mod_name;
static struct pci_dev __pci_devices[13];
static struct pci_bus __pci_bus;
static const char *__pci_dev_init_name[13] = { "foo0", "foo1", "foo2", "foo3", "foo4", "foo5", "foo6", "foo7", "foo8", "foo9", "foo10", "foo11", "foo12" };
static u8 __pci_dev_iomem[13][1024];
static u8 __pci_dev6_flashmem[1024];

/*****************************************************************************/
/*                       private function declarations                       */
/*****************************************************************************/

static int rnd();
static void testbench_run();
static void testbench_init();

/*****************************************************************************/
/*                         public function definitions                       */
/*****************************************************************************/

unsigned int testbench_raw_readl(const volatile void __iomem *addr)
{
}

void testbench_raw_writel(unsigned int val, volatile void __iomem *addr)
{
}

unsigned short testbench_raw_readw(const volatile void __iomem *addr)
{
}

void testbench_raw_writew(unsigned short val, volatile void __iomem *addr)
{
}

struct net_device *alloc_etherdev_mqs(int sizeof_priv, unsigned int txqs, unsigned int rxqs)
{
	struct net_device *dev;
	size_t alloc_size;
	struct net_device *p;
	
	alloc_size = sizeof(struct net_device);
	if (sizeof_priv) {
			/* ensure 32-byte alignment of private area */
			alloc_size = ALIGN(alloc_size, NETDEV_ALIGN);
			alloc_size += sizeof_priv;
	}
	/* ensure 32-byte alignment of whole construct */
	alloc_size += NETDEV_ALIGN - 1;

	p = kzalloc(alloc_size, GFP_KERNEL | __GFP_NOWARN | __GFP_REPEAT);
	if (!p)
			p = vzalloc(alloc_size);
	if (!p)
			return NULL;

	dev = PTR_ALIGN(p, NETDEV_ALIGN);
	dev->padded = (char *)dev - (char *)p;

	return dev;
}

struct page *alloc_pages_current(gfp_t gfp_mask, unsigned order)
{
}

struct sk_buff *__alloc_skb(unsigned int size, gfp_t priority, int flags, int node)
{
}

bool arch_dma_alloc_attrs(struct device **dev, gfp_t *gfp)
{
}

bool cancel_delayed_work_sync(struct delayed_work *dwork)
{
}

bool cancel_work_sync(struct work_struct *work)
{
}

int _cond_resched(void)
{
}

void __const_udelay(unsigned long xloops)
{
}

void consume_skb(struct sk_buff *skb)
{
}

struct system_counterval_t convert_art_to_tsc(cycle_t art)
{
}

unsigned long __must_check _copy_from_user(void *to, const void __user *from, unsigned n)
{
}

unsigned long __must_check _copy_to_user(void __user *to, const void *from, unsigned n)
{
}

u32 __pure crc32_le(u32 crc, unsigned char const *p, size_t len)
{
}

__sum16 csum_ipv6_magic(const struct in6_addr *saddr, const struct in6_addr *daddr, __u32 len, __u8 proto, __wsum csum)
{
}

void delayed_work_timer_fn(unsigned long __data)
{
}

int del_timer_sync(struct timer_list *timer)
{
}

__printf(2, 3) void dev_err(const struct device *dev, const char *fmt, ...)
{
}

void device_set_wakeup_capable(struct device *dev, bool capable)
{
}

int device_set_wakeup_enable(struct device *dev, bool enable)
{
}

int device_wakeup_enable(struct device *dev)
{
}

__printf(2, 3) void _dev_info(const struct device *dev, const char *fmt, ...)
{
}

void __dev_kfree_skb_any(struct sk_buff *skb, enum skb_free_reason reason)
{
}

void __dev_kfree_skb_irq(struct sk_buff *skb, enum skb_free_reason reason)
{
}

void dev_notice(const struct device *dev, const char *fmt, ...)
{
}

unsigned long dev_trans_start(struct net_device *dev)
{
}

__printf(2, 3) void dev_warn(const struct device *dev, const char *fmt, ...)
{
}

void disable_irq(unsigned int irq)
{
}

int dma_supported(struct device *dev, u64 mask)
{
	struct dma_map_ops *ops = get_dma_ops(dev);
	
	if(ops->dma_supported)
	{
		return ops->dma_supported(dev, mask);
	}
	
	if(mask < DMA_BIT_MASK(24))
	{
		return 0;
	}
	
	return 1;
}

void dql_completed(struct dql *dql, unsigned int count)
{
}

void dql_reset(struct dql *dql)
{
}

__printf(3, 4) void __dynamic_netdev_dbg(struct _ddebug *descriptor, const struct net_device *dev, const char *fmt, ...)
{
}

void enable_irq(unsigned int irq)
{
}

u32 ethtool_op_get_link(struct net_device *dev)
{
}

int ethtool_op_get_ts_info(struct net_device *dev, struct ethtool_ts_info *eti)
{
}

__be16 eth_type_trans(struct sk_buff *skb, struct net_device *dev)
{
}

int eth_validate_addr(struct net_device *dev)
{
}

unsigned long find_first_bit(const unsigned long *addr, unsigned long size)
{
}

unsigned long find_next_bit(const unsigned long *addr, unsigned long size, unsigned long offset)
{
}

void free_irq(unsigned int irq, void *dev_id)
{
}

void free_netdev(struct net_device *dev)
{
}

int get_device_system_crosststamp(int (*get_time_fn)(ktime_t *device_time, struct system_counterval_t *system_counterval, void *ctx), void *ctx, struct system_time_snapshot *history, struct system_device_crosststamp *xtstamp)
{
}

void init_timer_key(struct timer_list *timer, unsigned int flags, const char *name, struct lock_class_key *key)
{
}

void __iomem *ioremap(resource_size_t offset, unsigned long size)
{
	return (void __iomem *) offset;
}

void iounmap(volatile void __iomem *addr)
{
}

void kfree(const void *objp)
{
	free(objp);
}

void *__kmalloc(size_t size, gfp_t flags)
{
	return (flags & ___GFP_ZERO) ? calloc(size, 1) : malloc(size);
}

char *kstrdup(const char *s, gfp_t gfp)
{
	size_t size = strlen(s) + 1;
	void *d = kmalloc(gfp, size);
	if(d)
	{
		memcpy(d, s, size);
	}
	
	return d;
}

ktime_t ktime_get_with_offset(enum tk_offsets offs)
{
}

int mod_timer(struct timer_list *timer, unsigned long expires)
{
}

ssize_t __modver_version_show(struct module_attribute *mattr, struct module_kobject *mk, char *buf)
{
}

void msleep(unsigned int msecs)
{
}

unsigned long msleep_interruptible(unsigned int msecs)
{
}

void mutex_lock(struct mutex *lock)
{
}

void mutex_unlock(struct mutex *lock)
{
}

struct sk_buff *__napi_alloc_skb(struct napi_struct *napi, unsigned int length, gfp_t gfp_mask)
{
}

void napi_complete_done(struct napi_struct *n, int work_done)
{
}

void napi_disable(struct napi_struct *n)
{
}

gro_result_t napi_gro_receive(struct napi_struct *napi, struct sk_buff *skb)
{
}

void __napi_schedule(struct napi_struct *n)
{
}

struct sk_buff *__netdev_alloc_skb(struct net_device *dev, unsigned int length, gfp_t gfp_mask)
{
}

__printf(2, 3) void netdev_err(const struct net_device *dev, const char *format, ...)
{
}

__printf(2, 3) void netdev_info(const struct net_device *dev, const char *format, ...)
{
}

void netdev_rss_key_fill(void *buffer, size_t len)
{
}

__printf(2, 3) void netdev_warn(const struct net_device *dev, const char *format, ...)
{
}

void netif_carrier_off(struct net_device *dev)
{
}

void netif_carrier_on(struct net_device *dev)
{
}

void netif_device_attach(struct net_device *dev)
{
}

void netif_device_detach(struct net_device *dev)
{
}

void netif_napi_add(struct net_device *dev, struct napi_struct *napi, int (*poll)(struct napi_struct *, int), int weight)
{
}

void netif_schedule_queue(struct netdev_queue *txq)
{
}

void netif_tx_wake_queue(struct netdev_queue *dev_queue)
{
}

int net_ratelimit(void)
{
}

struct timespec ns_to_timespec(const s64 nsec)
{
}

netdev_features_t passthru_features_check(struct sk_buff *skb, struct net_device *dev, netdev_features_t features)
{
}

int pci_bus_read_config_word(struct pci_bus *bus, unsigned int devfn, int where, u16 *val)
{
}

int pci_bus_write_config_word(struct pci_bus *bus, unsigned int devfn, int where, u16 val)
{
}

int pci_cleanup_aer_uncorrect_error_status(struct pci_dev *dev)
{
}

void pci_clear_master(struct pci_dev *dev)
{
}

bool pci_dev_run_wake(struct pci_dev *dev)
{
}

void pci_disable_device(struct pci_dev *dev)
{
}

void pci_disable_link_state(struct pci_dev *pdev, int state)
{
}

void pci_disable_link_state_locked(struct pci_dev *pdev, int state)
{
}

void pci_disable_msi(struct pci_dev *dev)
{
}

void pci_disable_msix(struct pci_dev *dev)
{
}

int pci_disable_pcie_error_reporting(struct pci_dev *dev)
{
}

int pcie_capability_clear_and_set_word(struct pci_dev *dev, int pos, u16 clear, u16 set)
{
}

int pcie_capability_read_word(struct pci_dev *dev, int pos, u16 *val)
{
	*val = 0;
	return 0; // hack
}

int pcie_capability_write_word(struct pci_dev *dev, int pos, u16 val)
{
}

int __must_check pci_enable_device_mem(struct pci_dev *dev)
{
}

int pci_enable_msi_range(struct pci_dev *dev, int minvec, int maxvec)
{
	return 0; // hack
}

int pci_enable_msix_range(struct pci_dev *dev, struct msix_entry *entries, int minvec, int maxvec)
{
}

int pci_enable_pcie_error_reporting(struct pci_dev *dev)
{
}

int __pci_enable_wake(struct pci_dev *dev, pci_power_t state, bool runtime, bool enable)
{
}

int pci_prepare_to_sleep(struct pci_dev *dev)
{
}

int __must_check __pci_register_driver(struct pci_driver *drv, struct module *owner, const char *mod_name)
{
	__pci_drv = drv;
	__owner = owner;
	__mod_name = mod_name;
	return 0; // success
}

void pci_release_selected_regions(struct pci_dev *pdev, int bars)
{
}

int pci_request_selected_regions_exclusive(struct pci_dev *pdev, int bars, const char *res_name)
{
	return 0; // hack
}

void pci_restore_state(struct pci_dev *dev)
{
}

int pci_save_state(struct pci_dev *dev)
{
	return 0; // hack
}

int pci_select_bars(struct pci_dev *dev, unsigned long flags)
{
	int i, bars = 0;
	for(i = 0; i < PCI_NUM_RESOURCES; i++)
	{
		if(pci_resource_flags(dev, i) & flags)
		{
			bars |= (1 << i);
		}
	}
	return bars;
}

void pci_set_master(struct pci_dev *dev)
{
}

void pci_unregister_driver(struct pci_driver *dev)
{
}

void pm_qos_add_request(struct pm_qos_request *req, int pm_qos_class, s32 value)
{
}

void pm_qos_remove_request(struct pm_qos_request *req)
{
}

void pm_qos_update_request(struct pm_qos_request *req, s32 new_value)
{
}

int __pm_runtime_idle(struct device *dev, int rpmflags)
{
}

int __pm_runtime_resume(struct device *dev, int rpmflags)
{
}

int pm_schedule_suspend(struct device *dev, unsigned int delay)
{
}

void print_hex_dump(const char *level, const char *prefix_str, int prefix_type, int rowsize, int groupsize, const void *buf, size_t len, bool ascii)
{
}

asmlinkage __printf(1, 2) __cold int printk(const char *fmt, ...)
{
	int count;
	static char buffer[4096];
	va_list ap;

	va_start(ap, fmt);
	count = vsprintf(buffer, fmt, ap);
	va_end(ap);
	
	write(1, buffer, count);
	
	return count;
}

int pskb_expand_head(struct sk_buff *skb, int nhead, int ntail, gfp_t gfp_mask)
{
}

unsigned char *__pskb_pull_tail(struct sk_buff *skb, int delta)
{
}

int ___pskb_trim(struct sk_buff *skb, unsigned int len)
{
}

int ptp_clock_index(struct ptp_clock *ptp)
{
}

struct ptp_clock *ptp_clock_register(struct ptp_clock_info *info, struct device *parent)
{
}

int ptp_clock_unregister(struct ptp_clock *ptp)
{
}

void __put_page(struct page *page)
{
}

bool queue_delayed_work_on(int cpu, struct workqueue_struct *wq, struct delayed_work *work, unsigned long delay)
{
}

bool queue_work_on(int cpu, struct workqueue_struct *wq, struct work_struct *work)
{
}

void __lockfunc _raw_spin_lock(raw_spinlock_t *lock)
{
}

unsigned long __lockfunc _raw_spin_lock_irqsave(raw_spinlock_t *lock)
{
}

void __lockfunc _raw_spin_unlock_irqrestore(raw_spinlock_t *lock, unsigned long flags)
{
}

int register_netdev(struct net_device *dev)
{
}

int __must_check request_threaded_irq(unsigned int irq, irq_handler_t handler, irq_handler_t thread_fn, unsigned long flags, const char *name, void *dev)
{
}

unsigned long round_jiffies(unsigned long j)
{
}

int skb_pad(struct sk_buff *skb, int pad)
{
}

unsigned char *skb_put(struct sk_buff *skb, unsigned int len)
{
}

void skb_trim(struct sk_buff *skb, unsigned int len)
{
}

void skb_tstamp_tx(struct sk_buff *orig_skb, struct skb_shared_hwtstamps *hwtstamps)
{
}

size_t strlcpy(char *dest, const char *src, size_t size)
{
	size_t ret = strlen(src);

	if (size) {
		size_t len = (ret >= size) ? size - 1 : ret;
		memcpy(dest, src, len);
		dest[len] = '\0';
	}
	return ret;
}

void synchronize_irq(unsigned int irq)
{
}

u64 timecounter_cyc2time(struct timecounter *tc, cycle_t cycle_tstamp)
{
}

void timecounter_init(struct timecounter *tc, const struct cyclecounter *cc, u64 start_tstamp)
{
}

u64 timecounter_read(struct timecounter *tc)
{
}

void __udelay(unsigned long usecs)
{
}

void unregister_netdev(struct net_device *dev)
{
}

void usleep_range(unsigned long min, unsigned long max)
{
}

void vfree(const void *addr)
{
}

void *vmalloc(unsigned long size)
{
	return 0; // out-of-memory
}

void *vzalloc(unsigned long size)
{
	return 0; // out-of-memory
}

void warn_slowpath_null(const char *file, const int line)
{
}

/*****************************************************************************/
/*                       private function definitions                        */
/*****************************************************************************/

static int rnd()
{
	return 1;
}

static void testbench_run()
{
}

static void testbench_init()
{
	unsigned int i;
	
	current_task = &__init_task;
	dma_ops = &__dma_ops;
	jiffies = 0;
	page_offset_base = 0;
	phys_base = 0;
	system_wq = 0;
	vmemmap_base = 0;
	__pci_drv = 0;
	__owner = 0;
	__mod_name = 0;
	for(i = 0; i < (sizeof(__pci_devices) / sizeof(__pci_devices[0])); i++)
	{
		__pci_devices[i].bus = &__pci_bus;
		__pci_devices[i].dev.coherent_dma_mask = ~((u64) 0);
		__pci_devices[i].dev.dma_mask = &__pci_devices[i].dev.coherent_dma_mask;
	}
}

/*****************************************************************************/
/*                                   entry point                             */
/*****************************************************************************/

int main(int argc, char *argv[])
{
	while(1)
	{
		testbench_init();
		initcall_t e1000e_driver_init = __initcall6_start[0]; // pointer to e1000_init_module
		if((*e1000e_driver_init)() == 0) // calls e1000_init_module
		{
			if(__pci_drv)
			{
				const struct pci_device_id *pci_dev_ids = __pci_drv->id_table;
				unsigned int pci_dev_num = 0;
				struct pci_dev *pci_dev = &__pci_devices[0];
				const struct pci_device_id *pci_dev_id = pci_dev_ids;
				while(pci_dev_id->vendor || pci_dev_id->subvendor || pci_dev_id->class_mask)
				{
					pci_dev->dev.init_name = __pci_dev_init_name[pci_dev_num];
					pci_dev->resource[0].start = (resource_size_t) &__pci_dev_iomem[pci_dev_num][0];
					pci_dev->resource[0].end = (resource_size_t)(&__pci_dev_iomem[pci_dev_num][0] + sizeof(__pci_dev_iomem[pci_dev_num]) - 1);
					if(pci_dev_num == 6)
					{
						pci_dev->resource[1].start = (resource_size_t) &__pci_dev6_flashmem[0];
						pci_dev->resource[1].end = (resource_size_t)(&__pci_dev6_flashmem[0] + sizeof(__pci_dev6_flashmem[6]) - 1);
					}
					
					(*__pci_drv->probe)(pci_dev, pci_dev_id);
					pci_dev_num++;
					pci_dev++;
					pci_dev_id++;
				}
			}
			
			while(1)
			{
				testbench_run();
			}
		}
	}
	return 0;
}
