#ifndef __TESTBENCH_INSTRUMENTATION_H__
#define __TESTBENCH_INSTRUMENTATION_H__

#include <linux/types.h>

extern unsigned short testbench_raw_readw(const volatile void __iomem *addr);
extern void testbench_raw_writew(unsigned short val, volatile void __iomem *addr);
extern unsigned int testbench_raw_readl(const volatile void __iomem *addr);
extern void testbench_raw_writel(unsigned int val, volatile void __iomem *addr);

#define readw_relaxed(c) testbench_raw_readw(c)
#define writew_relaxed(v,c) testbench_raw_writew(v,c)
#define readl_relaxed(c) testbench_raw_readl(c)
#define writel_relaxed(v,c) testbench_raw_writel(v,c)
#define readw(c) testbench_raw_readw(c)
#define writew(v,c) testbench_raw_writew(v,c)
#define readl(c) testbench_raw_readl(c)
#define writel(v,c) testbench_raw_writel(v,c)

#define mmiowb() do {} while (0)

void __iomem *ioremap(resource_size_t offset, unsigned long size);
void iounmap(volatile void __iomem *addr);

#endif
