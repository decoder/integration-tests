mythaitsar
==========

[`mythaistar`](https://github.com/devonfw/my-thai-star) is a Java project which is used to test the following functions:

* PKM: project creation
* PKM: user creation
* PKM: administrator and user login
* PKM: inserting a documentation files
* ASFM: running ASFM to convert documentation file to ASFM documentation format
* PKM: inserting some Java source code files
* PKM: querying the documentation
* Code summarization: run Code summarization on each java source code file
* Java parser: running Java parser on all java source code files
* PKM: querying the source code
* OpenJML analyzer: run OpenJML analyzer on each java source code file with both 'static checking' and 'assertion checking' generate modes
* PKM: inserting some UML XMI files
* Class Model XMI to JSON transformer: run classmodelxmi2jsontransformer on each UML XML files
* State Machine Model XMI to JSON transformer: run smmodelxmi2jsontransformer on each UML XML files
* PKM: deleting a user
* PKM: deleting a project
