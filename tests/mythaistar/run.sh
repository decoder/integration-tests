#!/usr/bin/env bash
DIR=$(cd $(dirname $BASH_SOURCE); pwd)
source "${DIR}/../../api/decoder_rest_api.sh"
source "${DIR}/test.conf"

set +o errexit

echo -n "${ADMIN_NAME}'s password: "
read -s ADMIN_PASSWORD
USER_PASSWORD=$(generate_random_password)

set -o nounset

function exit_status
{
	local STATUS=$1
	if [ $STATUS -eq 0 ]; then
		echo "Test PASSED" >&2
	else
		echo "Test FAILED" >&2
	fi

	EXIT_STATUS=$STATUS

	return $STATUS
}

return_status=return

EXIT_STATUS=0

while true; do

	echo "PKM administrator's login"
	ADMIN_KEY=$(pkm_login "${ADMIN_NAME}" "${ADMIN_PASSWORD}")
	[ $? -eq 0 ] || exit_status 1 || break
	echo

	echo "creating user ${USER_NAME} with password '${USER_PASSWORD}'"
	pkm_post_user "${ADMIN_KEY}" ${USER_NAME} "${USER_PASSWORD}" || exit_status 1 || break
	echo

	echo "creating project ${PROJECT_NAME} owned by ${USER_NAME}"
	pkm_post_project "${ADMIN_KEY}" "${PROJECT_NAME}" ${USER_NAME} '"Owner","Developer"' || exit_status 1 || break
	echo

	echo "user's login by ${USER_NAME}"
	USER_KEY=$(pkm_login ${USER_NAME} "${USER_PASSWORD}")
	[ $? -eq 0 ] || exit_status 1 || break
	echo
	
	echo "inserting a documentation file in project ${PROJECT_NAME} by ${USER_NAME}"
	pkm_post_files "${USER_KEY}" "doc/rawdoc/$(uri_encode "${PROJECT_NAME}")" 'binary' "${DIR}/data" "${DIR}/data/my_thai_star.docx" || exit_status 1 || break
	echo
	
	echo "running doc_to_asfm by ${USER_NAME}"
	run_doc_to_asfm "${USER_KEY}" ${PROJECT_NAME} '' my_thai_star.docx || exit_status 1 || break
	echo
	
	echo "getting documentation of class 'BookingManagementImpl'"
	pkm_get "${USER_KEY}" "doc/asfm/artefacts/$(uri_encode "${PROJECT_NAME}")?class=$(uri_encode '/BookingManagementImpl/')" || exit_status 1 || break
	echo
	
	echo "inserting mythaistar source code into ${PROJECT_NAME} by ${USER_NAME}"
	pkm_post_files "${USER_KEY}" "code/rawsourcecode/$(uri_encode "${PROJECT_NAME}")" 'text' "${DIR}/data" "*.java" || exit_status 1 || break
	echo
	
	# build a file list (absolute paths) of all java source code files
	SOURCE_CODE_FILES=()
	build_file_list SOURCE_CODE_FILES "${DIR}/data" "*.java" || exit_status 1 || break
	
	# run code summarization on each java source code file
	for SOURCE_CODE_FILE in "${SOURCE_CODE_FILES[@]}"; do
		SOURCE_CODE_FILE_REL_PATH=$(realpath --relative-to="${DIR}/data" "${SOURCE_CODE_FILE}")
		if [ $? -ne 0 ]; then
			exit_status 1
			break
		fi
		echo "running code summarization on ${SOURCE_CODE_FILE_REL_PATH} by ${USER_NAME}"
		predictions_code_summ "${USER_KEY}" "${PROJECT_NAME}" "${SOURCE_CODE_FILE_REL_PATH}" || exit_status 1 || break
		echo
	done
	
	if [ ${EXIT_STATUS} -ne 0 ]; then
		break
	fi
	
	# run javaparser on each java source code file
	for SOURCE_CODE_FILE in "${SOURCE_CODE_FILES[@]}"; do
		SOURCE_CODE_FILE_REL_PATH=$(realpath --relative-to="${DIR}/data" "${SOURCE_CODE_FILE}")
		if [ $? -ne 0 ]; then
			exit_status 1
			break
		fi
		echo "running javaparser on ${SOURCE_CODE_FILE_REL_PATH} by ${USER_NAME}"
		javaparser "${USER_KEY}" "${PROJECT_NAME}" "${SOURCE_CODE_FILE_REL_PATH}" || exit_status 1 || break
		echo
	done
	
	if [ ${EXIT_STATUS} -ne 0 ]; then
		break
	fi
	
	echo "getting Java classes"
	pkm_get "${USER_KEY}" "code/java/classes/$(uri_encode "${PROJECT_NAME}")" || exit_status 1 || break
	echo
	
	echo "getting methods of class 'BookingmanagementRestService'"
	pkm_get "${USER_KEY}" "code/java/class/methods/$(uri_encode "${PROJECT_NAME}")/$(uri_encode 'BookingmanagementRestService')" || exit_status 1 || break
	echo
	
	echo "getting fields of class 'UsermanagementImpl'"
	pkm_get "${USER_KEY}" "code/java/class/fields/$(uri_encode "${PROJECT_NAME}")/$(uri_encode 'UsermanagementImpl')" || exit_status 1 || break
	echo
	
	# run openjml analyzer on each java source code file with 'type checking', 'static checking' and 'assertion checking' generate modes
	for SOURCE_CODE_FILE in "${SOURCE_CODE_FILES[@]}"; do
		SOURCE_CODE_FILE_REL_PATH=$(realpath --relative-to="${DIR}/data" "${SOURCE_CODE_FILE}")
		if [ $? -ne 0 ]; then
			exit_status 1
			break
		fi
		for OPENJML_GENERATE_MODE in '' sc rac; do
			echo "running openjml analyzer on ${SOURCE_CODE_FILE_REL_PATH} by ${USER_NAME}"
			openjml_analyzer "${USER_KEY}" "${PROJECT_NAME}" "${SOURCE_CODE_FILE_REL_PATH}" "${OPENJML_GENERATE_MODE}" || exit_status 1 || break
			echo
		done
		
	done
	
	if [ ${EXIT_STATUS} -ne 0 ]; then
		break
	fi
	
	echo "inserting mythaistar UML XMI files into ${PROJECT_NAME} by ${USER_NAME}"
	pkm_post_files "${USER_KEY}" "uml/rawuml/$(uri_encode "${PROJECT_NAME}")" 'text' "${DIR}/data" "*.uml" || exit_status 1 || break
	echo
	
	
	# build a file list (absolute paths) of all UML XMI files
	UML_XMI_FILES=()
	build_file_list UML_XMI_FILES "${DIR}/data" "*.uml" || exit_status 1 || break
	
	# run classmodelxmi2jsontransformer on each UML XMI file
	for UML_XMI_FILE in "${UML_XMI_FILES[@]}"; do
		UML_XMI_FILE_REL_PATH=$(realpath --relative-to="${DIR}/data" "${UML_XMI_FILE}")
		if [ $? -ne 0 ]; then
			exit_status 1
			break
		fi
		echo "running classmodelxmi2jsontransformer on ${UML_XMI_FILE_REL_PATH} by ${USER_NAME}"
		classmodelxmi2jsontransformer "${USER_KEY}" "${PROJECT_NAME}" "${UML_XMI_FILE_REL_PATH}" || exit_status 1 || break
		echo
	done
	
	if [ ${EXIT_STATUS} -ne 0 ]; then
		break
	fi
	
	# run smmodelxmi2jsontransformer on each UML XMI file
	for UML_XMI_FILE in "${UML_XMI_FILES[@]}"; do
		UML_XMI_FILE_REL_PATH=$(realpath --relative-to="${DIR}/data" "${UML_XMI_FILE}")
		if [ $? -ne 0 ]; then
			exit_status 1
			break
		fi
		echo "running smmodelxmi2jsontransformer on ${UML_XMI_FILE_REL_PATH} by ${USER_NAME}"
		smmodelxmi2jsontransformer "${USER_KEY}" "${PROJECT_NAME}" "${UML_XMI_FILE_REL_PATH}" || exit_status 1 || break
		echo
	done
	
	if [ ${EXIT_STATUS} -ne 0 ]; then
		break
	fi
	
	exit_status 0
  
	break
done

echo "Tip: ${USER_NAME}'s password is '${USER_PASSWORD}'"

if mr_proper_prompt; then
	echo "cleaning"

	echo "deleting user ${USER_NAME}"
	pkm_delete "${ADMIN_KEY}" "user/${USER_NAME}"
	echo

	echo "deleting project"
	pkm_delete "${ADMIN_KEY}" "project/$(uri_encode "${PROJECT_NAME}")"
	echo
fi

exit ${EXIT_STATUS}
