package com.devonfw.application.mtsj.bookingmanagement.service.api.rest;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.data.domain.Page;

import com.devonfw.application.mtsj.bookingmanagement.common.api.to.BookingCto;
import com.devonfw.application.mtsj.bookingmanagement.common.api.to.BookingEto;
import com.devonfw.application.mtsj.bookingmanagement.common.api.to.BookingSearchCriteriaTo;
import com.devonfw.application.mtsj.bookingmanagement.common.api.to.InvitedGuestEto;
import com.devonfw.application.mtsj.bookingmanagement.common.api.to.InvitedGuestSearchCriteriaTo;
import com.devonfw.application.mtsj.bookingmanagement.common.api.to.TableEto;
import com.devonfw.application.mtsj.bookingmanagement.common.api.to.TableSearchCriteriaTo;
import com.devonfw.application.mtsj.bookingmanagement.logic.api.Bookingmanagement;

/**
 * The service interface for REST calls in order to execute the logic of component {@link Bookingmanagement}.
 */
@Path("/bookingmanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface BookingmanagementRestService {

  /*********************
   * REST SERVICE METHODS
   *********************/

  /**
   * Get booking function delegates to {@link Bookingmanagement#findBooking}.
   *
   * @param id the ID of the {@link BookingEto}
   *
   * @return the {@link BookingEto}
   */

  /* @ requires id >= 0; @ */

  @GET
  @Path("/booking/{id}/")
  public /* @ non_null @ */ BookingCto getBooking(@PathParam("id") long id);

  /**
   * Save booking function delegates to {@link Bookingmanagement#saveBooking}.
   *
   * @param booking the {@link BookingEto} to be saved
   *
   * @return the recently created {@link BookingEto}
   */

  /* @ requires booking != null; @ */

  @POST
  @Path("/booking/")
  public /* @ non_null @ */ BookingEto saveBooking(@Valid BookingCto booking);

  /**
   * Delete booking function delegates to {@link Bookingmanagement#deleteBooking}.
   *
   * @param id ID of the {@link BookingEto} to be deleted
   */

  /* @ requires id >= 0; @ */

  @DELETE
  @Path("/booking/{id}/")
  public /* @ non_null @ */ void deleteBooking(@PathParam("id") long id);

  /**
   * Find bookings by post function delegates to {@link Bookingmanagement#findBookingEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding bookings.
   *
   * @return the {@link PaginatedListTo list} of matching {@link BookingEto}s.
   */

  /* @ requires searchCriteriaTo != null; @ */

  @POST
  @Path("/booking/search")
  public /* @ non_null @ */ Page<BookingCto> findBookingsByPost(BookingSearchCriteriaTo searchCriteriaTo);

  /**
   * Get invited guest function delegates to {@link Bookingmanagement#findInvitedGuest}.
   *
   * @param id the ID of the {@link InvitedGuestEto}
   *
   * @return the {@link InvitedGuestEto}
   */

  /* @ requires id >= 0; @ */

  @GET
  @Path("/invitedguest/{id}/")
  public /* @ non_null @ */ InvitedGuestEto getInvitedGuest(@PathParam("id") long id);

  /**
   * Save invited guest function delegates to {@link Bookingmanagement#saveInvitedGuest}.
   *
   * @param invitedguest the {@link InvitedGuestEto} to be saved
   *
   * @return the recently created {@link InvitedGuestEto}
   */

  /* @ requires invitedguest != null; @ */

  @POST
  @Path("/invitedguest/")
  public /* @ non_null @ */ InvitedGuestEto saveInvitedGuest(InvitedGuestEto invitedguest);

  /**
   * Delete invited guest function delegates to {@link Bookingmanagement#deleteInvitedGuest}.
   *
   * @param id ID of the {@link InvitedGuestEto} to be deleted
   */

  /* @ requires id >= 0; @ */

  @DELETE
  @Path("/invitedguest/{id}/")
  public void deleteInvitedGuest(@PathParam("id") long id);

  /**
   * Find invited guests by post function delegates to {@link Bookingmanagement#findInvitedGuestEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding invitedguests.
   *
   * @return the {@link PaginatedListTo list} of matching {@link InvitedGuestEto}s.
   */

  /* @ requires searchCriteriaTo != null; @ */

  @POST
  @Path("/invitedguest/search")
  public /* @ non_null @ */ Page<InvitedGuestEto> findInvitedGuestsByPost(
      InvitedGuestSearchCriteriaTo searchCriteriaTo);

  /**
   * Accept invite
   *
   * @param guestToken : string identifier
   *
   * @return InvitedGuestEto
   */

  /* @ requires guestToken != null; @ */

  @GET
  @Path("/invitedguest/accept/{token}")
  public /* @ non_null @ */ InvitedGuestEto acceptInvite(@PathParam("token") String guestToken);

  /**
   * Decline invite
   *
   * @param guestToken : string identifier
   *
   * @return InvitedGuestEto
   */

  /* @ requires guestToken != null; @ */

  @GET
  @Path("/invitedguest/decline/{token}")
  public /* @ non_null @ */ InvitedGuestEto declineInvite(@PathParam("token") String guestToken);

  /**
   * Cancel invite
   *
   * @param bookingToken : string identifier
   */

  /* @ requires bookingToken != null; @ */

  @GET
  @Path("/booking/cancel/{token}")
  public void cancelInvite(@PathParam("token") String bookingToken);

  /**
   * Get table function Delegates to {@link Bookingmanagement#findTable}.
   *
   * @param id the ID of the {@link TableEto}
   *
   * @return the {@link TableEto}
   */

  /* @ requires id >= 0; @ */

  @GET
  @Path("/table/{id}/")
  public /* @ non_null @ */ TableEto getTable(@PathParam("id") long id);

  /**
   * Save table funtion delegates to {@link Bookingmanagement#saveTable}.
   *
   * @param table the {@link TableEto} to be saved
   *
   * @return the recently created {@link TableEto}
   */

  /* @ requires table != null; @ */

  @POST
  @Path("/table/")
  public /* @ non_null @ */ TableEto saveTable(TableEto table);

  /**
   * Delete table function delegates to {@link Bookingmanagement#deleteTable}.
   *
   * @param id ID of the {@link TableEto} to be deleted
   */

  /* @ requires id >= 0; @ */

  @DELETE
  @Path("/table/{id}/")
  public void deleteTable(@PathParam("id") long id);

  /**
   * Find tables by post function delegates to {@link Bookingmanagement#findTableEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding tables.
   *
   * @return the {@link PaginatedListTo list} of matching {@link TableEto}s.
   */

  /* @ requires searchCriteriaTo != null; @ */

  @POST
  @Path("/table/search")
  public /* @ non_null @ */ Page<TableEto> findTablesByPost(TableSearchCriteriaTo searchCriteriaTo);
}