/**
 *  returns the user role id
 */
package com.devonfw.application.mtsj.usermanagement.logic.impl;

import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.apache.commons.codec.binary.Base32;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;

import com.devonfw.application.mtsj.general.common.api.UserProfile;
import com.devonfw.application.mtsj.general.common.api.to.UserDetailsClientTo;
import com.devonfw.application.mtsj.general.common.base.QrCodeService;
import com.devonfw.application.mtsj.general.logic.base.AbstractComponentFacade;
import com.devonfw.application.mtsj.usermanagement.common.api.to.UserEto;
import com.devonfw.application.mtsj.usermanagement.common.api.to.UserQrCodeTo;
import com.devonfw.application.mtsj.usermanagement.common.api.to.UserRoleEto;
import com.devonfw.application.mtsj.usermanagement.common.api.to.UserRoleSearchCriteriaTo;
import com.devonfw.application.mtsj.usermanagement.common.api.to.UserSearchCriteriaTo;
import com.devonfw.application.mtsj.usermanagement.dataaccess.api.UserEntity;
import com.devonfw.application.mtsj.usermanagement.dataaccess.api.UserRoleEntity;
import com.devonfw.application.mtsj.usermanagement.dataaccess.api.repo.UserRepository;
import com.devonfw.application.mtsj.usermanagement.dataaccess.api.repo.UserRoleRepository;
import com.devonfw.application.mtsj.usermanagement.logic.api.Usermanagement;

/**
 * Implementation of component interface of usermanagement
 */
@Named
@Transactional
public class UsermanagementImpl extends AbstractComponentFacade implements Usermanagement {

  /**
   * Logger instance.
   */

  private static final Logger LOG = LoggerFactory.getLogger(UsermanagementImpl.class);

  /***************
   * ENTITY FIELDS
   ***************/

  @Inject
  private /* @ spec_public @ */ UserRepository userDao;

  @Inject
  private /* @ spec_public @ */ UserRoleRepository userRoleDao;

  /*********************
   * ENTITY CONSTRUCTORS
   *********************/

  /**
   * The constructor.<br/>
   *
   * No parameters.
   */

  public UsermanagementImpl() {

    super();
  }

  /*********************
   * ENTITY METHODS
   *********************/

  /**
   * Find user<br/>
   *
   * @param id : number identifier
   *
   * @return UserEto
   */

  // @ also requires id >= 0;
  // @ ensures \result != null;

  @Override
  public UserEto findUser(Long id) {

    LOG.debug("Get User with id {} from database.", id);
    return getBeanMapper().map(getUserDao().find(id), UserEto.class);
  }

  /**
   * generate user qr code<br/>
   *
   * @param username : string identifier
   *
   * @return UserQrCodeTo
   */

  // @ also requires username != null;

  @Override
  public UserQrCodeTo generateUserQrCode(String username) {

    LOG.debug("Get User with username {} from database.", username);
    UserEntity user = getBeanMapper().map(getUserDao().findByUsername(username), UserEntity.class);
    initializeSecret(user);
    if (user != null && user.getTwoFactorStatus()) {
      return QrCodeService.generateQrCode(user);
    } else {
      return null;
    }
  }

  /**
   * get user status<br/>
   *
   * @param username : string identifier
   *
   * @return UserEto
   */

  // @ also requires username != null;
  // @ ensures \result != null;

  @Override
  public UserEto getUserStatus(String username) {

    LOG.debug("Get User with username {} from database.", username);
    return getBeanMapper().map(getUserDao().findByUsername(username), UserEto.class);
  }

  /**
   * find user by name<br/>
   *
   * @param username : string identifier
   *
   * @return UserEto
   */

  // @ also requires userName != null;
  // @ ensures \result != null;

  @Override
  public UserEto findUserbyName(String userName) {

    UserEntity entity = this.userDao.findByUsername(userName);
    /* @ assert entity != null; @ */
    return getBeanMapper().map(entity, UserEto.class);
  }

  /**
   * find user etos<br/>
   *
   * @param criteria : search pattern
   *
   * @return Page<UserEto> : list of objects
   */

  // @ also requires criteria != null;
  // @ ensures \result != null;

  @Override
  public Page<UserEto> findUserEtos(UserSearchCriteriaTo criteria) {

    Page<UserEntity> users = getUserDao().findUsers(criteria);
    /* @ assert users != null; @ */
    return mapPaginatedEntityList(users, UserEto.class);
  }

  /**
   * delete user<br/>
   *
   * @param userId : number identifier
   *
   * @return boolean
   */

  // @ also requires userId >= 0;
  // @ ensures \result == true;

  @Override
  public boolean deleteUser(Long userId) {

    UserEntity user = getUserDao().find(userId);
    getUserDao().delete(user);
    LOG.debug("The user with id '{}' has been deleted.", userId);
    return true;
  }

  /**
   * save user<br/>
   *
   * @param user
   *
   * @return UserEto
   */

  // @ also requires user != null;
  // @ ensures \result != null;

  @Override
  public UserEto saveUser(UserEto user) {

    Objects.requireNonNull(user, "user");
    UserEntity userEntity = getBeanMapper().map(user, UserEntity.class);
    // initialize, validate userEntity here if necessary
    UserEntity resultEntity = getUserDao().save(userEntity);
    LOG.debug("User with id '{}' has been created.", resultEntity.getId());
    return getBeanMapper().map(resultEntity, UserEto.class);
  }

  /**
   * save user two factor<br/>
   *
   * @param user
   *
   * @return UserEto
   */

  // @ also requires user != null;
  // @ ensures \result != null;

  @Override
  public UserEto saveUserTwoFactor(UserEto user) {

    Objects.requireNonNull(user, "user");
    UserEntity userEntity = getBeanMapper().map(getUserDao().findByUsername(user.getUsername()), UserEntity.class);
    // initialize, validate userEntity here if necessary
    userEntity.setTwoFactorStatus(user.getTwoFactorStatus());
    UserEntity resultEntity = getUserDao().save(userEntity);
    LOG.debug("User with id '{}' has been modified.", resultEntity.getId());
    return getBeanMapper().map(resultEntity, UserEto.class);
  }

  /**
   * Get user dao fetches the field 'userDao'.
   *
   * @return the {@link UserRepository} instance.
   */

  // @ ensures \result == this.userDao;

  public /*@ pure non_null @*/ UserRepository getUserDao() {

    return this.userDao;
  }

  /**
   * Find user role
   *
   * @param id : number identifier
   *
   * @return the {@link UserRepository} instance.
   */

  // @ also requires id >= 0;
  // @ ensures \result != null;

  @Override
  public UserRoleEto findUserRole(Long id) {

    LOG.debug("Get UserRole with id {} from database.", id);
    return getBeanMapper().map(getUserRoleDao().find(id), UserRoleEto.class);
  }

  /**
   * Find user role etos
   *
   * @param criteria : search pattern
   *
   * @return Page<UserRoleEto> : list of objects
   */

  // @ also requires criteria != null;
  // @ ensures \result != null; @Override

  public Page<UserRoleEto> findUserRoleEtos(UserRoleSearchCriteriaTo criteria) {

    Page<UserRoleEntity> userroles = getUserRoleDao().findUserRoles(criteria);
    return mapPaginatedEntityList(userroles, UserRoleEto.class);
  }

  /**
   * Delete user role
   *
   * @param userRoleId : number identifier
   *
   * @return boolean
   */

  // @ also requires userRoleId >= 0;
  // @ ensures \result == true;

  @Override
  public boolean deleteUserRole(Long userRoleId) {

    UserRoleEntity userRole = getUserRoleDao().find(userRoleId);
    getUserRoleDao().delete(userRole);
    LOG.debug("The userRole with id '{}' has been deleted.", userRoleId);
    return true;
  }

  /**
   * Save user role
   *
   * @param userRole
   *
   * @return UserRoleEto
   */

  // @ also requires userRole != null;
  // @ ensures \result != null;

  @Override
  public UserRoleEto saveUserRole(UserRoleEto userRole) {

    Objects.requireNonNull(userRole, "userRole");
    UserRoleEntity userRoleEntity = getBeanMapper().map(userRole, UserRoleEntity.class);
    // initialize, validate userRoleEntity here if necessary
    UserRoleEntity resultEntity = getUserRoleDao().save(userRoleEntity);
    LOG.debug("UserRole with id '{}' has been created.", resultEntity.getId());
    return getBeanMapper().map(resultEntity, UserRoleEto.class);
  }

  /**
   * initialize secret function assigns a randomly generated secret for an specific user<br/>
   *
   * @param user
   */

  // @ requires user != null;

  private void initializeSecret(UserEntity user) {

    if (user.getSecret() == null) {
      user.setSecret(Base32.random());
      UserEntity resultEntity = getUserDao().save(user);
      LOG.debug("User with id '{}' has been modified.", resultEntity.getId());
    }
  }

  /**
   * Get user role dao returns the field 'userRoleDao'.
   *
   * @return the {@link UserRoleRepository} instance.
   */

  // @ ensures \result == this.userRoleDao;

  public /*@ pure non_null @*/ UserRoleRepository getUserRoleDao() {

    return this.userRoleDao;
  }

  /**
   * Find user profile by login
   *
   * @param login : string identifier
   *
   * @return UserProfile
   */

  // @ also requires login != null;
  // @ ensures \result != null;

  @Override
  public UserProfile findUserProfileByLogin(String login) {

    UserEto userEto = findUserbyName(login);
    UserDetailsClientTo profile = new UserDetailsClientTo();
    profile.setId(userEto.getId());
    profile.setRole(Role.getRoleById(userEto.getUserRoleId()));
    return profile;
  }
}
