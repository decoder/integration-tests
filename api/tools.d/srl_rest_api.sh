#!/usr/bin/env bash

# run_srl(ACCESS_KEY, QUERY)
function run_srl
{
	if [ "$#" -lt 2 ]; then
		echo "Error in srl function: takes at least 2 arguments: access key and query" >&2
		$return_status 1
	fi

	local ACCESS_KEY="$1"
	shift
	local QUERY="$1"
	shift
	local URL="${SRL_SERVER_URL}/pkmfilesrl"
	echo "running SRL with query ${QUERY}" >&2
	call_curl -X POST "${URL}" -H  "keyParam: ${ACCESS_KEY}" --data-raw "${QUERY}"
	if [ $? -ne 0 ]; then
		echo >&2
		echo "running SRL with query ${QUERY} failed" >&2
		$return_status 1
	fi
	$return_status 0
}

# build_srl_query(PROJECT_ID, PATH, ACCESS)
function build_srl_query()
{
	local PROJECT_ID="$1"
	shift
	local _PATH="$1"
	shift
	local ACCESS="$1"
	shift
	echo -n "{\"project_id\":\"$(uri_encode "${PROJECT_ID}")\",\"path\":\"$(uri_encode "${_PATH}")\",\"access\":\"$(uri_encode "${ACCESS}")\"}"
}
