#!/usr/bin/env bash

function pkm_get
{
  if [ "$#" -ne 2 ]; then
      echo "Error in pkm_get function: takes 2 arguments: acces key and pkm path" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PKM_PATH=$2
  if ! call_curl -X GET "${PKM_SERVER_URL}/${PKM_PATH}" -H  "key: ${ACCESS_KEY}"; then
      echo "calling get on pkm failed" >&2
      $return_status 1
  fi
  $return_status 0
}


function pkm_post
{
  if [ "$#" -ne 3 ]; then
      echo "Error in pkm_post function: takes 3 arguments: access key, pkm path and content to post" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PKM_PATH=$2
  local DATA=$3
  if ! call_curl -X POST "${PKM_SERVER_URL}/${PKM_PATH}" -H  "key: ${ACCESS_KEY}" --data-raw "${DATA}"; then
      echo "calling post on pkm path ${PKM_PATH} failed" >&2
      $return_status 1
  fi
  $return_status 0
}


function pkm_put
{
  if [ "$#" -ne 3 ]; then
      echo "Error in pkm_put function: takes 3 arguments: access key, pkm path and content to put" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PKM_PATH=$2
  local DATA=$3
  if ! call_curl -X PUT "${PKM_SERVER_URL}/${PKM_PATH}" -H  "key: ${ACCESS_KEY}" --data-raw "${DATA}"; then
      echo "calling put on pkm path ${PKM_PATH} failed" >&2
      $return_status 1
  fi
  $return_status 0
}


function pkm_login
{
  if [ "$#" -ne 2 ]; then
      echo "Error in pkm_login function: takes 2 arguments: user name and password" >&2
      $return_status 1
  fi
  local USER_NAME=$1
  local PASSWORD=$2
  echo "user's login" >&2
  local USER_KEY
  USER_KEY=$(call_curl -X POST "${PKM_SERVER_URL}/user/login" -d "{\"user_name\":\"${USER_NAME}\",\"user_password\":\"${PASSWORD}\"}" | jq .key | sed -n 's/^"\([^"]*\)"$/\1/p')
  if [ $? -ne 0 ]; then
      echo >&2
      echo "user's login failed" >&2
      $return_status 1
  fi
  echo "${USER_KEY}"
  $return_status 0
}

function pkm_logout
{
  if [ "$#" -ne 1 ]; then
      echo "Error in pkm_logout function: takes 1 argument: access key" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  echo "user's logout" >&2
  call_curl -X POST "${PKM_SERVER_URL}/user/logout" -H  "key: ${ACCESS_KEY}"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "user's logout failed" >&2
      $return_status 1
  fi
  $return_status 0
}

function dup_user_session
{
  if [ "$#" -ne 1 ]; then
      echo "Error in dup_user_session function: takes 1 argument: access key" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  echo "duplicating user's session" >&2
  local USER_KEY
  USER_KEY=$(call_curl -X POST "${PKM_SERVER_URL}/user/dup" -H  "key: ${ACCESS_KEY}" | jq .key | sed -n 's/^"\([^"]*\)"$/\1/p')
  if [ $? -ne 0 ]; then
      echo >&2
      echo "duplicating user's session failed" >&2
      $return_status 1
  fi
  echo "${USER_KEY}"
  $return_status 0
}

function pkm_post_project
{
  if [ "$#" -ne 4 ]; then
      echo "Error in pkm_post_project function: takes 4 arguments: access key, project name, owner name and owner roles" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  local OWNER=$3
  local ROLES=$4
  echo "creating project ${PROJECT_NAME} for user ${OWNER}" >&2
  call_curl -X POST "${PKM_SERVER_URL}/project" -H  "key: $ADMIN_KEY" -d "{\"name\":\"${PROJECT_NAME}\",\"members\":[{\"name\":\"${OWNER}\",\"owner\":true,\"roles\":[${ROLES}]}]}"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "Creating project${PROJECT_NAME} for user ${OWNER} failed" >&2
      $return_status 1
  fi
  $return_status 0
}

function pkm_get_project
{
  if [ "$#" -ne 2 ]; then
      echo "Error in project function: takes 2 arguments: access key and project name" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  echo "getting project ${PROJECT_NAME}" >&2
  local PROJECT
  PROJECT=$(call_curl -X GET "${PKM_SERVER_URL}/project/$(uri_encode "${PROJECT_NAME}")" -H  "key: ${ACCESS_KEY}")
  if [ $? -ne 0 ]; then
      echo >&2
      echo "getting project ${PROJECT_NAME} failed" >&2
      $return_status 1
  fi
  echo "${PROJECT}"
  $return_status 0
}

function pkm_post_user
{
  if [ "$#" -ne 3 ]; then
      echo "Error in pkm_post_user function: takes 3 arguments: access key, username and password" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local USER_NAME=$2
  local PASSWORD=$3
  echo "creating user ${USER_NAME} with password XXXXX" >&2
  call_curl -X POST "${PKM_SERVER_URL}/user" -H  "key: $ACCESS_KEY" -d "{\"password\":\"${PASSWORD}\",\"phone\":\"phone\",\"name\":\"${USER_NAME}\",\"last_name\":\"last_name\",\"first_name\":\"first_name\",\"email\":\"email\"}"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "Creating user ${USER_NAME} with password XXXXX failed." >&2
      $return_status 1
  fi
  $return_status 0
}

function pkm_delete
{
  if [ "$#" -ne 2 ]; then
      echo "Error in pkm_delete function: takes 2 arguments: access key and path to pkm_delete" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PATH_TO_DEL=$2
  echo "deleting ${PATH_TO_DEL}" >&2
  call_curl -X DELETE "${PKM_SERVER_URL}/${PATH_TO_DEL}" -H  "key: $ACCESS_KEY"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "deleting ${PATH_TO_DEL} failed" >&2
      $return_status 1
  fi
  $return_status 0
}

# pkm_send_files(METHOD, ACCESS_KEY, PKM_PATH, FORMAT, ROOT_DIR, ...FILE_PATTERNS)
function pkm_send_files
{
  if [ "$#" -lt 6 ]; then
      echo "Error in pkm_send_files function: takes at least 6 arguments: method, access key, a pkm path, a format ('text' or 'binary'), a root directory, and one or more file patterns" >&2
      $return_status 1
  fi

  local METHOD="$1"
  shift
  local ACCESS_KEY="$1"
  shift
  local PKM_PATH="$1"
  shift
  local FORMAT="$1"
  shift
  local ROOT_DIR
  ROOT_DIR=$(realpath "$1")
  if [ $? -ne 0 ]; then
    $return_status 1
  fi
  shift
  
  if [ "${FORMAT}" != 'text' ] && [ "${FORMAT}" != 'binary' ]; then
    echo "Error in pkm_post_files function: format shall be 'text' or 'binary', got '${FORMAT}'" >&2
    $return_status 1
  fi
  
  local FILES=()
  build_file_list FILES "${ROOT_DIR}" "$@"
  if [ $? -ne 0 ]; then
    $return_status 1
  fi
  
  local FILE_OBJECTS=()
  local FIRST_FILE_IN_BUNDLE='yes'
  local FILE_OBJECT_SEPARATOR=
  local FILE
  for FILE in "${FILES[@]}"; do
    local REL_PATH
    REL_PATH=$(realpath --relative-to="${ROOT_DIR}" "${FILE}")
    if [ $? -ne 0 ]; then
      $return_status 1
    fi
    echo "sending (${METHOD}) File ${REL_PATH} to ${PKM_PATH}" >&2
    if [ "${FIRST_FILE_IN_BUNDLE}" = 'yes' ]; then
      FILE_OBJECT_SEPARATOR=
      FIRST_FILE_IN_BUNDLE='no'
    else
      FILE_OBJECT_SEPARATOR=','
    fi
    local FILE_OBJECT=$(mktemp)
    FILE_OBJECTS+=("${FILE_OBJECT}")
    (
      echo -n "${FILE_OBJECT_SEPARATOR}"
      if [ "${FORMAT}" = 'binary' ]; then
        echo -n "{\"format\":\"binary\""
      else
        echo -n "{\"format\":\"text\",\"encoding\":\"utf8\""
      fi
      echo -n ",\"rel_path\":\"${REL_PATH}\",\"content\":"
      if [ "${FORMAT}" = 'binary' ]; then
        echo -n '"'
        base64_encode_file "${FILE}" || exit 1
        echo -n '"'
      else
        to_json_string "${FILE}"
      fi
      echo -n "}"
    ) >> "${FILE_OBJECT}"&
  done
  
  wait
  if [ $? -ne 0 ]; then
    $return_status 1
  fi
  
  local BODY=$(mktemp)
  ( echo -n "["
    cat "${FILE_OBJECTS[@]}"
    echo -n "]"
  ) | gzip -5 > "${BODY}"
  rm -f "${FILE_OBJECTS[@]}"
  
  if ! call_curl -X "${METHOD}" "${PKM_SERVER_URL}/${PKM_PATH}" -H  "key: $ACCESS_KEY" -H "Content-Encoding: gzip" --data-binary @"${BODY}"; then
    rm -f "${BODY}"
    echo "calling ${METHOD} on pkm path ${PKM_PATH} failed" >&2
    $return_status 1
  fi
  
  rm -f "${BODY}"
  
  $return_status 0
}

# pkm_post_files(ACCESS_KEY, PKM_PATH, FORMAT, ROOT_DIR, ...FILE_PATTERNS)
function pkm_post_files
{
  if [ "$#" -lt 5 ]; then
      echo "Error in pkm_post_files function: takes at least 5 arguments: access key, a pkm path, a format ('text' or 'binary'), a root directory, and one or more file patterns" >&2
      $return_status 1
  fi
  
  pkm_send_files 'POST' "$@"
}

# pkm_put_files(ACCESS_KEY, PKM_PATH, FORMAT, ROOT_DIR, ...FILE_PATTERNS)
function pkm_put_files
{
  if [ "$#" -lt 5 ]; then
      echo "Error in pkm_put_files function: takes at least 5 arguments: access key, a pkm path, a format ('text' or 'binary'), a root directory, and one or more file patterns" >&2
      $return_status 1
  fi
  
  pkm_send_files 'PUT' "$@"
}

# pkm_git(ACCESS_KEY, PROJECT_NAME, BODY)
function pkm_git
{
	if [ "$#" -lt 3 ]; then
		echo "Error in pkm_git function: takes at least 3 arguments: access key, project name, and a request body" >&2
		$return_status 1
	fi

	local ACCESS_KEY="$1"
	shift
	local PROJECT_NAME="$1"
	shift
	local BODY="$1"
	shift
	echo "running git with ${BODY} on project ${PROJECT_NAME}" >&2
	local JOB;
	JOB=$(call_curl -X POST "${PKM_SERVER_URL}/git/run/$(uri_encode "${PROJECT_NAME}")" -H  "key: ${ACCESS_KEY}" --data-raw "$(echo -n "${BODY}" | compact_json)")
	if [ $? -ne 0 ]; then
		echo >&2
		echo "running git with ${BODY} on project ${PROJECT_NAME} failed" >&2
		$return_status 1
	fi
	local JOB_ID;
	JOB_ID=$(echo -n "${JOB}" | jq .id)
	if [ $? -ne 0 ]; then
		echo >&2
		echo "jq failed: can't get job id" >&2
		$return_status 1
	fi

	# poll server until job is finished
	local JOB_STATE
	while true; do
		JOB_STATE=$(jq .state <<< "${JOB}")
		if [ $? -ne 0 ]; then
			echo "jq failed" >&2
			$return_status 1
		fi
		
		echo "${JOB_STATE}" >&2
		
		if [ "${JOB_STATE}" = '"failed"' ]; then
			pretty_print_json <<< "${JOB}"
			$return_status 1
		elif [ "${JOB_STATE}" = '"finished"' ]; then
			pretty_print_json <<< "${JOB}"
			break
		fi
		
		sleep 10
		
		echo "getting job" >&2
		JOB=$(call_curl -X GET "${PKM_SERVER_URL}/git/jobs/${JOB_ID}" -H  "key: ${ACCESS_KEY}")
		if [ $? -ne 0 ]; then
			echo "getting job failed" >&2
			$return_status 1
		fi
	done

	$return_status 0
}

# pkm_send_json_file(METHOD, ACCESS_KEY, PKM_PATH, FILE)
function pkm_send_json_file
{
  if [ "$#" -ne 4 ]; then
      echo "Error in pkm_send_json_file function: takes 4 arguments: method, access key, project name, and a filename" >&2
      $return_status 1
  fi
  local METHOD="$1"
  local ACCESS_KEY="$2"
  local PKM_PATH="$3"
  local FILE="$4"
  echo "sending JSON file content (${METHOD}) from ${FILE} to ${PKM_PATH}"
  call_curl -X "${METHOD}" "${PKM_SERVER_URL}/${PKM_PATH}" -H  "key: ${ACCESS_KEY}" --data-binary @"${FILE}"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "calling ${METHOD} on pkm path ${PKM_PATH} failed" >&2
      $return_status 1
  fi
  $return_status 0
}

# pkm_post_json_file(ACCESS_KEY, PKM_PATH, FILE)
function pkm_post_json_file
{
  if [ "$#" -lt 3 ]; then
      echo "Error in pkm_post_json_file function: takes at least 3 arguments: access key, a pkm path, and a JSON file" >&2
      $return_status 1
  fi
  
  pkm_send_json_file 'POST' "$@"
}

# pkm_put_json_file(ACCESS_KEY, PKM_PATH, FILE)
function pkm_put_json_file
{
  if [ "$#" -lt 3 ]; then
      echo "Error in pkm_put_json_file function: takes at least 3 arguments: access key, a pkm path, and a JSON file" >&2
      $return_status 1
  fi
  
  pkm_send_json_file 'PUT' "$@"
}

# pkm_send_json_files(METHOD, ACCESS_KEY, PKM_PATH, ROOT_DIR, ...FILE_PATTERNS)
function pkm_send_json_files
{
  if [ "$#" -lt 5 ]; then
      echo "Error in pkm_send_json_files function: takes at least 5 arguments: method, access key, a pkm path, a root directory, and one or more JSON file patterns" >&2
      $return_status 1
  fi
  local METHOD="$1"
  shift
  local ACCESS_KEY="$1"
  shift
  local PKM_PATH="$1"
  shift
  local ROOT_DIR
  ROOT_DIR=$(realpath "$1")
  if [ $? -ne 0 ]; then
    $return_status 1
  fi
  shift
  
  local FILES=()
  build_file_list FILES "${ROOT_DIR}" "$@"
  if [ $? -ne 0 ]; then
    $return_status 1
  fi
  
  local BODY=$(mktemp)
  local FILE
  local SEPARATOR=
  echo -n '[' > "${BODY}"
  for FILE in "${FILES[@]}"; do
    echo -n "${SEPARATOR}" >> "${BODY}"
    if [ -z "${SEPARATOR}" ]; then
      SEPARATOR=','
    fi
    cat "${FILE}" >> "${BODY}"
  done
  echo -n ']' >> "${BODY}"
  
  echo "sending JSON file contents (${METHOD}) from $@ found in ${ROOT_DIR} to ${PKM_PATH}"
  call_curl -X "${METHOD}" "${PKM_SERVER_URL}/${PKM_PATH}" -H  "key: ${ACCESS_KEY}" --data-binary @"${BODY}"
  if [ $? -ne 0 ]; then
      rm -f "${BODY}"
      echo >&2
      echo "calling ${METHOD} on pkm path ${PKM_PATH} failed" >&2
      $return_status 1
  fi
  
  rm -f "${BODY}"
  
  $return_status 0
}

# pkm_post_json_files(ACCESS_KEY, PKM_PATH, ROOT_DIR, ...FILE_PATTERNS)
function pkm_post_json_files
{
  if [ "$#" -lt 4 ]; then
      echo "Error in pkm_post_json_files function: takes at least 4 arguments: access key, a pkm path, a root directory, and one or more JSON file patterns" >&2
      $return_status 1
  fi
  
  pkm_send_json_files 'POST' "$@"
}

# pkm_put_json_files(ACCESS_KEY, PKM_PATH, ROOT_DIR, ...FILE_PATTERNS)
function pkm_put_json_files
{
  if [ "$#" -lt 4 ]; then
      echo "Error in pkm_put_json_files function: takes at least 4 arguments: access key, a pkm path, a root directory, and one or more JSON file patterns" >&2
      $return_status 1
  fi
  
  pkm_send_json_files 'PUT' "$@"
}
