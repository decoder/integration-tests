#!/usr/bin/env bash

# run_semparsing(ACCESS_KEY, QUERY)
function run_semparsing
{
	if [ "$#" -lt 2 ]; then
		echo "Error in semparsing function: takes at least 2 arguments: access key and query" >&2
		$return_status 1
	fi

	local ACCESS_KEY="$1"
	shift
	local QUERY="$1"
	shift
	local URL="${SEMPARSING_SERVER_URL}/pkmfilesemparser"
	echo "running Semantic parsing with query ${QUERY}" >&2
	call_curl -X POST "${URL}" -H  "keyParam: ${ACCESS_KEY}" --data-raw "${QUERY}"
	if [ $? -ne 0 ]; then
		echo >&2
		echo "running Semantic parsing with query ${QUERY} failed" >&2
		$return_status 1
	fi
	$return_status 0
}

# build_semparsing_query(PROJECT_ID, FILE, SOURCE_LANGUAGE, TARGET_LANGUAGE)
function build_semparsing_query()
{
	local PROJECT_ID="$1"
	shift
	local FILE="$1"
	shift
	local SOURCE_LANGUAGE="$1"
	shift
	local TARGET_LANGUAGE="$1"
	shift
	echo -n "{\"project_id\":\"$(uri_encode "${PROJECT_ID}")\",\"file\":\"$(uri_encode "${FILE}")\",\"source_language\":\""${SOURCE_LANGUAGE}"\",\"target_language\":\"${TARGET_LANGUAGE}\"}"
}
