#!/usr/bin/env bash

# run_doc_to_asfm(ACCESS_KEY, PROJECT_NAME, [QUERY], FILE_PATH)
function run_doc_to_asfm
{
	if [ $# -lt 3 ]; then
		echo "Error in run_doc_to_asfm function: takes at least 3 arguments: access key, project name (and optionally query), and a file path" >&2
		$return_status 1
	fi

	local ACCESS_KEY="$1"
	shift
	local PROJECT_NAME="$1"
	shift
	local QUERY=''
	if [ "$1" != '--' ] && [ -n "$1" ]; then
		QUERY="$1"
	fi
	shift
	local FILE_PATH="$1"
	shift
	echo "running doc_to_asfm on '${FILE_PATH}' of project ${PROJECT_NAME} with query '${QUERY}'" >&2
	local JOB;
	local URL="${ASFM_SERVER_URL}/asfm/doc_to_asfm/$(uri_encode "${PROJECT_NAME}")"
	if [ -n "${QUERY}" ]; then
		URL+="?${QUERY}"
	fi
	JOB=$(call_curl -X POST "${URL}" -H  "key: ${ACCESS_KEY}" --data-raw "{\"file_path\": $(echo -n "${FILE_PATH}" | jq -Rs)}")
	if [ $? -ne 0 ]; then
		echo >&2
		echo "running doc_to_asfm on '${FILE_PATH}' of project ${PROJECT_NAME} failed" >&2
		$return_status 1
	fi
	local JOB_ID
	JOB_ID=$(echo -n "${JOB}" | jq .id)
	if [ $? -ne 0 ]; then
		echo >&2
		echo "jq failed: can't get job id" >&2
		$return_status 1
	fi

	# poll server until job is finished
	local JOB_STATE
	while true; do
		JOB_STATE=$(jq .state <<< "${JOB}")
		if [ $? -ne 0 ]; then
			echo "jq failed" >&2
			$return_status 1
		fi
		
		echo "${JOB_STATE}" >&2
		
		if [ "${JOB_STATE}" = '"failed"' ]; then
			pretty_print_json <<< "${JOB}"
			$return_status 1
		elif [ "${JOB_STATE}" = '"finished"' ]; then
			pretty_print_json <<< "${JOB}"
			break
		fi
		
		sleep 10
		
		echo "getting job" >&2
		JOB=$(call_curl -X GET "${ASFM_SERVER_URL}/asfm/jobs/${JOB_ID}" -H  "key: ${ACCESS_KEY}")
		if [ $? -ne 0 ]; then
			echo "getting job failed" >&2
			$return_status 1
		fi
	done

	$return_status 0
}

# run_asfm_to_doc(ACCESS_KEY, PROJECT_NAME, [QUERY], DOC_NAME)
function run_asfm_to_doc
{
	if [ "$#" -lt 3 ]; then
		echo "Error in run_asfm_to_doc function: takes at least 3 arguments: access key, project name (and optionally query), and document name" >&2
		$return_status 1
	fi

	local ACCESS_KEY="$1"
	shift
	local PROJECT_NAME="$1"
	shift
	local QUERY=''
	if [ "$1" != '--' ] && [ -n "$1" ]; then
		QUERY="$1"
	fi
	shift
	local DOC_NAME="$1"
	shift
	echo "running asfm_to_doc on '${DOC_NAME}' of project ${PROJECT_NAME} with query '${QUERY}'" >&2
	local JOB
	local URL="${ASFM_SERVER_URL}/asfm/asfm_to_doc/$(uri_encode "${PROJECT_NAME}")"
	if [ -n "${QUERY}" ]; then
		URL+="?${QUERY}"
	fi
	JOB=$(call_curl -X POST "${URL}" -H  "key: ${ACCESS_KEY}" --data-raw "{\"docName\": $(echo -n "${DOC_NAME}" | jq -Rs)}")
	if [ $? -ne 0 ]; then
		echo >&2
		echo "running asfm_to_doc on '${DOC_NAME}' of project ${PROJECT_NAME} failed" >&2
		$return_status 1
	fi
	local JOB_ID
	JOB_ID=$(echo -n "${JOB}" | jq .id)
	if [ $? -ne 0 ]; then
		echo >&2
		echo "jq failed: can't get job id" >&2
		$return_status 1
	fi

	# poll server until job is finished
	local JOB_STATE
	while true; do
		JOB_STATE=$(jq .state <<< "${JOB}")
		if [ $? -ne 0 ]; then
			echo "jq failed" >&2
			$return_status 1
		fi
		
		echo "${JOB_STATE}" >&2
		
		if [ "${JOB_STATE}" = '"failed"' ]; then
			pretty_print_json <<< "${JOB}"
			$return_status 1
		elif [ "${JOB_STATE}" = '"finished"' ]; then
			pretty_print_json <<< "${JOB}"
			break
		fi
		
		sleep 10
		
		echo "getting job" >&2
		JOB=$(call_curl -X GET "${ASFM_SERVER_URL}/asfm/jobs/${JOB_ID}" -H  "key: ${ACCESS_KEY}")
		if [ $? -ne 0 ]; then
			echo "getting job failed" >&2
			$return_status 1
		fi
		
	done

	$return_status 0
}

# run_code_to_asfm(ACCESS_KEY, PROJECT_NAME, [QUERY])
function run_code_to_asfm
{
	if [ "$#" -lt 2 ]; then
		echo "Error in run_code_to_asfm function: takes at least 2 arguments: access key and project name (and optionally query)" >&2
		$return_status 1
	fi

	local ACCESS_KEY="$1"
	shift
	local PROJECT_NAME="$1"
	shift
	local QUERY=''
	if [ "$1" != '--' ] && [ -n "$1" ]; then
		QUERY="$1"
	fi
	shift
	echo "running code_to_asfm on project ${PROJECT_NAME} with query '${QUERY}'" >&2
	local JOB
	local URL="${ASFM_SERVER_URL}/asfm/code_to_asfm/$(uri_encode "${PROJECT_NAME}")"
	if [ -n "${QUERY}" ]; then
		URL+="?${QUERY}"
	fi
	JOB=$(call_curl -X POST "${URL}" -H  "key: ${ACCESS_KEY}")
	if [ $? -ne 0 ]; then
		echo >&2
		echo "running code_to_asfm on project ${PROJECT_NAME} failed" >&2
		$return_status 1
	fi
	local JOB_ID
	JOB_ID=$(echo -n "${JOB}" | jq .id)
	if [ $? -ne 0 ]; then
		echo >&2
		echo "jq failed: can't get job id" >&2
		$return_status 1
	fi

	# poll server until job is finished
	local JOB
	local JOB_STATE
	while true; do
		JOB_STATE=$(jq .state <<< "${JOB}")
		if [ $? -ne 0 ]; then
			echo "jq failed" >&2
			$return_status 1
		fi
		
		echo "${JOB_STATE}" >&2
		
		if [ "${JOB_STATE}" = '"failed"' ]; then
			pretty_print_json <<< "${JOB}"
			$return_status 1
		elif [ "${JOB_STATE}" = '"finished"' ]; then
			pretty_print_json <<< "${JOB}"
			break
		fi
		
		sleep 10
		
		echo "getting job" >&2
		JOB=$(call_curl -X GET "${ASFM_SERVER_URL}/asfm/jobs/${JOB_ID}" -H  "key: ${ACCESS_KEY}")
		if [ $? -ne 0 ]; then
			echo "getting job failed" >&2
			$return_status 1
		fi
	done

	$return_status 0
}
