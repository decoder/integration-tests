#!/usr/bin/env bash

# smmodelxmi2jsontransformer(ACCESS_KEY, PROJECT_NAME, UML_XMI_FILENAME)
function smmodelxmi2jsontransformer
{
	if [ "$#" -lt 3 ]; then
		echo "Error in smmodelxmi2jsontransformer function: takes at least 3 arguments: access key, project name and a filename" >&2
		$return_status 1
	fi

	local ACCESS_KEY="$1"
	shift
	local PROJECT_NAME="$1"
	shift
	local UML_XMI_FILENAME="$1"
	shift
	local URL="${SMMODELXMI2JSONTRANSFORMER_SERVER_URL}/decoder/smModelToJson/$(uri_encode "${PROJECT_NAME}")/$(uri_encode "${UML_XMI_FILENAME}")"
	echo "running smmodelxmi2jsontransformer on ${UML_XMI_FILENAME} of project ${PROJECT_NAME}" >&2
	call_curl -X GET "${URL}" -H  "key: ${ACCESS_KEY}"
	if [ $? -ne 0 ]; then
		echo >&2
		echo "running smmodelxmi2jsontransformer on ${UML_XMI_FILENAME} of project ${PROJECT_NAME} failed" >&2
		$return_status 1
	fi
	$return_status 0
}
