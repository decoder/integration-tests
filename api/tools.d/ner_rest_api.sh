#!/usr/bin/env bash

# run_ner(ACCESS_KEY, QUERY)
function run_ner
{
	if [ "$#" -lt 2 ]; then
		echo "Error in ner function: takes at least 2 arguments: access key and query" >&2
		$return_status 1
	fi

	local ACCESS_KEY="$1"
	shift
	local QUERY="$1"
	shift
	local URL="${NER_SERVER_URL}/pkmner"
	echo "running NER with query ${QUERY}" >&2
	call_curl -X POST "${URL}" -H  "keyParam: ${ACCESS_KEY}" --data-raw "${QUERY}"
	if [ $? -ne 0 ]; then
		echo >&2
		echo "running NER with query ${QUERY} failed" >&2
		$return_status 1
	fi
	$return_status 0
}

# build_ner_query(PROJECT_ID, PATH, ACCESS)
function build_ner_query()
{
	local PROJECT_ID="$1"
	shift
	local _PATH="$1"
	shift
	local ACCESS="$1"
	shift
	echo -n "{\"project_id\":\"$(uri_encode "${PROJECT_ID}")\",\"path\":\"$(uri_encode "${_PATH}")\",\"access\":\"$(uri_encode "${ACCESS}")\"}"
}
