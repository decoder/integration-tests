#!/usr/bin/env bash

# run_frama_clang(ACCESS_KEY, PROJECT_NAME, OPTIONS, QUERY, ...SOURCE_FILE_PATHS)
function run_frama_clang
{
	if [ $# -lt 5 ]; then
		echo "Error in run_frama_clang function: takes at least 5 arguments: access key, project name, query, and one or more source file paths" >&2
		$return_status 1
	fi

	local ACCESS_KEY="$1"
	shift
	local PROJECT_NAME="$1"
	shift
	local OPTIONS='{}'
	local QUERY=''
	if [ "$1" != '--' ]; then
		OPTIONS="$1"
		shift
		if [ "$1" != '--' ]; then
			QUERY="$1"
		fi
	fi
	shift
	echo "running frama-clang with options ${OPTIONS} on $(printf "'%s' " "$@")of project ${PROJECT_NAME} with query '${QUERY}'" >&2
	local JOB
	local URL="${FRAMA_CLANG_SERVER_URL}/frama_clang/$(uri_encode "${PROJECT_NAME}")"
	if [ -n "${QUERY}" ]; then
		URL+="?${QUERY}"
	fi
	JOB=$(call_curl -X POST "${URL}" -H  "key: ${ACCESS_KEY}" --data-raw "{ \"source_file_paths\": $(printf '%s\n' "$@" | jq -Rs '(. / "\n" | map(select(length > 0) | .))'),\"options\":${OPTIONS}}")
	if [ $? -ne 0 ]; then
		echo >&2
		echo "running frama-clang with options ${OPTIONS} on $(printf "'%s' " "$@")of project ${PROJECT_NAME} failed" >&2
		$return_status 1
	fi
	local JOB_ID
	JOB_ID=$(echo -n "${JOB}" | jq .id)
	if [ $? -ne 0 ]; then
		echo >&2
		echo "jq failed: can't get job id" >&2
		$return_status 1
	fi

	# poll server until job is finished
	local JOB_STATE
	while true; do
		JOB_STATE=$(jq .state <<< "${JOB}")
		if [ $? -ne 0 ]; then
			echo "jq failed" >&2
			$return_status 1
		fi
		
		echo "${JOB_STATE}" >&2
		
		if [ "${JOB_STATE}" = '"failed"' ]; then
			pretty_print_json <<< "${JOB}"
			$return_status 1
		elif [ "${JOB_STATE}" = '"finished"' ]; then
			pretty_print_json <<< "${JOB}"
			break
		fi
		
		sleep 10
		
		echo "getting job" >&2
		JOB=$(call_curl -X GET "${FRAMA_CLANG_SERVER_URL}/frama_clang/jobs/${JOB_ID}" -H  "key: ${ACCESS_KEY}")
		if [ $? -ne 0 ]; then
			echo "getting job failed" >&2
			$return_status 1
		fi
	done

	$return_status 0
}
