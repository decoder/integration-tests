#!/usr/bin/env bash

function jmlgen_login
{
  if [ "$#" -ne 2 ]; then
      echo "Error in jmlgen_login function: takes 2 arguments: user name and password" >&2
      $return_status 1
  fi
  local USER_NAME=$1
  local PASSWORD=$2
  local USER_KEY
  USER_KEY=$(call_curl -X POST "${JMLGEN_SERVER_URL}/jmlgen/login?user=${USER_NAME}&password=${PASSWORD}" | jq .key | sed -n 's/^"\([^"]*\)"$/\1/p')
  if [ $? -ne 0 ]; then
      echo >&2
      echo "user's login failed" >&2
      $return_status 1
  fi
  echo "${USER_KEY}"
  $return_status 0
}

function jmlgen_gitclone
{
  if [ "$#" -ne 3 ]; then
      echo "Error in jmlgen_gitclone function: takes 3 arguments: access key, project name, repo URL" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  local REPO=$3
  call_curl -X POST "${JMLGEN_SERVER_URL}/jmlgen/gitclone?db=${PROJECT_NAME}&repo=$(uri_encode ${REPO})&extension=.java" -H  "key: $ADMIN_KEY"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "Cloning ${REPO} into ${PROJECT_NAME} failed" >&2
      $return_status 1
  fi
  $return_status 0
}


function jmlgen_generate
{
  if [ "$#" -ne 2 ]; then
      echo "Error in jmlgen_generate function: takes 2 arguments: access key, project name" >&2
      $return_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  call_curl -X POST "${JMLGEN_SERVER_URL}/jmlgen/generate?db=${PROJECT_NAME}" -H  "key: $ADMIN_KEY"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "Generating JML into ${PROJECT_NAME} failed" >&2
      $return_status 1
  fi
  $return_status 0
}

