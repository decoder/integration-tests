#!/usr/bin/env bash

# run_excavator(ACCESS_KEY, PROJECT_NAME, [QUERY], CONFIG)
function run_excavator
{
	if [ "$#" -lt 3 ]; then
			echo "Error in run_excavator function: takes at least 3 arguments: access key, project name (and optionally query), and config" >&2
			$return_status 1
	fi

	local ACCESS_KEY="$1"
	shift
	local PROJECT_NAME="$1"
	shift
	local QUERY=''
	if [ "$1" != '--' ] && [ -n "$1" ]; then
		QUERY="$1"
	fi
	shift
	local CONFIG="$1"
	shift
	local URL="${EXCAVATOR_SERVER_URL}/excavator/$(uri_encode "${PROJECT_NAME}")"
	if [ -n "${QUERY}" ]; then
		URL+="?${QUERY}"
	fi
	echo "running UNISIM Excavator with query '${QUERY}' and config ${CONFIG}" >&2
	local JOB
	JOB=$(call_curl -X POST "${URL}" -H  "key: ${ACCESS_KEY}" --data-raw "${CONFIG}")
	if [ $? -ne 0 ]; then
		echo >&2
		echo "running UNISIM Excavator with config ${CONFIG} failed" >&2
		$return_status 1
	fi
	local JOB_ID
	JOB_ID=$(echo -n "${JOB}" | jq .id)
	if [ $? -ne 0 ]; then
		echo >&2
		echo "jq failed: can't get job id" >&2
		$return_status 1
	fi
	
	# poll server until job is finished
	local JOB_STATE
	while true; do
		JOB_STATE=$(jq .state <<< "${JOB}")
		if [ $? -ne 0 ]; then
			echo "jq failed" >&2
			$return_status 1
		fi
		
		echo "${JOB_STATE}" >&2
		
		if [ "${JOB_STATE}" = '"failed"' ]; then
			pretty_print_json <<< "${JOB}"
			$return_status 1
		elif [ "${JOB_STATE}" = '"finished"' ]; then
			pretty_print_json <<< "${JOB}"
			break
		fi
		
		sleep 10
		
		echo "getting job" >&2
		JOB=$(call_curl -X GET "${EXCAVATOR_SERVER_URL}/excavator/jobs/${JOB_ID}" -H  "key: ${ACCESS_KEY}")
		if [ $? -ne 0 ]; then
			echo "getting job failed" >&2
			$return_status 1
		fi
	done

	$return_status 0
}
