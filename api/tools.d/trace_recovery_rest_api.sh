#!/usr/bin/env bash

# trace_recovery(ACCESS_KEY, QUERY)
function trace_recovery
{
	if [ "$#" -lt 2 ]; then
			echo "Error in trace_recovery function: takes at least 2 arguments: access key and query" >&2
			$return_status 1
	fi

	local ACCESS_KEY="$1"
	shift
	local QUERY="$1"
	shift
	local URL="${TRACE_RECOVERY_SERVER_URL}/pkm_trace_recovery"
	echo "running Trace recovery with query ${QUERY}" >&2
	call_curl -X POST "${URL}" -H  "keyParam: ${ACCESS_KEY}" --data-raw "${QUERY}"
	if [ $? -ne 0 ]; then
		echo >&2
		echo "running Trace recovery with query ${QUERY} failed" >&2
		$return_status 1
	fi
	$return_status 0
}

# build_trace_recovery_query(PROJECT_ID, SRC_PATH, SRC_ACCESS, TGT_PATH, TGT_ACCESS)
function build_trace_recovery_query()
{
	local PROJECT_ID="$1"
	shift
	local SRC_PATH="$1"
	shift
	local SRC_ACCESS="$1"
	shift
	local TGT_PATH="$1"
	shift
	local TGT_ACCESS="$1"
	shift
	echo -n "{\"project_id\":\"$(uri_encode "${PROJECT_ID}")\",\"src_path\":\"$(uri_encode "${SRC_PATH}")\",\"src_access\":\"$(uri_encode "${SRC_ACCESS}")\",\"tgt_path\":\"$(uri_encode "${TGT_PATH}")\",\"tgt_access\":\"$(uri_encode "${TGT_ACCESS}")\"}"
}
