#!/usr/bin/env bash

# javaparser(ACCESS_KEY, PROJECT_NAME, SOURCE_FILENAME, [GENERATE_MODE])
function javaparser
{
  if [ "$#" -lt 3 ]; then
      echo "Error in javaparser function: takes at least 3 arguments: access key, project name and source filename, and optionally generate mode (ast, comments, annotations, or all=default)" >&2
      $return_status 1
  fi
  
  local ACCESS_KEY="$1"
  shift
  local PROJECT_NAME="$1"
	shift
	local SOURCE_FILENAME="$1"
	shift
	local GENERATE_MODE=''
	if [ $# -ne 0 ]; then
		GENERATE_MODE="$1"
		shift
	fi
	local URL="${JAVAPARSER_SERVER_URL}/decoder/javaASTGenerator/$(uri_encode "${PROJECT_NAME}")/$(uri_encode "${SOURCE_FILENAME}")"
	if [ -n "${GENERATE_MODE}" ]; then
		URL+="?generate=$(uri_encode "${GENERATE_MODE}")"
	fi
  echo "running javaparser on ${SOURCE_FILENAME} with generate mode '${GENERATE_MODE}' of project ${PROJECT_NAME}" >&2
	call_curl -X GET "${URL}" -H  "key: ${ACCESS_KEY}"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "running javaparser on ${SOURCE_FILENAME} with generate mode '${GENERATE_MODE}' of project ${PROJECT_NAME} failed" >&2
      $return_status 1
  fi
  $return_status 0
}
