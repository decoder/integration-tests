#!/usr/bin/env bash

# predictions_code_summ(ACCESS_KEY, PROJECT_NAME, ARTEFACT_ID)
function predictions_code_summ
{
	if [ "$#" -lt 3 ]; then
		echo "Error in predictions_code_summ function: takes at least 3 arguments: access key, project name and artefact id" >&2
		$return_status 1
	fi

	local ACCESS_KEY="$1"
	shift
	local PROJECT_NAME="$1"
	shift
	local ARTEFACT_ID="$1"
	shift
	echo "running code summarization on artefact ${ARTEFACT_ID} of project ${PROJECT_NAME}" >&2
	call_curl -X POST "${CODE_SUMMARIZATION_SERVER_URL}/predictions/$(uri_encode "${PROJECT_NAME}")/code_summarization/$(uri_encode "${ARTEFACT_ID}")" -H  "key: ${ACCESS_KEY}"
	if [ $? -ne 0 ]; then
		echo >&2
		echo "running code summarization on artefact ${ARTEFACT_ID} of project ${PROJECT_NAME} failed" >&2
		$return_status 1
	fi
	$return_status 0
}

# predictions_code_summ_all(ACCESS_KEY, PROJECT_NAME)
function predictions_code_summ_all
{
	if [ "$#" -lt 2 ]; then
		echo "Error in predictions_code_summ_all function: takes at least 2 arguments: access key and project name" >&2
		$return_status 1
	fi

	local ACCESS_KEY="$1"
	shift
	local PROJECT_NAME="$1"
	shift
	echo "running code summarization on all artefacts of project ${PROJECT_NAME}" >&2
	call_curl -X POST "${CODE_SUMMARIZATION_SERVER_URL}/predictions/$(uri_encode "${PROJECT_NAME}")/code_summarization" -H  "key: ${ACCESS_KEY}"
	if [ $? -ne 0 ]; then
		echo >&2
		echo "running code summarization on all artefacts of project ${PROJECT_NAME} failed" >&2
		$return_status 1
	fi
	$return_status 0
}
