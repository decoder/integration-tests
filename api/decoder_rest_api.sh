#!/usr/bin/env bash

set -o errexit
set -o pipefail

API=$(cd $(dirname $BASH_SOURCE); pwd)

export HOSTALIASES="${API}/hosts"

if [ -z "${TARGET}" ]; then
	TARGET='master'
fi

source "${API}/conf.d/${TARGET}.conf"

set -o nounset

tracefile=$0
tracefile="${tracefile%.*}.trace"

function exit_status
{
    local STATUS=$1
    if [ $STATUS -eq 0 ]; then
        echo "Test PASSED" >&2
    else
        echo "Test FAILED" >&2
    fi

    exit $STATUS
}

function return_status
{
	if [ $1 -ne 0 ]; then
		exit_status $1
	fi
}

return_status=return_status

function mr_proper_prompt
{
  set +o nounset
  if [ "${MR_PROPER,,}" != 'yes' ] && [ "${MR_PROPER,,}" != 'y' ] && [ "${MR_PROPER,,}" != 'no' ] && [ "${MR_PROPER,,}" != 'n' ]; then
    echo -n "Do you want to clean before exiting (yes/No) ? "
    read MR_PROPER
  fi
  set -o nounset
  
  if [ "${MR_PROPER,,}" = 'yes' ] || [ "${MR_PROPER,,}" = 'y' ]; then
    return 0
  else
    return 1
  fi
}

function uri_encode
{
  echo -n $1 | jq -sRr @uri | sed -e 's/(/%28/g' -e 's/)/%29/g'
}

function pretty_print_json
{
  if [ $# -ne 0 ]; then
    jq . "$1"
  else
    jq .
  fi
}

# remove end of line and extra spaces from a JSON file
# example:              
# {                     |
#   "a" : "string",     |==>    {"a":"string","b":1}
#   "b" : 1             |
# }                     
function compact_json
{
  if [ $# -ne 0 ]; then
    jq -c . "$1"
  else
    jq .
  fi
}

# convert a text file to a JSON string
# example:              
# hello     |
# world!    |==>    "hello\nworld!\n"
#           |     
function to_json_string
{
  if [ $# -ne 0 ]; then
    jq -Rs . "$1"
  else
    jq -Rs .
  fi
}

function base64_encode_file
{
  node -e "process.stdout.write(require('fs').readFileSync(process.argv[1]).toString('base64'));" "$1"
}

# build_file_list(&FILE_LIST, STARTING_POINT, ...PATTERNS)
function build_file_list
{
  local -n __FILE_LIST_REF__=$1
  shift
  local STARTING_POINT=$1
  shift
  local FIND_EXPR=()
  for PATTERN in "$@"; do
    if [[ "${PATTERN}" == *'/'* ]]; then
      if [ -f "${STARTING_POINT}/${PATTERN}" ]; then
        __FILE_LIST_REF__+=( $(realpath "${STARTING_POINT}/${PATTERN}") )
        if [ $? -ne 0 ]; then
          return 1
        fi
      else
        __FILE_LIST_REF__+=( $(realpath "${PATTERN}") )
        if [ $? -ne 0 ]; then
          return 1
        fi
      fi
    else
      if [ ${#FIND_EXPR[@]} -ne 0 ]; then
        FIND_EXPR+=( '-o' )
      fi
      FIND_EXPR+=( '-type' 'f' '-name' "${PATTERN}" '-print0' )
    fi
  done
  if [ ${#FIND_EXPR[@]} -ne 0 ]; then
    local FIND_CMD=( 'find' "${STARTING_POINT}" "${FIND_EXPR[@]}" )
    if [ $? -ne 0 ]; then
      return 1
    fi
    local FILE
    while IFS= read -d $'\0' -r FILE ; do
      __FILE_LIST_REF__+=( "${FILE}" )
    done < <("${FIND_CMD[@]}")
  fi
}

function generate_random_password
{
	local LENGTH=8
	if [ $# -ne 0 ]; then
		LENGTH=$1;
	fi
	# set of all printable ASCII characters but space, single quote, double quote, backquote, and backslash
	local chars='!#$%&()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_abcdefghijklmnopqrstuvwxyz{|}~'
	# generate a password with random characters
	while [ ${LENGTH} -ge 0 ]; do
		echo -n "${chars:RANDOM%${#chars}:1}"
		LENGTH=$((${LENGTH}-1))
	done
}

function generate_random_hex
{
	local LENGTH=8
	if [ $# -ne 0 ]; then
		LENGTH=$1;
	fi
	# set of all hex characters
	local chars='0123456789ABCDEFabcdef'
	# generate a string of random hex digits
	while [ ${LENGTH} -ge 0 ]; do
		echo -n "${chars:RANDOM%${#chars}:1}"
		LENGTH=$((${LENGTH}-1))
	done
}

function call_curl
{
		if [ "${TRACE}" = "yes" ] && [ ! -f "${tracefile}" ]; then
				echo -n "" > ${tracefile}
		fi
    local CURL=$(which curl)
    local TRACE_FILE
    local OUT_FILE
    local HEADERS_FILE
    local args=("$@")
    local i=0
    local CURL_CMD=("$CURL")
    if [ "${COMPRESSED}" = "yes" ]; then
        CURL_CMD+=('--compressed')
    fi
    if [ "${TRACE}" = "yes" ]; then
        TRACE_FILE=$(mktemp)
        CURL_CMD+=('--trace-ascii' "${TRACE_FILE}")
    fi
    CURL_CMD+=('-s' '-S')
    CURL_CMD+=('-H'  'accept: application/json')
    CURL_CMD+=('-H'  'Content-Type: application/json')
    OUT_FILE=$(mktemp)
    CURL_CMD+=('-o' "${OUT_FILE}")
    HEADERS_FILE=$(mktemp)
    CURL_CMD+=('-D' "${HEADERS_FILE}")
    if [ "${ACCEPT_UNTRUSTED_SERVER_CERTIFICATE}" = "yes" ]; then
			CURL_CMD+=('-k')
    elif [ -f "${API}/certs/${CERT}" ]; then
			CURL_CMD+=('--cacert' "${API}/certs/${CERT}")
    fi
    CURL_CMD+=('-m' "${CURL_MAX_TIME}")

    while [ $i -lt $# ]; do
        CURL_CMD+=("${args[$i]}")
        i=$(($i+1))
    done
    if [ "${PRINT_CURL_CMD}" = 'yes' ]; then
      echo "${CURL_CMD[@]}" >&2
    fi
    "${CURL_CMD[@]}"
    if [ $? -ne 0 ]; then
        echo >&2
        echo "call to pkm failed" >&2
        $return_status 1
    fi
    http_code=$(grep "HTTP/[0-9][0-9]*.[0-9][0-9]*" ${HEADERS_FILE} | tail -n 1 | sed -e "s/HTTP\/[0-9][0-9]*\.[0-9][0-9]* //" -e "s/ .*//")
    local STATUS=0
    if [ ${http_code} -lt 300 ] ;
    then
        STATUS=0 ;
        pretty_print_json ${OUT_FILE} || cat ${OUT_FILE}
    else
        echo "Call to PKM server failed:" >&2
        printf "'%s' " "${CURL_CMD[@]}" >&2
        echo >&2
        cat ${HEADERS_FILE} >&2
        cat ${OUT_FILE} >&2
        STATUS=${http_code} ;
    fi
    if [ "${TRACE}" = "yes" ]; then
        printf "'%s' " "${CURL_CMD[@]}" >> ${tracefile}
        cat "${TRACE_FILE}" >> ${tracefile}
        rm -f "${TRACE_FILE}"
        cat "${HEADERS_FILE}" >> ${tracefile}
        cat "${OUT_FILE}" >> ${tracefile}
    fi

    rm -f "${HEADERS_FILE}"
    rm -f "${OUT_FILE}"
#     echo "curl returns ${STATUS}" >&2
    $return_status ${STATUS}
}

for TOOL_REST_API in "${API}/tools.d"/*; do
	source "${TOOL_REST_API}"
done
